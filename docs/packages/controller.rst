Controller package
==================

Some description.


API Reference
-------------

Movement controller
^^^^^^^^^^^^^^^^^^^

.. automodule:: mmrs_controller.movement_controller.movement_controller
    :members:
    :private-members:


Path divisions
^^^^^^^^^^^^^^

Path segment
""""""""""""

.. automodule:: mmrs_controller.path.path_segment
    :members:
    :private-members:


Path divider
""""""""""""

.. automodule:: mmrs_controller.path.dividers.path_divider
    :members:
    :private-members:


Equal length divider
""""""""""""""""""""

.. automodule:: mmrs_controller.path.dividers.equal_length_divider
    :members:
    :private-members:


Optimal length divider
""""""""""""""""""""""

.. automodule:: mmrs_controller.path.dividers.optimal_length_divider
    :members:
    :private-members:


Grid cells divider
""""""""""""""""""

.. automodule:: mmrs_controller.path.dividers.grid_cells_divider
    :members:
    :private-members:


Robot movement
^^^^^^^^^^^^^^

.. automodule:: mmrs_controller.robot_movement.robot_path
    :members:
    :private-members:


.. automodule:: mmrs_controller.robot_movement.robot_position
    :members:
    :private-members:


Main controller
^^^^^^^^^^^^^^^

.. automodule:: mmrs_controller.controller
    :members:
    :private-members:
