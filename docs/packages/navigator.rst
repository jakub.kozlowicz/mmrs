Navigator package
==================

Some description.

API Reference
-------------

.. automodule:: mmrs_navigator.navigator
    :members:
    :private-members:


.. automodule:: mmrs_navigator.path_transformations
    :members:
    :private-members:
