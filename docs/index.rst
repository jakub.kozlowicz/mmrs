.. Multi-Mobile Robot System documentation master file, created by
   sphinx-quickstart on Tue Feb 20 13:02:48 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Multi-Mobile Robot System
=========================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Introduction
============

Introduction to the system and architecture.

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Introduction

   introduction.rst


Packages
========

Description of the packages.

.. toctree::
   :maxdepth: 2
   :hidden:
   :caption: Packages

   packages/controller.rst
   packages/map_server.rst
   packages/msgs.rst
   packages/navigator.rst
   packages/simulation.rst


License
=======

| Copyright (c) 2023-2024 Jakub Kozłowicz
| MIT License

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
