ARG ROS_DISTRO=humble
ARG OVERLAY_WS=/opt/ros/overlay_ws

# multi-stage for caching
FROM ros:$ROS_DISTRO AS cacher

# clone overlay source
ARG ROS_DISTRO
ARG OVERLAY_WS
WORKDIR $OVERLAY_WS/src

# Do not copy the entire workspace, as it contains simulation packages that
# are not needed
COPY src/mmrs_controller ./mmrs_controller
COPY src/mmrs_map_server ./mmrs_map_server
COPY src/mmrs_msgs ./mmrs_msgs
COPY src/mmrs_navigator ./mmrs_navigator
COPY src/multi_mobile_robot_system ./multi_mobile_robot_system


# copy manifests for caching
WORKDIR /opt
RUN mkdir -p /tmp/opt \
  && find . -type f -name "package.xml" -exec cp --parents -t /tmp/opt {} + \
  && find . -type f -name "COLCON_IGNORE" -exec cp --parents -t /tmp/opt {} +

# multi-stage for building
FROM ros:$ROS_DISTRO AS builder

# install overlay dependencies
ARG ROS_DISTRO
ARG OVERLAY_WS
WORKDIR $OVERLAY_WS
COPY --from=cacher /tmp$OVERLAY_WS/src ./src
RUN . "/opt/ros/${ROS_DISTRO}/setup.sh" \
  && apt-get update \
  && apt-get upgrade -y \
  && rosdep install -y --from-paths src --ignore-src \
  && rm -rf /var/lib/apt/lists/*

# build overlay source
COPY --from=cacher $OVERLAY_WS/src ./src
ARG OVERLAY_MIXINS="release"
RUN . "/opt/ros/${ROS_DISTRO}/setup.sh" \
  && colcon build --mixin ${OVERLAY_MIXINS}

# source entrypoint setup
ENV OVERLAY_WS $OVERLAY_WS
RUN sed --in-place --expression \
  "\$isource $OVERLAY_WS/install/setup.bash" \
  /ros_entrypoint.sh

# run launch file
CMD ["ros2", "launch", "mmrs_controller", "mmrs_controller_launch.py"]
