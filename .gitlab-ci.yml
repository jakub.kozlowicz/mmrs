stages:
  - prepare
  - build
  - lint
  - test
  - release
  - deploy

variables:
  ROS_DISTRO: humble
  GIT_SUBMODULE_STRATEGY: recursive
  DEBIAN_FRONTEND: noninteracive

meta-build-image:
  image: docker:stable
  services:
    - docker:dind
  stage: prepare
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build --build-arg="ROS_DISTRO=${ROS_DISTRO}" -t ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/buildimage:latest -f .meta/Dockerfile .
    - docker push ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/buildimage:latest
  only:
    refs:
      - main
    changes:
      - .meta/Dockerfile

build:
  stage: build
  image: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/buildimage:latest
  before_script:
    - apt-get update -qq
    - rosdep install --from-paths src --ignore-src -r -y
    - source /opt/ros/${ROS_DISTRO}/setup.bash
  script:
    - colcon build
  artifacts:
    paths:
      - install
      - build
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

lint-pyright:
  stage: lint
  image: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/buildimage:latest
  needs:
    - job: build
      artifacts: true
  before_script:
    - python3 -m pip install basedpyright
    - npm i -g pyright-to-gitlab-ci
  script:
    - source install/setup.sh
    - basedpyright src --outputjson > report_raw.json
  after_script:
    - pyright-to-gitlab-ci --src report_raw.json --output report.json --base_path .
  artifacts:
    paths:
      - report.json
    reports:
      codequality: report.json
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

lint-ruff:
  stage: lint
  image: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/buildimage:latest
  needs:
    - job: build
      artifacts: true
  before_script:
    - python3 -m pip install -U ruff
  script:
    - source install/setup.sh
    - ruff check src --output-format gitlab > report.json
  artifacts:
    paths:
      - report.json
    reports:
      codequality: report.json
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

test:
  stage: test
  image: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/buildimage:latest
  needs:
    - job: build
      artifacts: true
  before_script:
    - apt-get update -qq
    - rosdep install --from-paths src --ignore-src -r -y
    - source /opt/ros/${ROS_DISTRO}/setup.bash
  script:
    - colcon test
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

release:
  stage: release
  image: docker:cli
  services:
    - name: docker:dind
      alias: docker
  variables:
    DOCKER_IMAGE_NAME: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build --pull -t "$DOCKER_IMAGE_NAME" .
    - docker push "$DOCKER_IMAGE_NAME"
    - |
      if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
        docker tag "$DOCKER_IMAGE_NAME" "$CI_REGISTRY_IMAGE:latest"
        docker push "$CI_REGISTRY_IMAGE:latest"
      fi
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

pages:
  image: ${CI_REGISTRY}/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}/buildimage:latest
  stage: deploy
  needs:
    - job: build
      artifacts: true
  before_script:
    - python3 -m pip install -U sphinx sphinx-rtd-theme
  script:
    - source install/setup.sh
    - sphinx-build -b html docs public
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
