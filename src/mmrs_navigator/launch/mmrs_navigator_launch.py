"""Launch navigator for a single robot in a multi mobile robot system."""

from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration, PathJoinSubstitution
from launch_ros.actions import Node
from launch_ros.substitutions import FindPackageShare


def generate_launch_description() -> LaunchDescription:
    """Generate launch description."""

    mmrs_navigator_dir = FindPackageShare("mmrs_navigator")

    namespace = LaunchConfiguration("namespace")

    x_pose = LaunchConfiguration("x_pose")
    y_pose = LaunchConfiguration("y_pose")
    z_pose = LaunchConfiguration("z_pose")
    x_orientation = LaunchConfiguration("x_orientation")
    y_orientation = LaunchConfiguration("y_orientation")
    z_orientation = LaunchConfiguration("z_orientation")
    w_orientation = LaunchConfiguration("w_orientation")

    navigator_params = LaunchConfiguration("navigator_params")

    return LaunchDescription(
        [
            DeclareLaunchArgument(
                "namespace",
                description="Namespace of the robot.",
            ),
            DeclareLaunchArgument(
                "x_pose",
                default_value="0.0",
                description="Initial x position of the robot.",
            ),
            DeclareLaunchArgument(
                "y_pose",
                default_value="0.0",
                description="Initial y position of the robot.",
            ),
            DeclareLaunchArgument(
                "z_pose",
                default_value="0.0",
                description="Initial yaw position of the robot.",
            ),
            DeclareLaunchArgument(
                "x_orientation",
                default_value="0.0",
                description="Initial x orientation of the robot.",
            ),
            DeclareLaunchArgument(
                "y_orientation",
                default_value="0.0",
                description="Initial y orientation of the robot.",
            ),
            DeclareLaunchArgument(
                "z_orientation",
                default_value="0.0",
                description="Initial z orientation of the robot.",
            ),
            DeclareLaunchArgument(
                "w_orientation",
                default_value="1.0",
                description="Initial w orientation of the robot.",
            ),
            DeclareLaunchArgument(
                "navigator_params",
                default_value=PathJoinSubstitution(
                    [mmrs_navigator_dir, "params", "navigator_params.yaml"],
                ),
                description="Path to the ROS2 parameters file to use for navigator",
            ),
            Node(
                package="mmrs_navigator",
                executable="navigator",
                name="navigator",
                namespace=namespace,
                parameters=[
                    navigator_params,
                    {
                        "initial_pose": {
                            "position": {
                                "x": x_pose,
                                "y": y_pose,
                                "z": z_pose,
                            },
                            "orientation": {
                                "x": x_orientation,
                                "y": y_orientation,
                                "z": z_orientation,
                                "w": w_orientation,
                            },
                        },
                    },
                ],
            ),
        ],
    )
