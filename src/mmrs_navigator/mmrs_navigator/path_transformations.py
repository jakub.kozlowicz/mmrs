"""Path transformations module."""

import itertools
import math

from geometry_msgs.msg import Point, PoseStamped, Quaternion
from nav_msgs.msg import Path


def calculate_orientation_to_next_point(
    current_point: Point,
    next_point: Point,
) -> Quaternion:
    """Calculate the orientation of the current point to the next point.

    :param current_point: The current point.
    :param next_point: The next point.
    :return: The orientation of the current point to the next point.
    """
    dx = next_point.x - current_point.x
    dy = next_point.y - current_point.y
    yaw = math.atan2(dy, dx)

    return Quaternion(
        x=0.0,
        y=0.0,
        z=float(math.sin(yaw / 2)),
        w=float(math.cos(yaw / 2)),
    )


def interpolate_path(path: Path, interpolation_distance: float) -> Path:
    """Interpolate the path to increase poses density.

    :param path: The path to interpolate.
    :param interpolation_distance: The maximum distance between poses.
    :return: The interpolated path.
    """
    interpolated_path = Path()
    interpolated_path.header = path.header
    interpolated_path.poses = []

    current_pose: PoseStamped
    next_pose: PoseStamped
    for current_pose, next_pose in itertools.pairwise(path.poses):
        dx = next_pose.pose.position.x - current_pose.pose.position.x
        dy = next_pose.pose.position.y - current_pose.pose.position.y

        distance = math.sqrt(dx**2 + dy**2)

        num_points_to_add = int(distance / interpolation_distance) + 1

        interpolated_path.poses.append(current_pose)

        for j in range(1, num_points_to_add):
            fraction = j / num_points_to_add
            new_pose = PoseStamped()
            new_pose.header = path.header
            new_pose.pose.position.x = current_pose.pose.position.x + dx * fraction
            new_pose.pose.position.y = current_pose.pose.position.y + dy * fraction
            new_pose.pose.orientation = calculate_orientation_to_next_point(
                current_pose.pose.position,
                new_pose.pose.position,
            )

            interpolated_path.poses.append(new_pose)

    interpolated_path.poses.append(path.poses[-1])  # type: ignore[type-var]

    return interpolated_path


def smooth_path_orientations(path: Path) -> Path:
    """Smooth the orientations of the path's poses.

    Iterates over the poses of the path and calculates the orientation to the next
    point, then updates the orientation of the current pose.

    :param path: The path to smooth.
    :return: The smoothed path.
    """
    current_pose: PoseStamped
    next_pose: PoseStamped
    for current_pose, next_pose in itertools.pairwise(path.poses):
        current_pose.pose.orientation = calculate_orientation_to_next_point(
            current_pose.pose.position,
            next_pose.pose.position,
        )

    return path
