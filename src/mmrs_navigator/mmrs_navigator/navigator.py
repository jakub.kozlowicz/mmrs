"""Navigator node for the multi mobile robot system.

Navigates single robot. Asks controller whether a robot can
move forward, or it should wait for another robot.
"""

import sys
import threading
import time
import typing

import rclpy
from action_msgs.msg import GoalStatus
from geometry_msgs.msg import PoseStamped, PoseWithCovarianceStamped
from mmrs_msgs.action import Navigate
from mmrs_msgs.msg import PathSegment, RobotMetadata
from mmrs_msgs.srv import (
    CalculatePath,
    CalculatePathThroughPoses,
    CanNextSegment,
    Register,
    ReleaseSegmentResources,
)
from nav2_simple_commander.robot_navigator import BasicNavigator
from nav_msgs.msg import Path
from rclpy.action import ActionServer, CancelResponse, GoalResponse
from rclpy.action.server import ServerGoalHandle
from rclpy.callback_groups import ReentrantCallbackGroup
from rclpy.executors import ExternalShutdownException, MultiThreadedExecutor
from rclpy.node import Node
from rclpy.parameter import Parameter
from rclpy.qos import (
    QoSDurabilityPolicy,
    QoSHistoryPolicy,
    QoSProfile,
    QoSReliabilityPolicy,
)
from unique_identifier_msgs.msg import UUID

from mmrs_navigator.path_transformations import (
    interpolate_path,
    smooth_path_orientations,
)

if typing.TYPE_CHECKING:
    from nav2_msgs.action import FollowPath


class NavigationError(Exception):
    """Base class for navigation errors."""


class NavigationRejectionError(NavigationError):
    """Error for rejected navigation."""


class Navigator(Node):
    """Navigator node for the multi mobile robot system."""

    def __init__(self, node_name: str = "navigator", namespace: str = "") -> None:
        """Initialize the navigator."""

        super().__init__(node_name=node_name, namespace=namespace)  # type: ignore[type]

        self.get_logger().debug("Starting navigator node.")

        self.declare_parameters(
            namespace="",
            parameters=[
                ("path_interpolation_step", Parameter.Type.DOUBLE),
                ("radius", Parameter.Type.DOUBLE),
                ("height", Parameter.Type.DOUBLE),
                ("max_speed", Parameter.Type.DOUBLE),
                ("initial_pose.header.frame_id", Parameter.Type.STRING),
                ("initial_pose.position.x", Parameter.Type.DOUBLE),
                ("initial_pose.position.y", Parameter.Type.DOUBLE),
                ("initial_pose.position.z", Parameter.Type.DOUBLE),
                ("initial_pose.orientation.x", Parameter.Type.DOUBLE),
                ("initial_pose.orientation.y", Parameter.Type.DOUBLE),
                ("initial_pose.orientation.z", Parameter.Type.DOUBLE),
                ("initial_pose.orientation.w", Parameter.Type.DOUBLE),
            ],
        )

        self.navigator = BasicNavigator(
            node_name="basic_navigator",
            namespace=self.get_namespace(),
        )

        self.path_interpolation_step = (
            self.get_parameter(
                "path_interpolation_step",
            )
            .get_parameter_value()
            .double_value
        )

        self.metadata = RobotMetadata(
            robotnamespace=self.get_namespace(),
            radius=self.get_parameter("radius").get_parameter_value().double_value,
            height=self.get_parameter("height").get_parameter_value().double_value,
            max_speed=self.get_parameter("max_speed")
            .get_parameter_value()
            .double_value,
        )

        self.position: PoseWithCovarianceStamped | None = None

        self.pose_subscription = self.create_subscription(
            PoseWithCovarianceStamped,
            "amcl_pose",
            self.update_position,
            QoSProfile(
                durability=QoSDurabilityPolicy.TRANSIENT_LOCAL,
                reliability=QoSReliabilityPolicy.RELIABLE,
                history=QoSHistoryPolicy.KEEP_LAST,
                depth=1,
            ),
        )

        self.calculate_path_service = self.create_service(
            CalculatePath,
            "mmrs/calculate_path_to_pose",
            self.calculate_path_to_pose_callback,
        )

        self.calculate_path_through_poses_service = self.create_service(
            CalculatePathThroughPoses,
            "mmrs/calculate_path_through_poses",
            self.calculate_path_through_poses_callback,
        )

        navigation_cb_group = ReentrantCallbackGroup()

        self.navigate_action_server = ActionServer(
            self,
            Navigate,
            "mmrs/navigate",
            execute_callback=self.navigate_execute_callback,
            goal_callback=self.navigate_goal_callback,
            cancel_callback=self.navigate_cancel_callback,
            callback_group=navigation_cb_group,
        )

        self.can_enter_next_segment_service_client = self.create_client(
            CanNextSegment,
            "/mmrs/can_next_segment",
            callback_group=navigation_cb_group,
        )

        self.release_segment_resources_service_client = self.create_client(
            ReleaseSegmentResources,
            "/mmrs/release_segment_resources",
            callback_group=navigation_cb_group,
        )

        self.alive_publisher = self.create_publisher(
            RobotMetadata,
            "/mmrs/alive",
            QoSProfile(
                durability=QoSDurabilityPolicy.TRANSIENT_LOCAL,
                reliability=QoSReliabilityPolicy.RELIABLE,
                history=QoSHistoryPolicy.KEEP_LAST,
                depth=1,
            ),
        )
        self.alive_timer = self.create_timer(25.0, self.publish_alive)

        self.register_in_controller(self.metadata)

        self.initial_pose = self._extract_initial_pose_from_params()
        self.navigator.setInitialPose(self.initial_pose)
        self.navigator.waitUntilNav2Active()

        self.get_logger().info("Navigator node has been initialized.")

    def _extract_initial_pose_from_params(self) -> PoseStamped:
        """Extract initial pose from parameters.

        :return: The initial pose.
        """
        initial_pose = PoseStamped()
        initial_pose.header.frame_id = (
            self.get_parameter("initial_pose.header.frame_id")
            .get_parameter_value()
            .string_value
        )
        initial_pose.header.stamp = self.get_clock().now().to_msg()
        initial_pose.pose.position.x = (
            self.get_parameter("initial_pose.position.x")
            .get_parameter_value()
            .double_value
        )
        initial_pose.pose.position.y = (
            self.get_parameter("initial_pose.position.y")
            .get_parameter_value()
            .double_value
        )
        initial_pose.pose.position.z = (
            self.get_parameter("initial_pose.position.z")
            .get_parameter_value()
            .double_value
        )
        initial_pose.pose.orientation.x = (
            self.get_parameter("initial_pose.orientation.x")
            .get_parameter_value()
            .double_value
        )
        initial_pose.pose.orientation.y = (
            self.get_parameter("initial_pose.orientation.y")
            .get_parameter_value()
            .double_value
        )
        initial_pose.pose.orientation.z = (
            self.get_parameter("initial_pose.orientation.z")
            .get_parameter_value()
            .double_value
        )
        initial_pose.pose.orientation.w = (
            self.get_parameter("initial_pose.orientation.w")
            .get_parameter_value()
            .double_value
        )

        return initial_pose

    def publish_alive(self) -> None:
        """Publish alive message."""
        self.alive_publisher.publish(self.metadata)

    def update_position(self, msg: PoseWithCovarianceStamped) -> None:
        """Update the robot position.
        :param msg: The message with the pose.
        """
        self.position = msg

    def register_in_controller(self, metadata: RobotMetadata) -> None:
        """Register robot in centralized controller.

        :param namespace: The namespace of the robot.
        :param metadata: The metadata of the robot.
        """
        client = self.create_client(
            Register,
            "/mmrs/register",
        )

        self.get_logger().debug("Waiting for the 'Register' service...")
        while not client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info(
                "'Register' service not available, waiting again...",
            )
            time.sleep(1)

        request = Register.Request()
        request.metadata = metadata

        result_code = -1
        while result_code != 0:
            future = client.call_async(request)
            rclpy.spin_until_future_complete(self, future)

            result = future.result()
            if result is not None:
                result_code = result.result_code

            time.sleep(2)

    def calculate_path_to_pose_callback(
        self,
        request: CalculatePath.Request,
        response: CalculatePath.Response,
    ) -> CalculatePath.Response:
        """Calculate a path to a pose's callback.

        :param request: The request to calculate a path to a pose.
        :param response: The response of the service.
        :return: The response of the service.
        """
        self.get_logger().info(
            f"Received request to calculate path to pose: {request.goal.pose.position}",
        )

        if self.position is None:
            self.get_logger().error("No current position available.")
            return response

        start = PoseStamped()
        start.header = self.position.header
        start.pose = self.position.pose.pose

        path = self.navigator.getPath(
            start=start,
            goal=request.goal,
            use_start=True,
        )

        self.get_logger().debug(f"Calculated path: {path}")

        if path is None:
            self.get_logger().error("Failed to calculate path.")
            return response

        response.path = self.interpolate_and_smooth_path(
            path,
            interpolation_distance=self.path_interpolation_step,
        )
        return response

    def calculate_path_through_poses_callback(
        self,
        request: CalculatePathThroughPoses.Request,
        response: CalculatePathThroughPoses.Response,
    ) -> CalculatePathThroughPoses.Response:
        """Calculate a path through poses' callback.

        :param request: The request to calculate a path through poses.
        :param response: The response of the service.
        :return: The response of the service.
        """
        self.get_logger().info(
            f"Received request to calculate path through poses: {request.goals}",
        )

        if self.position is None:
            self.get_logger().error("No current position available.")
            return response

        start = PoseStamped()
        start.header = self.position.header
        start.pose = self.position.pose.pose

        path = self.navigator.getPathThroughPoses(
            start=start,
            goals=request.goals,
            use_start=True,
        )

        self.get_logger().debug(f"Calculated path: {path}")

        if path is None:
            self.get_logger().error("Failed to calculate path.")
            return response

        response.path = self.interpolate_and_smooth_path(
            path,
            interpolation_distance=self.path_interpolation_step,
        )
        return response

    def interpolate_and_smooth_path(
        self,
        path: Path,
        *,
        interpolation_distance: float,
    ) -> Path:
        """Interpolate and smooth path orientations.

        :param path: The path to be interpolated and smoothed.
        :param interpolation_distance: The maximum distance between points.
        :return: The interpolated and smoothed path.
        """
        path = interpolate_path(path, interpolation_distance)
        self.get_logger().info("Interpolated path")

        path = smooth_path_orientations(path)
        self.get_logger().info("Smoothed path orientations.")

        return path

    def navigate_goal_callback(self, goal: Navigate.Goal) -> GoalResponse:
        """Navigate through poses' goal callback.

        Always accepts the goal.

        :param goal: The goal handle to accept or reject.
        :return: The response to the goal.
        """
        self.get_logger().debug(f"Accepting goal request {goal}")
        return GoalResponse.ACCEPT

    def navigate_cancel_callback(self, goal_handle: ServerGoalHandle) -> CancelResponse:
        """Navigate through poses' cancel callback.

        :param goal_handle: The goal handle to cancel.
        :return: The response to the cancel request.
        """
        self.get_logger().info(f"Cancelling goal for action {goal_handle.goal_id}")
        return CancelResponse.ACCEPT

    def navigate_execute_callback(
        self,
        goal_handle: ServerGoalHandle,
    ) -> Navigate.Result:
        """The execute callback for the navigate action.

        :param goal_handle: The goal handle for the navigate action.
        :return: The result of the navigate action.
        """
        goal_pose: PoseStamped = goal_handle.request.goal_pose
        segments: list[PathSegment] = goal_handle.request.path_segments

        self.get_logger().info(
            f"Navigating to final pose {goal_pose.pose.position}.",
        )

        result = Navigate.Result()
        nav_result = GoalStatus.STATUS_EXECUTING

        try:
            for idx in range(len(segments) + 1):
                # Before first segment
                if idx == 0:
                    current_segment = segments[idx]
                    self._wait_for_permission(
                        previous_id=UUID(),
                        current_id=current_segment.uuid,
                    )

                    nav_result = self._navigate_single_segment(
                        goal_handle=goal_handle,
                        segment=current_segment,
                    )

                # After last segment
                elif idx == len(segments):
                    previous_segment = segments[idx - 1]
                    self.get_logger().info("Robot is finilazing navigation")
                    self._release_resources_sync(previous_segment)

                # Normal segments traversing
                else:
                    previous_segment = segments[idx - 1]
                    current_segment = segments[idx]

                    self._wait_for_permission(
                        previous_id=previous_segment.uuid,
                        current_id=current_segment.uuid,
                    )

                    nav_result = self._navigate_single_segment(
                        goal_handle=goal_handle,
                        segment=current_segment,
                    )
        except NavigationError as exc:
            self.get_logger().error(
                f"Exception in navigate_through_poses_callback: {exc!s}"
            )
            goal_handle.abort()
            result.result_code = GoalStatus.STATUS_ABORTED
            return result

        if nav_result == GoalStatus.STATUS_ABORTED:
            self.get_logger().error("Failed to navigate robot to goal.")
            goal_handle.abort()
            result.result_code = nav_result

        elif nav_result == GoalStatus.STATUS_CANCELED:
            self.get_logger().info("Cancelled navigation to goal.")
            goal_handle.canceled()
            result.result_code = nav_result

        elif nav_result == GoalStatus.STATUS_SUCCEEDED:
            self.get_logger().info("Successfully navigated robot to goal.")
            goal_handle.succeed()
            result.result_code = self.navigator.getResult().value

        else:
            self.get_logger().error(f"Unknown navigation result: {nav_result}.")
            goal_handle.abort()
            result.result_code = GoalStatus.STATUS_ABORTED

        return result

    def _navigate_single_segment(
        self,
        goal_handle: ServerGoalHandle,
        segment: PathSegment,
    ) -> int:
        """Navigate robot through a single segment.

        :param goal_handle: The goal handle for the navigate action.
        :param segment: The segment to navigate through.
        :return: The result of the navigation.
        """
        self.get_logger().info(
            f"Starting navigation through segment {segment.uuid.uuid}."
        )

        if not self.navigator.followPath(segment.path):
            msg = "Navigator rejected 'FollowPath' action."
            self.get_logger().error(msg)
            raise NavigationRejectionError(msg)

        while (
            not self.navigator.isTaskComplete() and not goal_handle.is_cancel_requested
        ):
            feedback: FollowPath.Feedback | None = self.navigator.getFeedback()
            if not feedback:
                self.get_logger().warning("No feedback received.")
                continue

            distance_to_goal = feedback.distance_to_goal
            self.get_logger().debug(f"Distance to goal: {distance_to_goal}")

            nav_feedback = Navigate.Feedback()
            nav_feedback.distance_to_goal = distance_to_goal
            goal_handle.publish_feedback(nav_feedback)

        if goal_handle.is_cancel_requested:
            self.navigator.cancelTask()
            return GoalStatus.STATUS_CANCELED

        self.get_logger().info(
            f"Robot successfully navigated through segment {segment.uuid.uuid}."
        )

        return GoalStatus.STATUS_SUCCEEDED

    def _wait_for_permission(
        self,
        previous_id: UUID,
        current_id: UUID,
    ) -> None:
        """Wait for permission to enter next segment.

        :param previous_id: ID of the previous segment.
        :param current_id: ID of the current segment.
        """
        while True:
            if self._ask_for_permission_sync(
                previous_id=previous_id,
                current_id=current_id,
                first=all(previous_id.uuid == UUID().uuid),
            ):
                break

            time.sleep(1)

        self.get_logger().info(f"Robot can enter next segment {current_id}.")

    def _ask_for_permission_async(
        self,
        previous_id: UUID,
        current_id: UUID,
        *,
        first: bool = False,
    ) -> rclpy.Future:
        """Ask for permission to enter next segment.

        :param prevoius_id: The ID of the previous segment.
        :param current_id: The ID of the current segment.
        :param first: Whether this is the first segment.
        :return: The future of the service call.
        """
        self.get_logger().debug("Waiting for the 'CanNextSegment' service...")
        while not self.can_enter_next_segment_service_client.wait_for_service(
            timeout_sec=1.0,
        ):
            self.get_logger().info(
                "'CanNextSegment' service not available, waiting again...",
            )

        self.get_logger().debug(
            f"Asking for permission to enter next segment {current_id}."
        )

        request = CanNextSegment.Request(
            robot_namespace=self.get_namespace(),
            current_id=previous_id,
            next_id=current_id,
            first=first,
        )

        return self.can_enter_next_segment_service_client.call_async(request)

    def _ask_for_permission_sync(
        self,
        previous_id: UUID,
        current_id: UUID,
        *,
        first: bool = False,
    ) -> bool:
        """Ask for permission to enter next segment.

        :param namespace: The namespace of the robot.
        :param previous_id: The ID of the previous segment.
        :param current_id: The ID of the current segment.
        :return: True if permission is granted, False if not.
        """
        event = threading.Event()

        def done_callback(_: rclpy.Future) -> None:
            nonlocal event
            event.set()

        future = self._ask_for_permission_async(
            previous_id=previous_id,
            current_id=current_id,
            first=first,
        )
        future.add_done_callback(done_callback)

        event.wait()

        result = future.result()
        return result.result if result is not None else False

    def _release_resources_async(self, segment: PathSegment) -> rclpy.Future:
        """Release resources after navigating through a segment.

        :param segment: The segment that was navigated through.
        """
        while not self.release_segment_resources_service_client.wait_for_service(
            timeout_sec=1.0,
        ):
            self.get_logger().info(
                "'ReleaseSegmentResources' service not available, waiting again...",
            )

        self.get_logger().debug(
            f"Releasing resources for segment {segment.uuid.uuid}.",
        )

        request = ReleaseSegmentResources.Request(
            robot_namespace=self.get_namespace(),
            segment_id=segment.uuid,
        )

        return self.release_segment_resources_service_client.call_async(request)

    def _release_resources_sync(self, segment: PathSegment) -> None:
        """Release resources after navigating through a segment.

        :param segment: The segment that was navigated through.
        """
        event = threading.Event()

        def done_callback(_: rclpy.Future) -> None:
            nonlocal event
            event.set()

        future = self._release_resources_async(segment)
        future.add_done_callback(done_callback)

        event.wait()


def main(args: list[str] | None = None) -> None:
    """Run navigator node."""
    rclpy.init(args=args)
    node = Navigator()
    executor = MultiThreadedExecutor()
    try:
        rclpy.spin(node, executor=executor)
    except KeyboardInterrupt:
        pass
    except ExternalShutdownException:
        sys.exit(1)
    finally:
        rclpy.try_shutdown()
        node.destroy_node()


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
