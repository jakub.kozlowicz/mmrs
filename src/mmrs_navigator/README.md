# mmrs_navigator

The `mmrs_navigator` package is a high-level navigator using
`nav2_simple_commander` to navigate robot using a calculated path
that navigate segment by segment and ask for permission to enter
next segment.