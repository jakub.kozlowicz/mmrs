"""Controller node for multi mobile robot system."""

import functools
import sys
import threading
import typing
from collections.abc import Sequence

import rclpy
from geometry_msgs.msg import PoseStamped
from mmrs_msgs.action import Navigate
from mmrs_msgs.msg import DividedPath, Mission, PathSegment, PathToDivide, RobotMetadata
from mmrs_msgs.srv import (
    AddRobotClearance,
    CalculatePath,
    CalculatePathThroughPoses,
    DividePaths,
    Register,
    RegisterDividedPaths,
    RemoveRobotClearance,
)
from rclpy.action import ActionClient
from rclpy.callback_groups import ReentrantCallbackGroup
from rclpy.duration import Duration
from rclpy.executors import ExternalShutdownException, MultiThreadedExecutor
from rclpy.node import Node
from rclpy.task import Future

if typing.TYPE_CHECKING:
    from nav_msgs.msg import Path
    from rclpy.client import Client
    from rclpy.clock import Time


class Controller(Node):
    """Controller node for multi mobile robot system."""

    NOT_ALIVE_THRESHOLD_S = 30

    def __init__(self) -> None:
        """Create controller node."""
        super().__init__(node_name="controller")  # type: ignore[type-var]
        self.get_logger().debug("Starting controller node.")

        self._robots: dict[str, Time] = {}

        self._registered_paths: dict[str, Path] = {}

        self._calculate_path_to_pose_clients: dict[str, Client] = {}
        self._calculate_path_through_poses_clients: dict[str, Client] = {}
        self._calculate_path_calls_futures: dict[str, Future] = {}

        self._navigate_action_clients: dict[str, ActionClient] = {}
        self._navigate_action_send_goals_futures: dict[str, Future] = {}
        self._navigate_action_result_futures: dict[str, Future] = {}

        self.register_cb_group = ReentrantCallbackGroup()

        self.register_service = self.create_service(
            Register,
            "mmrs/register",
            self.register_robot_in_registry,
            callback_group=self.register_cb_group,
        )

        self.add_robot_clearance_client = self.create_client(
            AddRobotClearance,
            "mmrs/add_robot_clearance",
            callback_group=self.register_cb_group,
        )

        self.remove_robot_clearance_client = self.create_client(
            RemoveRobotClearance,
            "mmrs/remove_robot_clearance",
            callback_group=self.register_cb_group,
        )

        self.divide_paths_client = self.create_client(
            DividePaths,
            "mmrs/divide_paths",
        )

        self.register_divided_paths_client = self.create_client(
            RegisterDividedPaths,
            "mmrs/register_divided_paths",
        )

        self.mission_subscription = self.create_subscription(
            Mission,
            "mmrs/mission",
            self.mission_msg_callback,
            10,
        )

        self.navigation_timer = self.create_timer(
            1.0,
            self.divide_paths,
        )

        self.alive_subscription = self.create_subscription(
            RobotMetadata,
            "mmrs/alive",
            self.alive_msg_callback,
            10,
        )

        self.alive_timer = self.create_timer(
            self.NOT_ALIVE_THRESHOLD_S,
            self.alive_timer_callback,
        )

        self.get_logger().info("Controller node has been initialized.")

    def register_robot_in_registry(
        self,
        request: Register.Request,
        response: Register.Response,
    ) -> Register.Response:
        """Registers robot in registry with metadata. Create all clients for robot
        control.

        :param request: The request to register robot.
        :type request: Register.Request
        :param response: The response of the registration in the registry.
        :type response: Register.Response
        :return: The response of the registration in registry.
        :rtype: Register.Response
        """
        self.get_logger().debug(
            f"Received request to register robot '{request.metadata.robotnamespace}'"
        )

        while not self.add_robot_clearance_client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info(
                "Service 'add_robot_clearance' is not available. Waiting...",
            )

        event = threading.Event()

        def done_callback(_: rclpy.Future) -> None:
            nonlocal event
            event.set()

        self.get_logger().info(
            f"Adding robot '{request.metadata.robotnamespace}' clearance"
        )
        clearance_request = AddRobotClearance.Request()
        clearance_request.robot_namespace = request.metadata.robotnamespace
        clearance_request.clearance = request.metadata.radius
        future = self.add_robot_clearance_client.call_async(clearance_request)
        future.add_done_callback(done_callback)

        event.wait()

        result = future.result()
        result_code = result.result_code if result is not None else -1

        if result_code != 0:
            self.get_logger().error(
                f"Failed to add clearance for '{request.metadata.robotnamespace}'",
            )
            response.result_code = result_code
            return response

        self._robots.update({request.metadata.robotnamespace: self.get_clock().now()})

        self._calculate_path_to_pose_clients.update(
            {
                request.metadata.robotnamespace: self.create_client(
                    CalculatePath,
                    f"{request.metadata.robotnamespace}/mmrs/calculate_path_to_pose",
                ),
            },
        )

        self._calculate_path_through_poses_clients.update(
            {
                request.metadata.robotnamespace: self.create_client(
                    CalculatePathThroughPoses,
                    f"{request.metadata.robotnamespace}/mmrs/calculate_path_through_poses",
                ),
            },
        )

        self._navigate_action_clients.update(
            {
                request.metadata.robotnamespace: ActionClient(
                    self,
                    Navigate,
                    f"{request.metadata.robotnamespace}/mmrs/navigate",
                ),
            },
        )

        self.get_logger().info(
            f"Robot '{request.metadata.robotnamespace}' has been registered.",
        )

        response.result_code = 0
        return response

    def alive_timer_callback(self) -> None:
        """Check if robots are alive."""
        event = threading.Event()

        def done_callback(_: rclpy.Future) -> None:
            nonlocal event
            event.set()

        while not self.add_robot_clearance_client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info(
                "Service 'add_robot_clearance' is not available. Waiting...",
            )

        to_remove = set()
        for robot_namespace, last_seen in self._robots.items():
            threshold = Duration(seconds=self.NOT_ALIVE_THRESHOLD_S)
            if self.get_clock().now() - last_seen < threshold:
                continue

            self.get_logger().warning(
                f"Robot '{robot_namespace}' is not alive. Removing from registry.",
            )

            request = RemoveRobotClearance.Request()
            request.robot_namespace = robot_namespace

            future = self.remove_robot_clearance_client.call_async(request)
            future.add_done_callback(done_callback)

            event.wait()

            result = future.result()
            result_code = result.result_code if result is not None else -1

            if result_code != 0:
                self.get_logger().error(
                    f"Failed to remove robot '{robot_namespace}' from registry",
                )
                continue

            to_remove.add(robot_namespace)

            event.clear()

        for robot_namespace in to_remove:
            self._robots.pop(robot_namespace, None)
            self._registered_paths.pop(robot_namespace, None)
            self._calculate_path_to_pose_clients.pop(robot_namespace, None)
            self._calculate_path_through_poses_clients.pop(robot_namespace, None)
            self._calculate_path_calls_futures.pop(robot_namespace, None)

            self._navigate_action_clients.pop(robot_namespace, None)
            self._navigate_action_send_goals_futures.pop(robot_namespace, None)
            self._navigate_action_result_futures.pop(robot_namespace, None)

    def alive_msg_callback(self, msg: RobotMetadata) -> None:
        """Callback for the alive message.

        :param msg: The alive message with metadata.
        :type msg: RobotMetadata
        """
        self.get_logger().debug(f"Received alive message for {msg.robotnamespace}")
        self._robots[msg.robotnamespace] = self.get_clock().now()

    def mission_msg_callback(self, msg: Mission) -> None:
        """Callback for the mission message.

        Navigates the robot to pose or through poses.

        :param msg: The mission message with goal.
        :type msg: Mission
        """
        self.get_logger().info(f"Received mission message for {msg.robot_namespace}")

        poses = list(msg.path.poses)

        if msg.robot_namespace not in self._robots:
            self.get_logger().warning(
                "Mission message for not registered robot. Dropping.",
            )
            return

        if len(poses) == 1:
            self.request_path_to_pose(
                poses[0],
                robot_namespace=msg.robot_namespace,
            )
        else:
            self.request_path_through_poses(
                poses,
                robot_namespace=msg.robot_namespace,
            )

    def request_path_to_pose(
        self,
        pose: PoseStamped,
        *,
        robot_namespace: str,
    ) -> None:
        """Request a path to pose from the navigator.

        :param robot_namespace: The namespace of the robot.
        :type robot_namespace: str
        :param pose: The pose to navigate to.
        :type pose: PoseStamped
        """
        self.get_logger().info(
            f"Requesting path to pose for robot '{robot_namespace}': "
            f"'{pose.pose.position}'",
        )

        request = CalculatePath.Request()
        request.robot_namespace = robot_namespace
        request.goal = pose

        while not self._calculate_path_to_pose_clients[
            robot_namespace
        ].wait_for_service(
            timeout_sec=1.0,
        ):
            self.get_logger().info(
                f"Service 'calculate_path_to_pose' for '{robot_namespace}' is not "
                "available. Waiting...",
            )

        self._calculate_path_calls_futures[robot_namespace] = (
            self._calculate_path_to_pose_clients[robot_namespace].call_async(
                request,
            )
        )
        self._calculate_path_calls_futures[robot_namespace].add_done_callback(
            functools.partial(
                self.register_calculated_robot_path,
                robot_namespace=robot_namespace,
            ),
        )

    def request_path_through_poses(
        self,
        poses: list[PoseStamped],
        *,
        robot_namespace: str,
    ) -> None:
        """Request a path through poses from the navigator.

        :param robot_namespace: The namespace of the robot.
        :type robot_namespace: str
        :param poses: The poses to navigate through.
        :type poses: list[PoseStamped]
        """
        _poses = [pose.pose.position for pose in poses]
        self.get_logger().info(
            f"Requesting path through poses for robot '{robot_namespace}': "
            f"'{_poses}'",
        )

        request = CalculatePathThroughPoses.Request()
        request.robot_namespace = robot_namespace
        request.goals = poses

        while not self._calculate_path_through_poses_clients[
            robot_namespace
        ].wait_for_service(
            timeout_sec=1.0,
        ):
            self.get_logger().info(
                f"Service 'calculate_path_through_poses' for '{robot_namespace}' is "
                "not available. Waiting...",
            )

        self._calculate_path_calls_futures[robot_namespace] = (
            self._calculate_path_through_poses_clients[
                robot_namespace
            ].call_async(request)
        )
        self._calculate_path_calls_futures[robot_namespace].add_done_callback(
            functools.partial(
                self.register_calculated_robot_path,
                robot_namespace=robot_namespace,
            ),
        )

    def register_calculated_robot_path(
        self,
        future: Future,
        *,
        robot_namespace: str,
    ) -> None:
        """Navigate robot to the goal.

        :param future: The future of the service call that requests path calculation.
        :type future: Future
        :param robot_namespace: The namespace of the robot.
        :type robot_namespace: str
        """
        response = future.result()

        if response is None:
            self.get_logger().error(
                f"Failed to receive response for robot '{robot_namespace}'",
            )
            return

        path = response.path

        self.get_logger().debug(f"Path obtained from the navigator: {path}")

        try:
            self._calculate_path_calls_futures.pop(robot_namespace)
        except KeyError:
            self.get_logger().error(
                f"Failed to remove done future for {robot_namespace} "
                "after path calulation.",
            )

        self._registered_paths[robot_namespace] = path

        self.get_logger().info(f"Path registered for robot '{robot_namespace}'")

    def divide_paths(self) -> None:
        """Send navigation goals to robots."""

        if (
            len(self._robots) != len(self._registered_paths)
            or not self._robots
            or not self._registered_paths
        ):
            self.get_logger().debug(
                "Not all robots have paths. Skipping sending navigation goals.",
            )
            return

        self.get_logger().info("Dividing paths, as all robots have them defined.")

        request = DividePaths.Request()
        request.paths = [
            PathToDivide(robot_namespace=robot, path=path)
            for robot, path in self._registered_paths.items()
        ]

        while not self.divide_paths_client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info(
                "Service 'divide_paths' is not available. Waiting...",
            )

        self._divide_path_feature = self.divide_paths_client.call_async(request)
        self._divide_path_feature.add_done_callback(self.divided_paths_callback)

    def divided_paths_callback(self, future: Future) -> None:
        self._registered_paths.clear()
        response: DividePaths.Response | None = future.result()
        if response is None:
            self.get_logger().error("Failed to divide paths.")
            return

        self._divided_paths = response.paths

        request = RegisterDividedPaths.Request()
        request.paths = response.paths

        while not self.register_divided_paths_client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info(
                "Service 'register_divided_paths' is not available. Waiting...",
            )

        self._register_divided_paths_future = (
            self.register_divided_paths_client.call_async(request)
        )
        self._register_divided_paths_future.add_done_callback(
            self.register_divided_paths_callback
        )

    def register_divided_paths_callback(self, future: Future) -> None:
        response: RegisterDividedPaths.Response | None = future.result()
        if response is None:
            self.get_logger().error("Failed to register divided paths.")
            return

        if not self._divided_paths:
            self.get_logger().warning("No divided paths to send.")
            return

        divided_path: DividedPath
        for divided_path in self._divided_paths:
            self._send_one_navigation_goal(
                divided_path.robot_namespace,
                list(divided_path.segments),
            )

    def _send_one_navigation_goal(
        self,
        robot_namespace: str,
        segments: Sequence[PathSegment],
    ) -> None:
        """Send navigation goal to robot.

        :param robot_namespace: The namespace of the robot.
        :type robot_namespace: str
        :param segments: The segments of the path to send to the robot.
        :type segments: Sequence[PathSegment]
        """
        self.get_logger().debug(f"Sending navigation goal to robot '{robot_namespace}'")

        goal_msg = Navigate.Goal()
        goal_msg.robot_namespace = robot_namespace
        goal_msg.goal_pose = segments[-1].path.poses[-1]  # type: ignore[index]
        goal_msg.path_segments = segments

        self._navigate_action_send_goals_futures[robot_namespace] = (
            self._navigate_action_clients[robot_namespace].send_goal_async(goal_msg)
        )
        self._navigate_action_send_goals_futures[robot_namespace].add_done_callback(
            functools.partial(
                self._navigate_action_goal_response_callback,
                robot_namespace=robot_namespace,
            ),
        )

    def _navigate_action_goal_response_callback(
        self,
        future: Future,
        *,
        robot_namespace: str,
    ) -> None:
        """Navigate action goal handle callback.

        :param future: The result of the action request. It is a goal handle.
        :type future: Future
        :param robot_namespace: The namespace of the robot.
        :type robot_namespace: str
        """
        goal_handle = future.result()

        if not goal_handle:
            self.get_logger().error(
                f"Goal handle for robot '{robot_namespace}' is invalid",
            )
            return

        if not goal_handle.accepted:
            self.get_logger().error(
                f"Goal rejected for robot '{robot_namespace}': {goal_handle.accepted}",
            )
            return

        self.get_logger().info(
            f"Goal accepted for robot '{robot_namespace}': {goal_handle.accepted}",
        )

        self._navigate_action_result_futures[robot_namespace] = (
            goal_handle.get_result_async()
        )
        self._navigate_action_result_futures[robot_namespace].add_done_callback(
            functools.partial(
                self._navigate_action_result_callback,
                robot_namespace=robot_namespace,
            ),
        )

    def _navigate_action_result_callback(
        self,
        future: Future,
        *,
        robot_namespace: str,
    ) -> None:
        """Navigate action result handle callback.

        :param future: The result of the action request. It is a result of the action.
        :type future: Future
        :param robot_namespace: The namespace of the robot.
        :type robot_namespace: str
        """
        result = future.result()

        if result is None:
            self.get_logger().error(
                f"Failed to receive result for robot '{robot_namespace}'",
            )
            return

        self.get_logger().info(
            f"Received result for robot '{robot_namespace}': "
            f"{result.result.result_code}",
        )

        try:
            self._navigate_action_send_goals_futures.pop(robot_namespace)
            self._navigate_action_result_futures.pop(robot_namespace)
        except KeyError:
            self.get_logger().warning(
                f"Failed to remove futures for {robot_namespace}. Ignoring.",
            )


def main(args: list[str] | None = None) -> None:
    """Run controller node."""
    rclpy.init(args=args)
    node = Controller()
    executor = MultiThreadedExecutor()
    try:
        rclpy.spin(node, executor=executor)
    except KeyboardInterrupt:
        pass
    except ExternalShutdownException:
        sys.exit(1)
    finally:
        rclpy.try_shutdown()
        node.destroy_node()
