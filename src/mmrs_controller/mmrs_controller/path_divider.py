r"""Path divider node for the MMRS controller.

The path divider node is responsible for dividing the paths into segments and
computing the required resources for each segment.
"""

import sys

import rclpy
from mmrs_msgs.msg import DividedPath
from mmrs_msgs.srv import AddRobotClearance, DividePaths, RemoveRobotClearance
from rclpy.executors import ExternalShutdownException
from rclpy.node import Node
from rclpy.parameter import Parameter

from mmrs_controller.path.dividers.equal_length_divider import EqualLengthPathDivider
from mmrs_controller.path.dividers.grid_cells_divider import GridCellsPathDivider
from mmrs_controller.path.dividers.optimal_length_divider import (
    OptimalLengthPathDivider,
)
from mmrs_controller.path.dividers.path_divider import PathDivider as PathDividerImpl


class PathDivider(Node):
    """Path divider node for the MMRS controller."""

    def __init__(self) -> None:
        """Create controller node."""
        super().__init__(node_name="path_divider")  # type: ignore[type-var]
        self.get_logger().debug("Starting path_divider node.")

        self.declare_parameters(
            namespace="",
            parameters=[
                ("divider", Parameter.Type.STRING),
                ("size_m", Parameter.Type.DOUBLE),
                ("grid_size_m", Parameter.Type.DOUBLE),
                ("circle_radius_m", Parameter.Type.DOUBLE),
                ("segment_length_m", Parameter.Type.DOUBLE),
            ],
        )

        self._divider = self._create_divider_from_params()

        self.add_robot_clearance_service = self.create_service(
            AddRobotClearance,
            "mmrs/add_robot_clearance",
            self.add_robot_clearance_callback,
        )

        self.remove_robot_clearance_service = self.create_service(
            RemoveRobotClearance,
            "mmrs/remove_robot_clearance",
            self.remove_robot_clearance_callback,
        )

        self.divide_paths_service = self.create_service(
            DividePaths,
            "mmrs/divide_paths",
            self.divide_paths_service_callback,
        )

        self.get_logger().info("Path divider node started.")

    def add_robot_clearance_callback(
        self,
        request: AddRobotClearance.Request,
        response: AddRobotClearance.Response,
    ) -> AddRobotClearance.Response:
        self.get_logger().debug("Received request to add robot clearance.")
        self._divider.add_robot_clearence(
            robot_namespace=request.robot_namespace,
            clearance=request.clearance,
        )
        self.get_logger().info(f"Robot clearance added for {request.robot_namespace}")
        response.result_code = 0
        return response

    def remove_robot_clearance_callback(
        self,
        request: RemoveRobotClearance.Request,
        response: RemoveRobotClearance.Response,
    ) -> RemoveRobotClearance.Response:
        self.get_logger().debug("Received request to remove robot clearance.")
        self._divider.remove_robot_clearance(robot_namespace=request.robot_namespace)
        self.get_logger().info(f"Robot clearance removed for {request.robot_namespace}")
        response.result_code = 0
        return response

    def divide_paths_service_callback(
        self,
        request: DividePaths.Request,
        response: DividePaths.Response,
    ) -> DividePaths.Response:
        self.get_logger().debug("Received request to divide paths.")

        _paths = {path.robot_namespace: path.path for path in request.paths}
        divided = self._divider.divide(_paths)

        response.paths = [
            DividedPath(
                robot_namespace=robot_namespace,
                segments=[segment.to_msg() for segment in segments],
            )
            for robot_namespace, segments in divided.items()
        ]

        self.get_logger().info("Successfully divided paths.")

        return response

    def _create_divider_from_params(self) -> PathDividerImpl:
        divider_param = self.get_parameter("divider").get_parameter_value().string_value

        if divider_param == "equal_length":
            self.get_logger().info("Using equal length path divider.")
            divider = EqualLengthPathDivider(
                segment_length=self.get_parameter("segment_length_m")
                .get_parameter_value()
                .double_value
            )
        elif divider_param == "optimal_length":
            self.get_logger().info("Using optimal length path divider.")
            divider = OptimalLengthPathDivider()

        elif divider_param == "grid_based":
            self.get_logger().info("Using grid based path divider.")
            divider = GridCellsPathDivider(
                size_m=self.get_parameter("size_m").get_parameter_value().double_value,
                grid_size_m=self.get_parameter("grid_size_m")
                .get_parameter_value()
                .double_value,
                circle_radius_m=self.get_parameter("circle_radius_m")
                .get_parameter_value()
                .double_value,
            )
        else:
            msg = (
                "Invalid path divider. Use one of the following: "
                "'equal_length', 'optimal_length', 'grid_based'."
            )
            self.get_logger().error(msg)
            raise ValueError(msg)

        return divider


def main(args: list[str] | None = None) -> None:
    """Run controller node."""
    rclpy.init(args=args)
    node = PathDivider()
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        pass
    except ExternalShutdownException:
        sys.exit(1)
    finally:
        rclpy.try_shutdown()
        node.destroy_node()
