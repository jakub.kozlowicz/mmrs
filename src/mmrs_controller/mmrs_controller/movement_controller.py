r"""Movement controller for the robots.

The movement controller is responsible for managing the movement of the robots. It
checks if the robot can proceed to the next segment without collision and deadlock.

The movement controller provides the following services:
- `mmrs/can_next_segment`: Check if the robot can proceed to the next segment.
- `mmrs/register_divided_paths`: Register the divided paths.
- `mmrs/release_segment_resources`: Release the resources for the segment.

The movement controller uses the `ResourcesManager` to manage the resources for the
robots. The `ResourcesManager` keeps track of the resources for the segments, the
occupied resources for the robots, the maximum demand of the resources for the robots,
and the allocation of the resources for the robots.
"""

import sys
import traceback
from uuid import UUID

import rclpy
from mmrs_msgs.srv import CanNextSegment, RegisterDividedPaths, ReleaseSegmentResources
from rclpy.executors import ExternalShutdownException
from rclpy.node import Node

from mmrs_controller.resources.manager import ResourcesManager


class MovementController(Node):
    """Movement controller for the robots."""

    def __init__(self) -> None:
        """Initialize the movement controller."""
        super().__init__(node_name="movement_controller")  # type: ignore[type-var]
        self.get_logger().debug("Starting movement controller node.")

        self._resource_mgn = ResourcesManager()

        self.can_next_segment_service = self.create_service(
            CanNextSegment,
            "mmrs/can_next_segment",
            self.can_next_segment_callback,
        )

        self.register_divided_paths_service = self.create_service(
            RegisterDividedPaths,
            "mmrs/register_divided_paths",
            self.register_divided_paths_callback,
        )

        self.release_segment_resources_service = self.create_service(
            ReleaseSegmentResources,
            "mmrs/release_segment_resources",
            self.release_segment_resources_callback,
        )

        self.get_logger().info("Movement controller node has been initialized.")

    def can_next_segment_callback(
        self,
        request: CanNextSegment.Request,
        response: CanNextSegment.Response,
    ) -> CanNextSegment.Response:
        """Check if the robot can proceed to the next segment.

        :param request: The request to check if the robot can proceed to the next
           segment.
        :param response: The response to the request to check if the robot can proceed
           to the next segment.
        :return: The response to the request to check if the robot can proceed to the
           next segment.
        """
        self.get_logger().info(
            f"Received request to check if the robot '{request.robot_namespace}' can "
            "proceed to the next segment."
        )

        robot_id = request.robot_namespace
        current_id = (
            UUID(bytes=bytes(request.current_id.uuid)) if not request.first else None
        )
        next_id = UUID(bytes=bytes(request.next_id.uuid))

        try:
            if self._resource_mgn.check_collision(robot_id, current_id, next_id):
                self.get_logger().error(
                    f"'{robot_id}' cannot proceed to the next segment due to collision."
                )
                response.result = False
            elif self._resource_mgn.check_deadlock(robot_id, current_id, next_id):
                self.get_logger().error(
                    f"'{robot_id}' cannot proceed to the next segment due to possible "
                    "deadlock."
                )
                response.result = False
            else:
                self.get_logger().info(
                    f"'{robot_id}' can proceed to the next segment without collision "
                    "and deadlock."
                )
                if current_id is not None:
                    self._resource_mgn.release_segment_resources(robot_id, current_id)

                self._resource_mgn.hold_segment_resources(robot_id, next_id)
                response.result = True
        except ValueError:
            self.get_logger().error(traceback.format_exc())
            response.result = False

        return response

    def register_divided_paths_callback(
        self,
        request: RegisterDividedPaths.Request,
        response: RegisterDividedPaths.Response,
    ) -> RegisterDividedPaths.Response:
        """Register the divided paths."""
        self._resource_mgn = ResourcesManager(request.paths)
        response.result = 0
        return response

    def release_segment_resources_callback(
        self,
        request: ReleaseSegmentResources.Request,
        response: ReleaseSegmentResources.Response,
    ) -> ReleaseSegmentResources.Response:
        """Release the resources for the segment."""
        robot_id = request.robot_namespace
        segment_id = UUID(bytes=bytes(request.segment_id.uuid))

        self._resource_mgn.release_segment_resources(robot_id, segment_id)

        response.result = 0
        return response


def main(args: list[str] | None = None) -> None:
    """Run movement controller node."""
    rclpy.init(args=args)
    node = MovementController()
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        pass
    except ExternalShutdownException:
        sys.exit(1)
    finally:
        rclpy.try_shutdown()
        node.destroy_node()
