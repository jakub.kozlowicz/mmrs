import copy
import dataclasses
import types
from collections.abc import Iterable
from typing import Protocol
from uuid import UUID

RobotID = str
SegmentID = UUID
ResourceID = UUID


class ID(Protocol):
    """ID for the robots."""

    uuid: list[int]


class Segment(Protocol):
    """Segment for the robots."""

    uuid: ID
    required_resources: Iterable[ID]


class DividedPath(Protocol):
    """Divided path for the robots."""

    robot_namespace: RobotID
    segments: Iterable[Segment]


@dataclasses.dataclass
class BankersAlgorithm:
    available: dict[ResourceID, int]
    allocated: dict[RobotID, dict[ResourceID, int]]
    max: dict[RobotID, dict[ResourceID, int]]

    _robot_id: RobotID | None = dataclasses.field(
        default=None,
        init=False,
    )
    _current_resources: dict[ResourceID, int] | None = dataclasses.field(
        default=None,
        init=False,
    )
    _next_resources: dict[ResourceID, int] | None = dataclasses.field(
        default=None,
        init=False,
    )

    def __call__(
        self,
        robot_id: RobotID,
        current_resources: dict[ResourceID, int],
        next_resources: dict[ResourceID, int],
    ) -> "BankersAlgorithm":
        """Request resources for the robot to a new segment."""
        self._robot_id = robot_id
        self._current_resources = current_resources
        self._next_resources = next_resources

        return self

    def __enter__(self) -> "BankersAlgorithm":
        """Enter the context manager."""
        if (
            self._robot_id is None
            or self._current_resources is None
            or self._next_resources is None
        ):
            msg = "Request resources first before entering the context manager."
            raise ValueError(msg)

        self.release_resources(self._robot_id, self._current_resources)

        try:
            self.request_resources(self._robot_id, self._next_resources)
        except ValueError:
            self.request_resources(self._robot_id, self._current_resources)
            raise

        return self

    def __exit__(
        self,
        exc_type: type[BaseException] | None,
        exc_val: BaseException | None,
        exc_tb: types.TracebackType | None,
    ) -> None:
        """Exit the context manager."""
        if (
            self._robot_id is None
            or self._current_resources is None
            or self._next_resources is None
        ):
            msg = (
                "Request resources first before exiting the context manager."
                f"{self._robot_id=} {self._current_resources=} {self._next_resources=}"
            )
            raise ValueError(msg)

        self.release_resources(self._robot_id, self._next_resources)
        self.request_resources(self._robot_id, self._current_resources)

        self._robot_id = None
        self._current_resources = None
        self._next_resources = None

    def request_resources(
        self,
        robot_id: RobotID,
        resources: dict[ResourceID, int],
    ) -> None:
        """Attempt to allocate resources safely for the robot to a new segment."""
        if robot_id not in self.allocated:
            msg = f"Robot '{robot_id}' is not in the system. Assign resources first."
            raise ValueError(msg)

        for resource_id, required in resources.items():
            if self.available.get(resource_id, 0) < required:
                msg = f"Resource '{resource_id}' is insufficient for allocation."
                raise ValueError(msg)

        for resource_id, required in resources.items():
            allocated = self.allocated[robot_id].get(resource_id, 0)
            self.allocated[robot_id][resource_id] = allocated + required
            self.available[resource_id] -= required

    def release_resources(
        self,
        robot_id: RobotID,
        resources: dict[ResourceID, int],
    ) -> None:
        """Release the resources for the robot."""
        if robot_id not in self.allocated:
            msg = f"Robot '{robot_id}' is not in the system. Assign resources first."
            raise ValueError(msg)

        for resource_id, count in resources.items():
            if self.allocated[robot_id].get(resource_id, 0) < count:
                msg = (
                    "Attempting to release more resources than allocated for "
                    f"'{resource_id}'"
                )
                raise ValueError(msg)

        for resource_id, count in resources.items():
            self.allocated[robot_id][resource_id] -= count
            self.available[resource_id] += count

    def check_safe_state(self) -> bool:
        """Check if the system is in a safe state after an allocation."""
        work = copy.deepcopy(self.available)
        finish = {robot: False for robot in self.max}
        need = {
            robot: {
                resource: self.max[robot][resource]
                - self.allocated[robot].get(resource, 0)
                for resource in self.max[robot]
            }
            for robot in self.max
        }

        while True:
            found_process = False
            for robot, resources in self.allocated.items():
                if not finish[robot] and all(
                    need[robot][res] <= work[res] for res in need[robot]
                ):
                    for resource in resources:
                        work[resource] += resources[resource]
                    finish[robot] = True
                    found_process = True
                    break
            if not found_process:
                break

        return all(finish.values())


class ResourcesManager:
    """Resources manager for the robots."""

    def __init__(self, paths: Iterable[DividedPath] | None = None) -> None:
        """Initialize the resources manager."""
        self.segments: dict[RobotID, list[SegmentID]] = {}
        self.segments_resources: dict[SegmentID, dict[ResourceID, int]] = {}

        self.bankers = None

        self._resource_occupied: dict[ResourceID, RobotID | None] = {}

        if paths is not None:
            self.assign_resources(paths)

    def assign_resources(self, paths: Iterable[DividedPath]) -> None:
        """Assign resources to the segments."""
        allocated = {}
        max_res = {}
        available = {}

        for path in paths:
            robot_id = path.robot_namespace

            self.segments[robot_id] = []

            allocated[robot_id] = {}
            max_res[robot_id] = {}

            for segment in path.segments:
                segment_id = UUID(bytes=bytes(segment.uuid.uuid))

                self.segments[robot_id].append(segment_id)
                self.segments_resources[segment_id] = {}

                for resource in segment.required_resources:
                    resource_id = UUID(bytes=bytes(resource.uuid))

                    self.segments_resources[segment_id][resource_id] = 1
                    available[resource_id] = 1
                    max_res[robot_id][resource_id] = 1
                    allocated[robot_id][resource_id] = 0
                    self._resource_occupied[resource_id] = None

        self.bankers = BankersAlgorithm(
            available=available,
            allocated=allocated,
            max=max_res,
        )

    def hold_segment_resources(self, robot_id: RobotID, segment_id: SegmentID) -> None:
        """Hold the resources for the segment."""
        if self.bankers is None:
            msg = "Assign resources first before holding the resources."
            raise ValueError(msg)

        if robot_id not in self.segments:
            msg = f"Robot '{robot_id}' is not in the system. Assign resources first."
            raise ValueError(msg)

        if segment_id not in self.segments_resources:
            msg = (
                f"Segment '{segment_id}' is not in the system. Assign resources first."
            )
            raise ValueError(msg)

        resources = self.segments_resources[segment_id]

        self.bankers.request_resources(robot_id, resources)

        for resource in resources:
            if (
                self._resource_occupied[resource] is not None
                and self._resource_occupied[resource] != robot_id
            ):
                msg = (
                    f"Resource '{resource}' is already occupied by"
                    f" '{self._resource_occupied[resource]}'."
                )
                raise ValueError(msg)

            self._resource_occupied[resource] = robot_id

    def release_segment_resources(
        self,
        robot_id: RobotID,
        segment_id: SegmentID,
    ) -> None:
        """Release the resources for the segment."""
        if self.bankers is None:
            msg = "Assign resources first before releasing the resources."
            raise ValueError(msg)

        if robot_id not in self.segments:
            msg = f"Robot '{robot_id}' is not in the system. Assign resources first."
            raise ValueError(msg)

        if segment_id not in self.segments_resources:
            msg = (
                f"Segment '{segment_id}' is not in the system. Assign resources first."
            )
            raise ValueError(msg)

        resources = self.segments_resources[segment_id]

        self.bankers.release_resources(robot_id, resources)

        for resource in resources:
            if self._resource_occupied[resource] is None:
                msg = f"Resource '{resource}' is not occupied."
                raise ValueError(msg)

            self._resource_occupied[resource] = None

    def check_deadlock(
        self,
        robot_id: RobotID,
        current_id: SegmentID | None,
        next_id: SegmentID,
    ) -> bool:
        if self.bankers is None:
            msg = "Assign resources first before checking the safe state."
            raise ValueError(msg)

        if robot_id not in self.segments:
            msg = f"Robot '{robot_id}' is not in the system. Assign resources first."
            raise ValueError(msg)

        if current_id not in self.segments_resources and current_id is not None:
            msg = (
                f"Segment '{current_id}' is not in the system. Assign resources first."
            )
            raise ValueError(msg)

        if next_id not in self.segments_resources:
            msg = f"Segment '{next_id}' is not in the system. Assign resources first."
            raise ValueError(msg)

        next_resources = self.segments_resources[next_id]

        if current_id is not None:
            current_resources = self.segments_resources[current_id]
        else:
            current_resources = {}

        with self.bankers(robot_id, current_resources, next_resources):
            return not self.bankers.check_safe_state()

    def check_collision(
        self,
        robot_id: RobotID,
        current_id: SegmentID | None,
        next_id: SegmentID,
    ) -> bool:
        if self.bankers is None:
            msg = "Assign resources first before checking the safe state."
            raise ValueError(msg)

        if robot_id not in self.segments:
            msg = f"Robot '{robot_id}' is not in the system. Assign resources first."
            raise ValueError(msg)

        if current_id not in self.segments_resources and current_id is not None:
            msg = (
                f"Segment '{current_id}' is not in the system. Assign resources first."
            )
            raise ValueError(msg)

        if next_id not in self.segments_resources:
            msg = f"Segment '{next_id}' is not in the system. Assign resources first."
            raise ValueError(msg)

        next_resources = self.segments_resources[next_id]
        return any(
            self._resource_occupied[resource_id] is not None
            for resource_id in next_resources
            if self._resource_occupied[resource_id] != robot_id
        )
