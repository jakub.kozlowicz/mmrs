r"""Path segment for the robot path.

The path segment is a part of the robot path. It contains the path, the robot
namespace, the clearance required by the path, and the required resources.
Original path is divided into segments and each segment is checked if the robot
can enter it. The segments are checked for collisions and deadlocks.
"""

import dataclasses
import uuid

import unique_identifier_msgs.msg
from mmrs_msgs.msg import PathSegment as PathSegmentMsg
from nav_msgs.msg import Path


@dataclasses.dataclass
class PathSegment:
    """Segment of the robot path.

    Represents a segment of the robot path. The segment contains the path, the
    robot namespace, the clearance required by the path, and the required
    resources.

    :param robot_id: The namespace of the robot.
    :param path: The segment of the robot path.
    :param clearance: The required clearance for the path.
    :param required_resources: The required resources for the segment (defaults
       to None).
    """

    robot_id: str
    path: Path

    clearance: float
    required_resources: set[uuid.UUID] | None = None

    id: uuid.UUID = dataclasses.field(default_factory=uuid.uuid4, init=False)

    def to_msg(self) -> PathSegmentMsg:
        """Convert the path segment to a ROS message.

        :return: The ROS message of the path segment.
        """
        return PathSegmentMsg(
            path=self.path,
            uuid=unique_identifier_msgs.msg.UUID(uuid=list(self.id.bytes)),
            required_resources=[
                unique_identifier_msgs.msg.UUID(uuid=list(r.bytes))
                for r in self.required_resources or []
            ],
        )
