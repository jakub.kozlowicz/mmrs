r"""Equal length path divider for the controller of the multi-mobile robot
system.

Divide a path into equal length segments. The length is computed as the
euclidean distance between two consecutive points in the path. The path is
segmented into equal length segments. Each segment is represented by a list of
poses. Each segment has a unique identifier.
"""

from collections.abc import Mapping, Sequence

import numpy as np
from nav_msgs.msg import Path

from mmrs_controller.path.dividers.path_divider import PathDivider
from mmrs_controller.path.path_segment import PathSegment


class EqualLengthPathDivider(PathDivider):
    """Equal length path divider."""

    def __init__(self, segment_length: float) -> None:
        """Initialize class."""
        super().__init__()

        self._segment_length = segment_length

    def divide_single(
        self,
        path: Path,
        robot_namespace: str,
        other_paths: Mapping[str, Path],  # noqa: ARG002
    ) -> Sequence[PathSegment]:
        """Divides a single path into segments.

        :param path: The path to divide.
        :param robot_namespace: The namespace of the robot.
        :param other_paths: The paths of other robots. Not used in this
           implementation.
        :return: The path divided into segments.
        """
        poses = list(path.poses)

        segments: list[PathSegment] = []

        current_segment_length = 0.0
        current_segment = Path(header=path.header, poses=[])

        for i in range(len(poses) - 1):
            start_point = np.array(
                [poses[i].pose.position.x, poses[i].pose.position.y],
            )
            end_point = np.array(
                [poses[i + 1].pose.position.x, poses[i + 1].pose.position.y]
            )
            distance = np.linalg.norm(start_point - end_point)
            current_segment_length += distance
            current_segment.poses.append(poses[i])  # type: ignore[type]

            if current_segment_length >= self._segment_length:
                segments.append(
                    PathSegment(
                        robot_id=robot_namespace,
                        path=current_segment,
                        clearance=self._clearances[robot_namespace],
                    ),
                )

                current_segment = Path(header=path.header, poses=[poses[i]])
                current_segment_length = 0.0

        if current_segment.poses:
            current_segment.poses.append(poses[-1])  # type: ignore[type]
            segments.append(
                PathSegment(
                    robot_id=robot_namespace,
                    path=current_segment,
                    clearance=self._clearances[robot_namespace],
                ),
            )

        return segments
