r"""Optimal length path divider for the controller of the multi-mobile robot
system.

Divide a path into optimal length segments. Iterate over the path and check the
clearance of the path to other paths. If the clearance is too small, create a
segment and start a new segment. The clearance is calculated using the
Euclidean distance. If the clearance come back to normal, end the segment and
start a new one. The segments are used by the controller to plan the movement
of the robot.
"""

import itertools
from collections.abc import Mapping, Sequence

from nav_msgs.msg import Path
from shapely.geometry import LineString

from mmrs_controller.path.dividers.path_divider import PathDivider
from mmrs_controller.path.path_segment import PathSegment

Point = tuple[float, float]
PointPair = tuple[Point, Point]


class OptimalLengthPathDivider(PathDivider):
    """Optimal length path divider."""

    def __init__(self) -> None:
        """Initialize class."""
        super().__init__()

    def divide_single(
        self,
        path: Path,
        robot_namespace: str,
        other_paths: Mapping[str, Path],
    ) -> Sequence[PathSegment]:
        """Divide a single path into segments.

        :param path: The path to divide.
        :param robot_namespace: The robot namespace.
        :param other_paths: Other robots paths.
        :return: The path divided into segments.
        """
        if not path.poses:
            return []

        segments = []
        current_segment = Path(header=path.header, poses=[path.poses[0]])  # type: ignore[type]
        segment_start_too_close = False

        for points in itertools.pairwise(path.poses):
            current_point, next_point = points
            segment_line = LineString(
                [
                    (current_point.pose.position.x, current_point.pose.position.y),
                    (next_point.pose.position.x, next_point.pose.position.y),
                ]
            )

            segment_too_close = any(
                self._lines_too_close(
                    (
                        self._clearances[robot_namespace],
                        segment_line,
                    ),
                    (
                        self._clearances[other_namespace],
                        LineString(
                            (p.pose.position.x, p.pose.position.y)
                            for p in other_path.poses
                        ),
                    ),
                )
                for other_namespace, other_path in other_paths.items()
                if other_namespace != robot_namespace
            )

            if segment_too_close != segment_start_too_close:
                segments.append(
                    PathSegment(
                        robot_id=robot_namespace,
                        path=current_segment,
                        clearance=self._clearances[robot_namespace],
                    )
                )
                current_segment = Path(header=path.header, poses=[current_point])

            current_segment.poses.append(next_point)  # type: ignore[type]
            segment_start_too_close = segment_too_close

        if current_segment.poses:
            segments.append(
                PathSegment(
                    robot_id=robot_namespace,
                    path=current_segment,
                    clearance=self._clearances[robot_namespace],
                )
            )

        return segments
