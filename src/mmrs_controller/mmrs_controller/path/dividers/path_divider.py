r"""Path divider for the controller of the multi-mobile robot system.

Divide a path into segments. The path is represented by a list of poses. Each
pose is a point in 2D space with orientation. The path is divided into segments
and each segment is represented by a list of poses. Each segment has a unique
identifier. The segments are used by the controller to plan the movement of the
robot.
"""

import abc
import itertools
import uuid
from collections.abc import Mapping, Sequence

from nav_msgs.msg import Path
from shapely.geometry import LineString

from mmrs_controller.path.path_segment import PathSegment


class PathDivider(abc.ABC):
    """Base class for path dividers."""

    def __init__(self) -> None:
        """Initialize class."""

        self._clearances: dict[str, float] = {}

    @abc.abstractmethod
    def divide_single(
        self,
        path: Path,
        robot_namespace: str,
        other_paths: Mapping[str, Path],
    ) -> Sequence[PathSegment]:
        """Divide a single path into segments.

        :param path: The path to divide.
        :param robot_namespace: The robot namespace.
        :param other_paths: Other robots paths.
        :return: The path divided into segments.
        """
        raise NotImplementedError

    def add_robot_clearence(self, robot_namespace: str, clearance: float) -> None:
        """Add required clearance for the robot.

        :param robot_namespace: The robot namespace.
        :param clearance: The required clearance.
        """
        self._clearances[robot_namespace] = clearance

    def remove_robot_clearance(self, robot_namespace: str) -> None:
        """Remove required clearance for the robot.

        :param robot_namespace: The robot namespace.
        """
        self._clearances.pop(robot_namespace, None)

    def divide(
        self,
        robots_paths: Mapping[str, Path],
    ) -> Mapping[str, Sequence[PathSegment]]:
        """Divide all defined paths into segments.

        :param robots_paths: The paths to divide.
        :return: The paths divided into segments for each robot.
        """
        segments: Mapping[str, Sequence[PathSegment]] = {}

        for robot_namespace, path in robots_paths.items():
            divided = self.divide_single(
                path,
                robot_namespace,
                robots_paths,
            )
            segments[robot_namespace] = divided

        return self._assign_resources(segments)

    def _assign_resources(
        self,
        segments: Mapping[str, Sequence[PathSegment]],
    ) -> Mapping[str, Sequence[PathSegment]]:
        """Assign required resources to each segment.

        :param segments: The segments to assign resources to.
        :return: The segments with assigned resources.
        """
        for (namespace1, segments1), (namespace2, segments2) in itertools.combinations(
            segments.items(), 2
        ):
            for segment1 in segments1:
                for segment2 in segments2:
                    if namespace1 == namespace2:
                        continue

                    lines_too_close = self._lines_too_close(
                        (
                            self._clearances[namespace1],
                            LineString(
                                (p.pose.position.x, p.pose.position.y)
                                for p in segment1.path.poses
                            ),
                        ),
                        (
                            self._clearances[namespace2],
                            LineString(
                                (p.pose.position.x, p.pose.position.y)
                                for p in segment2.path.poses
                            ),
                        ),
                    )

                    if lines_too_close:
                        resource_id = uuid.uuid4()

                        if not segment1.required_resources:
                            segment1.required_resources = set()
                        segment1.required_resources.add(resource_id)

                        if not segment2.required_resources:
                            segment2.required_resources = set()
                        segment2.required_resources.add(resource_id)

        return segments

    @staticmethod
    def _lines_too_close(
        first: tuple[float, LineString],
        second: tuple[float, LineString],
    ) -> bool:
        """Check if two lines are too close to each other.

        :param first: The first line with its required clearance.
        :param second: The second line with its required clearance.
        :return: True if the lines are too close to each other, False otherwise.
        """
        first_clearance, first_line = first
        second_clearance, second_line = second
        if first_line.distance(second_line) < first_clearance + second_clearance:
            return True

        return False
