r"""Grid cell based path divider for the controller of the multi-mobile robot
system.

Divide a path based on the grid of the 2D motion space. The grid is represented
by the cells. Additionaly cells are subdivided into squares that can tell
whether the robot can fit into one cell or requires more. For this the corners
of cells have circles dfefined that represenst that all four cells are required
for the robot to fit. The path is divided into segments and each segment is
represented by a list of poses. Each segment has a unique identifier.
"""

import dataclasses
import uuid
from collections import defaultdict
from collections.abc import Mapping, Sequence

import numpy as np
from nav_msgs.msg import Path
from shapely.geometry import Point as ShapelyPoint
from shapely.geometry import Polygon as ShapelyPolygon
from shapely.geometry import box
from shapely.geometry.base import BaseGeometry

from mmrs_controller.path.dividers.path_divider import PathDivider
from mmrs_controller.path.path_segment import PathSegment


@dataclasses.dataclass
class PolygonsData:
    """Polygons generatation data.

    :param polygons: The mapping of polygons id to their geometry.
    :param polygons_cells: The mapping of polygons id to cells that are
       required for robot to fit.
    :param cells_polygons: The mapping of cells to polygons that are contained
       in it.
    """

    polygons: dict[uuid.UUID, ShapelyPolygon]
    polygons_cells: dict[uuid.UUID, list[tuple[int, int]]]
    cells_polygons: dict[tuple[int, int], list[uuid.UUID]]


class GridCellsPathDivider(PathDivider):
    """Grid cells path divider."""

    def __init__(
        self,
        size_m: float,
        grid_size_m: float,
        circle_radius_m: float,
    ) -> None:
        """Initialize class."""
        super().__init__()

        self._ticks = self._generate_ticks(
            -size_m / 2,
            size_m / 2,
            grid_size_m,
        )

        polygons_data = self._generate_polygons(
            self._ticks,
            circle_radius_m,
        )

        self._resource_assigner = defaultdict(uuid.uuid4)

        self.polygons = polygons_data.polygons
        self.polygons_cells = polygons_data.polygons_cells
        self.cells_polygons = polygons_data.cells_polygons

    def divide(
        self,
        robots_paths: Mapping[str, Path],
    ) -> Mapping[str, Sequence[PathSegment]]:
        """Divide all defined paths into segments.

        :param robots_paths: The paths to divide.
        :return: The paths divided into segments for each robot.
        """
        # NOTE: Important to reset the resource assigner, as this is the new
        # round of dividing paths.
        self._resource_assigner = defaultdict(uuid.uuid4)
        return {
            robot_namespace: self.divide_single(
                path,
                robot_namespace,
                robots_paths,
            )
            for robot_namespace, path in robots_paths.items()
        }

    def divide_single(
        self,
        path: Path,
        robot_namespace: str,
        other_paths: Mapping[str, Path],  # noqa: ARG002
    ) -> Sequence[PathSegment]:
        """Divides a single path into segments.

        :param path: The path to divide.
        :param robot_namespace: The namespace of the robot.
        :param other_paths: The paths of other robots. Not used in this
           implementation.
        :return: The path divided into segments.
        """
        segments: list[PathSegment] = []

        current_polygon_id = None
        prevoius_polygon_id = None

        for point in path.poses:
            point_geom = ShapelyPoint([point.pose.position.x, point.pose.position.y])

            cell = self._get_cell_index(point.pose.position.x, point.pose.position.y)

            if cell not in self.cells_polygons:
                msg = f"Point {point_geom} does not belong to any cell."
                raise ValueError(msg)

            polygons_ids = self.cells_polygons[cell]

            for polygon_id in polygons_ids:
                polygon = self.polygons[polygon_id]
                if polygon.intersects(point_geom):
                    current_polygon_id = polygon_id
                    break
            else:
                msg = f"Point {point_geom} does not belong to any polygon."
                raise ValueError(msg)

            if current_polygon_id != prevoius_polygon_id:
                if prevoius_polygon_id is not None:
                    segments[-1].path.poses.append(point)  # type: ignore[type]

                segments.append(
                    PathSegment(
                        robot_id=robot_namespace,
                        path=Path(header=path.header, poses=[point]),
                        clearance=self._clearances[robot_namespace],
                        required_resources={
                            self._resource_assigner[cell]
                            for cell in self.polygons_cells[current_polygon_id]
                        },
                    ),
                )

                prevoius_polygon_id = current_polygon_id

            else:
                segments[-1].path.poses.append(point)  # type: ignore[type]

        return segments

    def _generate_polygons(
        self,
        ticks: np.ndarray,
        clearance: float,
    ) -> PolygonsData:
        """Generate polygons for the grid and circles.

        :param ticks: The ticks for the grid.
        :param clearance: The clearances for the cells and circles that are
           required to fit the robot in one cell.
        :return: The generated polygons that cover whole 2D motion space.
        """
        polygons_data = PolygonsData({}, {}, {})

        boxes_data = self._generate_boxes_polygons(
            ticks,
            clearance,
        )

        polygons_data.polygons.update(boxes_data.polygons)
        polygons_data.polygons_cells.update(boxes_data.polygons_cells)
        polygons_data.cells_polygons.update(boxes_data.cells_polygons)

        circles_data = self._generate_circle_polygons(
            ticks,
            clearance,
        )

        polygons_data.polygons.update(circles_data.polygons)
        polygons_data.polygons_cells.update(circles_data.polygons_cells)

        for cell, polygon_ids in circles_data.cells_polygons.items():
            if cell in polygons_data.cells_polygons:
                polygons_data.cells_polygons[cell].extend(polygon_ids)
            else:
                polygons_data.cells_polygons[cell] = polygon_ids

        return polygons_data

    def _get_cell_index(self, x: float, y: float) -> tuple[int, int]:
        """Get the cell index for a given point.

        :param x: The x coordinate of the point in the global frame.
        :param y: The y coordinate of the point in the global frame.
        :raises ValueError: If the point is outside of the grid.
        :return: The cell index for the given point.
        """
        _min, _max = self._ticks[0], self._ticks[-1]
        if x < _min or x > _max or y < _min or y > _max:
            msg = f"Point ({x}, {y}) is outside of the grid."
            raise ValueError(msg)

        return (
            np.searchsorted(self._ticks, x, side="right").astype(int) - 1,
            np.searchsorted(self._ticks, y, side="right").astype(int) - 1,
        )

    def _generate_circle_polygons(
        self,
        ticks: np.ndarray,
        clearance: float,
    ) -> PolygonsData:
        """Generate circles for the grid.

        :param ticks: The ticks for the grid.
        :param clearance: The clearance for the circles.
        :return: The generated polygons for the circles.
        """

        def _get_circle_cells(x: int, y: int) -> list[tuple[int, int]]:
            """Gets cells that are part of the circle."""
            return [(x, y), (x - 1, y), (x, y - 1), (x - 1, y - 1)]

        polygons = {}
        polygons_cells = {}
        cells_polygons = {}

        for x in ticks:
            for y in ticks:
                cell_x, cell_y = self._get_cell_index(x, y)

                circle = ShapelyPoint(x, y).buffer(clearance)
                circle_id = uuid.uuid4()
                circle_cells = _get_circle_cells(cell_x, cell_y)

                polygons[circle_id] = circle
                polygons_cells[circle_id] = circle_cells

                for cell in circle_cells:
                    if cell in cells_polygons:
                        cells_polygons[cell].append(circle_id)
                    else:
                        cells_polygons[cell] = [circle_id]

        return PolygonsData(polygons, polygons_cells, cells_polygons)

    def _generate_boxes_polygons(
        self,
        ticks: np.ndarray,
        clearance: float,
    ) -> PolygonsData:
        """Generate the boxes for the grid.

        :param ticks: Thje ticks for the grid.
        :param clearance: The clearance for the boxes.
        :return: The generated polygons for the boxes.
        """
        polygons = {}
        polygons_cells = {}
        cells_polygons = {}

        for i in range(len(ticks) - 1):
            for j in range(len(ticks) - 1):
                subgrid_boxes = self._generate_subgrid_boxes(
                    (ticks[i], ticks[j], ticks[i + 1], ticks[j + 1]),
                    clearance=clearance,
                )

                polygons.update(subgrid_boxes.polygons)
                polygons_cells.update(subgrid_boxes.polygons_cells)
                cells_polygons.update(subgrid_boxes.cells_polygons)

        return PolygonsData(polygons, polygons_cells, cells_polygons)

    def _generate_subgrid_boxes(
        self,
        subgrid: tuple[float, float, float, float],
        *,
        clearance: float,
    ) -> PolygonsData:
        """Genertate 9 non-overlapping boxes within a main grid cell.

        The boxes that are in the corners of the cell are substracted by the
        circles that are required for the robot to fit into one cell.

        :param subgrid: The coordinates of the one cell of the grid.
        :param clearance: The clearance for the boxes.
        :return: The generated polygons for the boxes of one cell.
        """
        polygons = {}
        polygons_cells = {}
        cells_polygons = {}

        def _create_box_with_circle_diff(
            x1: float,
            y1: float,
            x2: float,
            y2: float,
            circle: BaseGeometry | None = None,
        ) -> BaseGeometry:
            new_box = box(x1, y1, x2, y2)
            if circle:
                return new_box.difference(circle)
            return new_box

        x_min, y_min, x_max, y_max = subgrid

        x_div_1 = x_min + clearance
        x_div_2 = x_max - clearance
        y_div_1 = y_min + clearance
        y_div_2 = y_max - clearance

        boxes = [
            {
                "coords": (x_min, y_min, x_div_1, y_div_1),
                "circle": "bottom_left",
                "cells": [(0, 0), (-1, 0), (0, -1)],
            },
            {
                "coords": (x_div_1, y_min, x_div_2, y_div_1),
                "circle": None,
                "cells": [(0, 0), (0, -1)],
            },
            {
                "coords": (x_div_2, y_min, x_max, y_div_1),
                "circle": "bottom_right",
                "cells": [(0, 0), (1, 0), (0, -1)],
            },
            {
                "coords": (x_min, y_div_1, x_div_1, y_div_2),
                "circle": None,
                "cells": [(0, 0), (-1, 0)],
            },
            {
                "coords": (x_div_1, y_div_1, x_div_2, y_div_2),
                "circle": None,
                "cells": [(0, 0)],
            },
            {
                "coords": (x_div_2, y_div_1, x_max, y_div_2),
                "circle": None,
                "cells": [(0, 0), (1, 0)],
            },
            {
                "coords": (x_min, y_div_2, x_div_1, y_max),
                "circle": "top_left",
                "cells": [(0, 0), (-1, 0), (0, 1)],
            },
            {
                "coords": (x_div_1, y_div_2, x_div_2, y_max),
                "circle": None,
                "cells": [(0, 0), (0, 1)],
            },
            {
                "coords": (x_div_2, y_div_2, x_max, y_max),
                "circle": "top_right",
                "cells": [(0, 0), (1, 0), (0, 1)],
            },
        ]

        circles = {
            "bottom_left": ShapelyPoint(x_min, y_min).buffer(clearance),
            "bottom_right": ShapelyPoint(x_max, y_min).buffer(clearance),
            "top_left": ShapelyPoint(x_min, y_max).buffer(clearance),
            "top_right": ShapelyPoint(x_max, y_max).buffer(clearance),
        }

        boxes_polygons = []

        for box_data in boxes:
            box_polygon = _create_box_with_circle_diff(
                *box_data["coords"],
                circle=circles.get(box_data["circle"]),
            )

            centroid: ShapelyPoint = box_polygon.centroid  # type: ignore[attr-defined]
            cell = self._get_cell_index(centroid.x, centroid.y)

            box_id = uuid.uuid4()
            polygons[box_id] = box_polygon

            polygons_cells[box_id] = [
                (cell[0] + x_offset, cell[1] + y_offset)
                for x_offset, y_offset in box_data["cells"]
            ]

            boxes_polygons.append(box_polygon)

            if cell in cells_polygons:
                cells_polygons[cell].append(box_id)
            else:
                cells_polygons[cell] = [box_id]

        return PolygonsData(polygons, polygons_cells, cells_polygons)

    @staticmethod
    def _generate_ticks(
        start: float,
        end: float,
        grid_spacing: float,
    ) -> np.ndarray:
        """Generate ticks for the grid.

        :param start: Start of the grid.
        :param end: End of the grid.
        :param grid_spacing: Spacing of the grid.
        :return: Ticks for the grid.
        """
        num_neg = abs(start) // grid_spacing
        num_pos = (end // grid_spacing) + 1

        return np.arange(
            -num_neg * grid_spacing,
            num_pos * grid_spacing,
            grid_spacing,
        )
