"""Launch controller for the multi-mobile robot system."""

from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import LaunchConfiguration, PathJoinSubstitution
from launch_ros.actions import Node
from launch_ros.substitutions import FindPackageShare


def generate_launch_description() -> LaunchDescription:
    """Generate launch description."""

    mmrs_controller_dir = FindPackageShare("mmrs_controller")

    controller_params = LaunchConfiguration("controller_params")

    return LaunchDescription(
        [
            DeclareLaunchArgument(
                "controller_params",
                default_value=PathJoinSubstitution(
                    [mmrs_controller_dir, "params", "controller_params.yaml"],
                ),
                description=(
                    "Full path to the ROS2 parameters file to use for visualization"
                ),
            ),
            Node(
                package="mmrs_controller",
                executable="controller",
                name="controller",
                parameters=[controller_params],
            ),
            Node(
                package="mmrs_controller",
                executable="path_divider",
                name="path_divider",
                parameters=[controller_params],
            ),
            Node(
                package="mmrs_controller",
                executable="movement_controller",
                name="movement_controller",
                parameters=[controller_params],
            ),
        ],
    )
