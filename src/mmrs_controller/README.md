# mmrs_controller

The `mmrs_controller` package is responsible for controlling the multiple
mobile robots' movement, path segmentation, collision avoidance and mission
accomplishment.

