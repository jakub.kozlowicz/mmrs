import random
import uuid
from contextlib import nullcontext as does_not_raise

import pytest
from mmrs_controller.movement_controller import ResourcesManager


@pytest.mark.parametrize(
    "path",
    [
        "collision_equal_length_fixture",
        "collision_grid_based_fixture",
        "collision_optimal_length_fixture",
        "no_collision_equal_length_fixture",
        "no_collision_grid_based_fixture",
        "no_collision_optimal_length_fixture",
    ],
)
def test_assign_resources_on_creation(path, request) -> None:
    # Arrange

    # Act

    # Assert
    with does_not_raise():
        _ = ResourcesManager(request.getfixturevalue(path))


@pytest.mark.parametrize(
    "path",
    [
        "collision_equal_length_fixture",
        "collision_grid_based_fixture",
        "collision_optimal_length_fixture",
        "no_collision_equal_length_fixture",
        "no_collision_grid_based_fixture",
        "no_collision_optimal_length_fixture",
    ],
)
def test_assign_resources_separatelly(path, request) -> None:
    # Arrange
    rm = ResourcesManager()

    # Act

    # Assert
    with does_not_raise():
        rm.assign_resources(request.getfixturevalue(path))


@pytest.mark.parametrize(
    "path",
    [
        "collision_equal_length_fixture",
        "collision_grid_based_fixture",
        "collision_optimal_length_fixture",
        "no_collision_equal_length_fixture",
        "no_collision_grid_based_fixture",
        "no_collision_optimal_length_fixture",
    ],
)
def test_raise_error_when_no_resources(path, request) -> None:
    # Arrange
    _ = request.getfixturevalue(path)
    rm = ResourcesManager()

    robot_id = str(uuid.uuid4())
    segment_id = uuid.uuid4()

    # Act

    # Assert
    with pytest.raises(expected_exception=ValueError):
        rm.hold_segment_resources(robot_id, segment_id)


@pytest.mark.parametrize(
    "path",
    [
        "collision_equal_length_fixture",
        "collision_grid_based_fixture",
        "collision_optimal_length_fixture",
        "no_collision_equal_length_fixture",
        "no_collision_grid_based_fixture",
        "no_collision_optimal_length_fixture",
    ],
)
def test_request_single_robot(path, request) -> None:
    # Arrange
    rm = ResourcesManager(request.getfixturevalue(path))
    robot_id = next(iter(rm.segments))
    segment_id = next(iter(rm.segments[robot_id]))

    ref_allocated = {
        resource_id: 1 for resource_id in rm.segments_resources[segment_id]
    }

    ref_available = {
        resource_id: 0 for resource_id in rm.segments_resources[segment_id]
    }

    # Act
    with does_not_raise():
        rm.hold_segment_resources(robot_id, segment_id)

    # Assert
    assert rm.bankers is not None, "Bankers algorithm is not initialized."
    assert set(ref_allocated.items()).issubset(
        set(rm.bankers.allocated[robot_id].items())
    ), "Allocated resources are not as expected."
    assert set(ref_available.items()).issubset(
        set(rm.bankers.available.items())
    ), "Available resources are not as expected."


@pytest.mark.parametrize(
    "path",
    [
        "collision_equal_length_fixture",
        "collision_grid_based_fixture",
        "collision_optimal_length_fixture",
        "no_collision_equal_length_fixture",
        "no_collision_grid_based_fixture",
        "no_collision_optimal_length_fixture",
    ],
)
def test_request_release_single_robot(path, request) -> None:
    # Arrange
    rm = ResourcesManager(request.getfixturevalue(path))
    robot_id = next(iter(rm.segments))
    segment_id = next(iter(rm.segments[robot_id]))

    ref_allocated = {
        resource_id: 0 for resource_id in rm.segments_resources[segment_id]
    }

    ref_available = {
        resource_id: 1 for resource_id in rm.segments_resources[segment_id]
    }

    # Act
    with does_not_raise():
        rm.hold_segment_resources(robot_id, segment_id)

    with does_not_raise():
        rm.release_segment_resources(robot_id, segment_id)

    # Assert
    assert rm.bankers is not None, "Bankers algorithm is not initialized."
    assert set(ref_allocated.items()).issubset(
        set(rm.bankers.allocated[robot_id].items())
    ), "Allocated resources are not as expected."
    assert set(ref_available.items()).issubset(
        set(rm.bankers.available.items())
    ), "Available resources are not as expected."


@pytest.mark.parametrize(
    "path",
    [
        "collision_equal_length_fixture",
        "collision_grid_based_fixture",
        "collision_optimal_length_fixture",
        "no_collision_equal_length_fixture",
        "no_collision_grid_based_fixture",
        "no_collision_optimal_length_fixture",
    ],
)
def test_request_release_multiple_robots(path, request) -> None:
    # Arrange
    rm = ResourcesManager(request.getfixturevalue(path))

    robot1, robot2 = rm.segments.keys()

    segment1 = next(iter(rm.segments[robot1]))
    segment2 = next(iter(rm.segments[robot2]))

    ref_allocated1 = {resource_id: 0 for resource_id in rm.segments_resources[segment1]}
    ref_available1 = {resource_id: 1 for resource_id in rm.segments_resources[segment1]}

    ref_allocated2 = {resource_id: 0 for resource_id in rm.segments_resources[segment2]}
    ref_available2 = {resource_id: 1 for resource_id in rm.segments_resources[segment2]}

    # Act
    with does_not_raise():
        rm.hold_segment_resources(robot1, segment1)
        rm.hold_segment_resources(robot2, segment2)

    with does_not_raise():
        rm.release_segment_resources(robot1, segment1)
        rm.release_segment_resources(robot2, segment2)

    # Assert
    assert rm.bankers is not None, "Bankers algorithm is not initialized."
    assert set(ref_allocated1.items()).issubset(
        set(rm.bankers.allocated[robot1].items())
    ), "Allocated resources are not as expected."
    assert set(ref_available1.items()).issubset(
        set(rm.bankers.available.items())
    ), "Available resources are not as expected."
    assert set(ref_allocated2.items()).issubset(
        set(rm.bankers.allocated[robot2].items())
    ), "Allocated resources are not as expected."
    assert set(ref_available2.items()).issubset(
        set(rm.bankers.available.items())
    ), "Available resources are not as expected."


@pytest.mark.parametrize(
    "path",
    [
        "collision_equal_length_fixture",
        "collision_grid_based_fixture",
        "collision_optimal_length_fixture",
        "no_collision_equal_length_fixture",
        "no_collision_grid_based_fixture",
        "no_collision_optimal_length_fixture",
    ],
)
def test_request_single_robot_multiple_times_same_resources(path, request) -> None:
    # Arrange
    rm = ResourcesManager(request.getfixturevalue(path))
    robot_id = next(iter(rm.segments))
    segment_id = next(iter(rm.segments[robot_id]))

    ref_allocated = {
        resource_id: 1 for resource_id in rm.segments_resources[segment_id]
    }

    ref_available = {
        resource_id: 0 for resource_id in rm.segments_resources[segment_id]
    }

    # Act
    for _ in range(random.randint(1, 10)):
        with does_not_raise():
            rm.hold_segment_resources(robot_id, segment_id)

    # Assert
    assert rm.bankers is not None, "Bankers algorithm is not initialized."
    assert set(ref_allocated.items()).issubset(
        set(rm.bankers.allocated[robot_id].items())
    ), "Allocated resources are not as expected."
    assert set(ref_available.items()).issubset(
        set(rm.bankers.available.items())
    ), "Available resources are not as expected."


@pytest.mark.parametrize(
    "path",
    [
        "collision_equal_length_fixture",
        "collision_grid_based_fixture",
        "collision_optimal_length_fixture",
        "no_collision_equal_length_fixture",
        "no_collision_grid_based_fixture",
        "no_collision_optimal_length_fixture",
    ],
)
def test_request_release_single_robot_multiple_times_same_resources(
    path, request
) -> None:
    # Arrange
    rm = ResourcesManager(request.getfixturevalue(path))
    robot_id = next(iter(rm.segments))
    segment_id = next(iter(rm.segments[robot_id]))

    ref_allocated = {
        resource_id: 0 for resource_id in rm.segments_resources[segment_id]
    }

    ref_available = {
        resource_id: 1 for resource_id in rm.segments_resources[segment_id]
    }

    # Act
    repetitions = random.randint(1, 10)
    for _ in range(repetitions):
        with does_not_raise():
            rm.hold_segment_resources(robot_id, segment_id)

    with does_not_raise():
        rm.release_segment_resources(robot_id, segment_id)

    # Assert
    assert rm.bankers is not None, "Bankers algorithm is not initialized."
    assert set(ref_allocated.items()).issubset(
        set(rm.bankers.allocated[robot_id].items())
    ), "Allocated resources are not as expected."
    assert set(ref_available.items()).issubset(
        set(rm.bankers.available.items())
    ), "Available resources are not as expected."


@pytest.mark.parametrize(
    "path",
    [
        "collision_equal_length_fixture",
        "collision_grid_based_fixture",
        "collision_optimal_length_fixture",
        "no_collision_equal_length_fixture",
        "no_collision_grid_based_fixture",
        "no_collision_optimal_length_fixture",
    ],
)
def test_check_deadlock(path, request) -> None:
    # Arrange
    rm = ResourcesManager(request.getfixturevalue(path))

    robot1, robot2 = rm.segments.keys()

    segments1 = iter(rm.segments[robot1])
    segments2 = iter(rm.segments[robot2])

    try:
        while True:
            segment11 = next(segments1)
            segment21 = next(segments2)

            segment12 = next(segments1)
            segment22 = next(segments2)

            if (
                rm.segments_resources[segment11]
                .keys()
                .isdisjoint(rm.segments_resources[segment21].keys())
                and rm.segments_resources[segment12]
                .keys()
                .isdisjoint(rm.segments_resources[segment22].keys())
                and rm.segments_resources[segment11]
                .keys()
                .isdisjoint(rm.segments_resources[segment22].keys())
                and rm.segments_resources[segment12]
                .keys()
                .isdisjoint(rm.segments_resources[segment21].keys())
            ):
                break
    except StopIteration:
        pytest.skip("Not enough segments for the test.")

    # Act
    with does_not_raise():
        rm.hold_segment_resources(robot1, segment11)
        rm.hold_segment_resources(robot2, segment21)

    # Assert
    assert rm.bankers is not None, "Bankers algorithm is not initialized."
    assert not rm.check_deadlock(
        robot1, segment11, segment12
    ), "Deadlock should not be detected."
    assert not rm.check_deadlock(
        robot2, segment21, segment22
    ), "Deadlock should not be detected."

    with does_not_raise():
        rm.release_segment_resources(robot1, segment11)
        rm.release_segment_resources(robot2, segment21)


@pytest.mark.parametrize(
    "path",
    [
        "no_collision_equal_length_fixture",
        "no_collision_grid_based_fixture",
        "no_collision_optimal_length_fixture",
    ],
)
def test_safe_state_no_collisions(path, request) -> None:
    # Arrange
    rm = ResourcesManager(request.getfixturevalue(path))
    robot1, robot2 = rm.segments.keys()
    segment1 = random.choice(rm.segments[robot1])
    segment2 = random.choice(rm.segments[robot2])

    # Act
    with does_not_raise():
        rm.hold_segment_resources(robot1, segment1)

    with does_not_raise():
        rm.hold_segment_resources(robot2, segment2)

    # Assert
    assert rm.bankers is not None, "Bankers algorithm is not initialized."
    assert rm.bankers.check_safe_state(), "Bankers algorithm did not return safe state."


@pytest.mark.parametrize(
    "path",
    [
        "collision_equal_length_fixture",
        "collision_grid_based_fixture",
        "collision_optimal_length_fixture",
    ],
)
def test_unsufficient_resources(path, request) -> None:
    # Arrange
    no_robots = 2
    rm = ResourcesManager(request.getfixturevalue(path))
    robots = list(rm.segments.keys())

    if len(robots) < no_robots:
        pytest.skip("Not enough robots for the test.")

    robot1, robot2 = robots[:no_robots]
    segment1, segment2 = None, None

    for seg1 in rm.segments[robot1]:
        for seg2 in rm.segments[robot2]:
            resources1 = rm.segments_resources[seg1]
            resources2 = rm.segments_resources[seg2]

            if any(
                (resources1.get(res_id, 0) + resources2.get(res_id, 0))
                > rm.bankers.available.get(res_id, 0)  # type: ignore[attr-defined]
                for res_id in set(resources1) | set(resources2)
            ):
                segment1, segment2 = seg1, seg2
                break
        if segment1 and segment2:
            break

    if not segment1 or not segment2:
        pytest.fail("Suitable segments not found for an unsafe test.")

    # Act
    with does_not_raise():
        rm.hold_segment_resources(robot1, segment1)  # type: ignore[attr-defined]

    with pytest.raises(expected_exception=ValueError):
        rm.hold_segment_resources(robot2, segment2)  # type: ignore[attr-defined]

    # Assert
    assert rm.bankers is not None, "Bankers algorithm is not initialized."


@pytest.mark.parametrize(
    "path",
    [
        "collision_equal_length_fixture",
        "collision_grid_based_fixture",
        "collision_optimal_length_fixture",
    ],
)
def test_safe_state_collisions(path, request) -> None:
    # Arrange
    no_robots = 2
    rm = ResourcesManager(request.getfixturevalue(path))
    robots = list(rm.segments.keys())

    if len(robots) < no_robots:
        pytest.skip("Not enough robots for the test.")

    robot1, robot2 = robots[:no_robots]
    segment1, segment2 = None, None

    for seg1 in rm.segments[robot1]:
        for seg2 in rm.segments[robot2]:
            resources1 = rm.segments_resources[seg1]
            resources2 = rm.segments_resources[seg2]

            if all(
                (resources1.get(res_id, 0) + resources2.get(res_id, 0))
                <= rm.bankers.available.get(res_id, 0)  # type: ignore[attr-defined]
                for res_id in set(resources1) | set(resources2)
            ):
                segment1, segment2 = seg1, seg2
                break
        if segment1 and segment2:
            break

    if not segment1 or not segment2:
        pytest.fail("Suitable segments not found for a safe test.")

    # Act
    with does_not_raise():
        rm.hold_segment_resources(robot1, segment1)  # type: ignore[attr-defined]
        rm.hold_segment_resources(robot2, segment2)  # type: ignore[attr-defined]

    # Assert
    assert rm.bankers is not None, "Bankers algorithm is not initialized."
    assert rm.bankers.check_safe_state(), "Bankers algorithm did not return safe state."


@pytest.mark.parametrize(
    "path",
    [
        "no_collision_equal_length_fixture",
        "no_collision_grid_based_fixture",
        "no_collision_optimal_length_fixture",
    ],
)
def test_check_no_collsion(path, request) -> None:
    # Arrange
    rm = ResourcesManager(request.getfixturevalue(path))

    robot1, robot2 = rm.segments.keys()

    segments1 = iter(rm.segments[robot1])
    segments2 = iter(rm.segments[robot2])

    try:
        while True:
            segment11 = next(segments1)
            segment21 = next(segments2)

            segment12 = next(segments1)
            segment22 = next(segments2)

            if (
                rm.segments_resources[segment11]
                .keys()
                .isdisjoint(rm.segments_resources[segment21].keys())
                and rm.segments_resources[segment12]
                .keys()
                .isdisjoint(rm.segments_resources[segment22].keys())
                and rm.segments_resources[segment11]
                .keys()
                .isdisjoint(rm.segments_resources[segment22].keys())
                and rm.segments_resources[segment12]
                .keys()
                .isdisjoint(rm.segments_resources[segment21].keys())
            ):
                break
    except StopIteration:
        pytest.skip("Not enough segments for the test.")

    # Act
    with does_not_raise():
        rm.hold_segment_resources(robot1, segment11)
        rm.hold_segment_resources(robot2, segment21)

    # Assert
    assert rm.bankers is not None, "Bankers algorithm is not initialized."
    assert not rm.check_collision(
        robot1, segment11, segment12
    ), "Collison should not be detected."
    assert not rm.check_collision(
        robot2, segment21, segment22
    ), "Collsion should not be detected."

    with does_not_raise():
        rm.release_segment_resources(robot1, segment11)
        rm.release_segment_resources(robot2, segment21)
