import typing

import pytest
from fixtures import (
    collision_equal_length,
    collision_grid_based,
    collision_optimal_length,
    no_collision_equal_length,
    no_collision_grid_based,
    no_collision_optimal_length,
)


@pytest.fixture()
def no_collision_equal_length_fixture() -> list[typing.Any]:
    return no_collision_equal_length.no_collision


@pytest.fixture()
def collision_equal_length_fixture():
    return collision_equal_length.collision


@pytest.fixture()
def no_collision_grid_based_fixture():
    return no_collision_grid_based.no_collision


@pytest.fixture()
def collision_grid_based_fixture():
    return collision_grid_based.collision


@pytest.fixture()
def no_collision_optimal_length_fixture():
    return no_collision_optimal_length.no_collision


@pytest.fixture()
def collision_optimal_length_fixture():
    return collision_optimal_length.collision
