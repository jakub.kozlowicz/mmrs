import builtin_interfaces.msg
import geometry_msgs.msg
import mmrs_msgs.msg
import nav_msgs.msg
import std_msgs.msg
import unique_identifier_msgs.msg
from numpy import array, uint8

collision = [
    mmrs_msgs.msg.DividedPath(
        robot_namespace="/robot1",
        segments=[
            mmrs_msgs.msg.PathSegment(
                uuid=unique_identifier_msgs.msg.UUID(
                    uuid=array(
                        [
                            245,
                            140,
                            164,
                            24,
                            95,
                            144,
                            71,
                            55,
                            172,
                            61,
                            192,
                            51,
                            166,
                            149,
                            92,
                            79,
                        ],
                        dtype=uint8,
                    )
                ),
                path=nav_msgs.msg.Path(
                    header=std_msgs.msg.Header(
                        stamp=builtin_interfaces.msg.Time(sec=65, nanosec=153000000),
                        frame_id="map",
                    ),
                    poses=[
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.35000015422701836, y=1.700000174343586, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.1399555891129844,
                                    w=0.9901577819095487,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.38533340670664984, y=1.710192282649757, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.10550977824124384,
                                    w=0.9944182654675464,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4097764612700985, y=1.7049463108918985, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.15984406931353062,
                                    w=0.9871422762222735,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4334993009790651, y=1.6970567538895693, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.1743182081710164,
                                    w=0.9846893735082379,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4569795259383227, y=1.6884744478534799, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.25289231923334193,
                                    w=0.9674944314427766,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4787820470151587, y=1.6762407135403237, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.3455876693900876,
                                    w=0.9383864677016222,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.49781052019909566, y=1.6600259611014394, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.37708404459197636,
                                    w=0.9261790449552162,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.515700687702008, y=1.642563802638108, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.4058551169315586,
                                    w=0.9139374289635319,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5324647565553278, y=1.624017507195731, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.43172549241016583,
                                    w=0.9020050438901103,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5481454513690664, y=1.6045465291223877, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.5265435578093816,
                                    w=0.8501481528118723,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5592828416717452, y=1.582164936992001, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.640386919959715,
                                    w=0.7680524674424981,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.563778080996542, y=1.5575723462935116, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.673576948624323,
                                    w=0.7391171045794747,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5660928393318159, y=1.5326791574460117, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6809431550374245,
                                    w=0.7323362749500245,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5679086352573108, y=1.5077455328068936, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6854215389312228,
                                    w=0.7281464921079783,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5694184924575438, y=1.4827913088022342, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6881432906755439,
                                    w=0.725574814542397,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5707414294889759, y=1.4578264036450719, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6904789326037788,
                                    w=0.7233525030234887,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5719033862933998, y=1.4328531061538001, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.692916287954038,
                                    w=0.7210180426924119,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5728967334761705, y=1.407872942207348, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6953343973345164,
                                    w=0.7186863543183806,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5737222339767527, y=1.3828866747451798, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6980395118510098,
                                    w=0.7160592432856404,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5743592884296049, y=1.3578943037672957, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7010385311800296,
                                    w=0.713123395914723,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5747865345297214, y=1.3328981180920891, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7047507049200364,
                                    w=0.7094550330462895,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.574952855332981, y=1.3078988806590246, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7093905090279692,
                                    w=0.7048156537003408,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5747911121665084, y=1.28289964322596, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7157075130171352,
                                    w=0.6984001401852861,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5741792347159844, y=1.2579065093086115, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7170605087898521,
                                    w=0.6970109229660885,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5734704639534698, y=1.2329171900885854, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7152383455827858,
                                    w=0.698880611412278,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5728921558393836, y=1.2079232932317723, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7142885851300341,
                                    w=0.699851282168529,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5723817493376373, y=1.1829286334354947, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7137222668042836,
                                    w=0.7004288157034625,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5719117786275092, y=1.1579332106997526, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7137222668042836,
                                    w=0.7004288157034625,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5714418079173811, y=1.1329377879640106, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7140854833128619,
                                    w=0.7000585136414216,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5709458972654602, y=1.1079423652282685, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7151532961397198,
                                    w=0.698967640896554,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5703736926670899, y=1.08294923131092, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7168685196088627,
                                    w=0.6972083803238439,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5696786548149362, y=1.0579583862119648, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7190793053623324,
                                    w=0.6949280197255148,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5688249255541677, y=1.0329736446287257, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7220327165467701,
                                    w=0.6918589135337432,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5677583361828056, y=1.007995769500667, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7241538346502678,
                                    w=0.6896384732316727,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5665383959790802, y=0.9830255237672532, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7261529313262305,
                                    w=0.6875332139804758,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.565173497277101, y=0.9580629074284843, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7272971668048237,
                                    w=0.6863226873400562,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.563725438173492, y=0.9331048687265024, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7281360523342267,
                                    w=0.685432629286882,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5622163439127235, y=0.908150644721843, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7278110729915466,
                                    w=0.6857776914065473,
                                ),
                            ),
                        ),
                    ],
                ),
                required_resources=[],
            ),
            mmrs_msgs.msg.PathSegment(
                uuid=unique_identifier_msgs.msg.UUID(
                    uuid=array(
                        [
                            165,
                            255,
                            191,
                            20,
                            153,
                            200,
                            68,
                            125,
                            163,
                            248,
                            126,
                            228,
                            124,
                            183,
                            234,
                            176,
                        ],
                        dtype=uint8,
                    )
                ),
                path=nav_msgs.msg.Path(
                    header=std_msgs.msg.Header(
                        stamp=builtin_interfaces.msg.Time(sec=65, nanosec=153000000),
                        frame_id="map",
                    ),
                    poses=[
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5622163439127235, y=0.908150644721843, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7278110729915466,
                                    w=0.6857776914065473,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5607309007753543, y=0.8831948948382546, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7266566870552877,
                                    w=0.6870007708567972,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5593293809790794, y=0.8582338043784148, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7249332103022258,
                                    w=0.6888191639326746,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5580529832549814, y=0.833266610402859, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7225506844619264,
                                    w=0.6913179502830819,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5569490098498591, y=0.8082910240931938, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.720531048760102,
                                    w=0.6934226761309926,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.555990757882455, y=0.7833093342678126, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7183784117008021,
                                    w=0.6956525408580299,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5551873826263432, y=0.7583223038661799, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7167408955779797,
                                    w=0.6973395791191516,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5545015000477633, y=0.7333314587672248, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7150999801686884,
                                    w=0.6990221873179288,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5539331101467155, y=0.7083383248498762, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7137650088776358,
                                    w=0.7003852597691562,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5534600876787295, y=0.6833429021141342, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7123848249779051,
                                    w=0.7017890431897605,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.553085484401663, y=0.6583459534994631, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7127594746300713,
                                    w=0.7014085338268026,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5526841782433394, y=0.6333490048847921, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7146194768421589,
                                    w=0.6995134046734481,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5521501206181938, y=0.6083543450885145, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7150679884113214,
                                    w=0.6990549134005042,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5515840195355395, y=0.5833612111711659, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7164107952479339,
                                    w=0.697678702879931,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.550921788080359, y=0.5583696031327463, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.715227930883008,
                                    w=0.6988912697156913,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5503442429057372, y=0.5333764692153977, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7101106799316974,
                                    w=0.7040900668571758,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5501313827951435, y=0.5083772317823332, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7079155360508039,
                                    w=0.7062971002474121,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5500741623353065, y=0.48337723140980415, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7074950996824921,
                                    w=0.7067182493223594,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5500466965145847, y=0.4583772310372751, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7073009671714624,
                                    w=0.7069125418595386,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5500329636042238, y=0.4333772306647461, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7072146688831614,
                                    w=0.706998877026322,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5500253342095789, y=0.40837723029221706, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7071715157815961,
                                    w=0.7070420406645985,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5500207565727919, y=0.38337722991968803, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7071607270942188,
                                    w=0.707052831162973,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5500169418754695, y=0.358377229547159, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7071607270942188,
                                    w=0.707052831162973,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.550013127178147, y=0.33337722917463, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7071499382420925,
                                    w=0.7070636214968246,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.550010075420289, y=0.30837722880210094, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7071391492252396,
                                    w=0.7070744116661304,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5500077866018955, y=0.2833772284295719, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7071391492252396,
                                    w=0.7070744116661304,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.550005497783502, y=0.2583772280570429, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7071391492252396,
                                    w=0.7070744116661304,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5500032089651086, y=0.23337722768451385, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7071391492252396,
                                    w=0.7070744116661304,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5500009201467151, y=0.20837722731198483, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7071391492252396,
                                    w=0.7070744116661304,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5499986313283216, y=0.1833772269394558, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7071391492252396,
                                    w=0.7070744116661304,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5499963425099281, y=0.15837722656692677, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7071499382420925,
                                    w=0.7070636214968246,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5499932907520702, y=0.13337722619439774, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7071607270942188,
                                    w=0.707052831162973,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5499894760547477, y=0.1083772258218687, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7071823043042016,
                                    w=0.7070312500017235,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5499841354784962, y=0.08337722544933968, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7071930926620127,
                                    w=0.7070204591743707,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5499780319627803, y=0.058377225076810646, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7072254567464538,
                                    w=0.7069880857056714,
                                ),
                            ),
                        ),
                    ],
                ),
                required_resources=[
                    unique_identifier_msgs.msg.UUID(
                        uuid=array(
                            [
                                155,
                                33,
                                58,
                                123,
                                41,
                                196,
                                67,
                                177,
                                172,
                                74,
                                144,
                                222,
                                51,
                                214,
                                43,
                                244,
                            ],
                            dtype=uint8,
                        )
                    )
                ],
            ),
            mmrs_msgs.msg.PathSegment(
                uuid=unique_identifier_msgs.msg.UUID(
                    uuid=array(
                        [
                            216,
                            196,
                            231,
                            47,
                            33,
                            214,
                            68,
                            181,
                            157,
                            146,
                            230,
                            157,
                            16,
                            213,
                            121,
                            232,
                        ],
                        dtype=uint8,
                    )
                ),
                path=nav_msgs.msg.Path(
                    header=std_msgs.msg.Header(
                        stamp=builtin_interfaces.msg.Time(sec=65, nanosec=153000000),
                        frame_id="map",
                    ),
                    poses=[
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5499780319627803, y=0.058377225076810646, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7072254567464538,
                                    w=0.7069880857056714,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5499696396286708, y=0.033377224704281616, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7072578193469321,
                                    w=0.7069557107574861,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5499589584761679, y=0.008377224331752586, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7073225400935605,
                                    w=0.7068909564251006,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5499436996868781, y=-0.016622776040776444, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7073872548970189,
                                    w=0.7068261961821024,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5499238632608012, y=-0.04162277641330547, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7074735320482693,
                                    w=0.7067398400056037,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5498979233190084, y=-0.0666227767858345, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7076244915837763,
                                    w=0.7065886914682418,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5498613022247127, y=-0.09162277715836353, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7077969769908319,
                                    w=0.7064159110344556,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5498124740989851, y=-0.11662277753089256, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7080556253539523,
                                    w=0.706156662083297,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5497453354261097, y=-0.1416227779034216, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.70841114985196,
                                    w=0.7058000019590706,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.549653019750906, y=-0.16662201533648613, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7089063299765471,
                                    w=0.705302640941591,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5495256088603355, y=-0.19162201570901516, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7095733112239467,
                                    w=0.704631617228949,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5493508957229665, y=-0.2166212531420797, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7105187945463719,
                                    w=0.703678223761664,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.549109043912722, y=-0.24162049057514423, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7118062294627207,
                                    w=0.702375890601368,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5487756393667382, y=-0.2666182021292798, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7136260867060878,
                                    w=0.700526807747252,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5483125351117906, y=-0.29161362486502185, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7156758122045163,
                                    w=0.698432625115269,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5477029464796601, y=-0.31660599584290594, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7182825816348201,
                                    w=0.6957514879035602,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5469064376787287, y=-0.34159378918400307, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7206794617460714,
                                    w=0.6932684281123674,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5459375045588217, y=-0.36657471606991976, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7234056135736429,
                                    w=0.6904232891858019,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5447717330570754, y=-0.391547250621727, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7261418834663838,
                                    w=0.6875448822265298,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5434075972945607, y=-0.41651062989996035, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.729455184407588,
                                    w=0.6840286060837599,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.541802372661266, y=-0.4414587503889038, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7371225850174703,
                                    w=0.675759050740101,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5396348616426394, y=-0.4663649092073001, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7548569865617246,
                                    w=0.6558894189106517,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5361444135925808, y=-0.4911200060117267, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7496343345738654,
                                    w=0.6618522224998553,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5330468793667364, y=-0.5159269826997388, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.687611259599193,
                                    w=0.7260790285309245,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5344064374924642, y=-0.5408903619779721, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.634546466771898,
                                    w=0.7728847142409407,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5392739912759339, y=-0.5654119993062636, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6521331671404539,
                                    w=0.7581044336470807,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5430101058335595, y=-0.590131237955859, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6711358996934231,
                                    w=0.741334340323379,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5454888961536994, y=-0.61500764213514, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6908101395788172,
                                    w=0.7230362031427576,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5466279647741885, y=-0.6399817025658763, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7025822095674149,
                                    w=0.7116025848740075,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5469468734703469, y=-0.6649794141200118, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7076568358319216,
                                    w=0.7065562983233203,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5469079635576577, y=-0.6899794144925409, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7104758467710361,
                                    w=0.7037215863926438,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5466691635052712, y=-0.7149786519256054, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7114310459662619,
                                    w=0.7027559084314771,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5463624618405447, y=-0.7399763634797409, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7118812405518338,
                                    w=0.702299864267666,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5460237167183095, y=-0.7649740750338765, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7118812405518338,
                                    w=0.702299864267666,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5456849715960743, y=-0.789971786588012, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7110984414376765,
                                    w=0.7030924594851713,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5454019210547472, y=-0.8149702610816121, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7095409814181648,
                                    w=0.7046641722750969,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5452294967357716, y=-0.8399702614541411, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7076568526122046,
                                    w=0.7065562815168999,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5451905868230824, y=-0.8649694988872056, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7053350905313044,
                                    w=0.7088740438647734,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5453157088952594, y=-0.8899694992597347, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7038083040927366,
                                    w=0.7103899429820962,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.54554840543193, y=-0.9149687366927992, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7028210352491974,
                                    w=0.711366707409931,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5458505294598694, y=-0.9399664482469348, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7022998642676661,
                                    w=0.7118812405518338,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5461892745821046, y=-0.9649641598010703, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.702147788896079,
                                    w=0.7120312370594055,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5465387008568428, y=-0.9899618713552059, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7024193308184772,
                                    w=0.7117633621453993,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5468690536449685, y=-1.0149595829093414, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7031033091795174,
                                    w=0.7110877137321472,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5471513412468312, y=-1.0399580574029414, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7042634061681046,
                                    w=0.7099387682980127,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.547351994325993, y=-1.064957294836006, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7060161964327477,
                                    w=0.7081956864981852,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5474290512119069, y=-1.089957295208535, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7079155360508039,
                                    w=0.7062971002474121,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5473718307520699, y=-1.114957295581064, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7101858775189936,
                                    w=0.7040142181608102,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5471536300652247, y=-1.1399565330141286, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7122991605304354,
                                    w=0.7018759904054541,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5467851303038742, y=-1.1649534816287996, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7145767908554641,
                                    w=0.6995570098074254,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5462541244365866, y=-1.1899481414250772, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7163365776570046,
                                    w=0.6977549050423437,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5455972335576575, y=-1.2149389865240323, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7178682948188572,
                                    w=0.6961789362612649,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5448304793958414, y=-1.239927542804594, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7190365515500161,
                                    w=0.6949722566657327,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5439798018929309, y=-1.2649130473272976, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7200437061824755,
                                    w=0.6939287147733583,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5430566451408936, y=-1.2898962630316078, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7202451110239295,
                                    w=0.6937196696405021,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5421189925390308, y=-1.3148787157964534, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7197155978572702,
                                    w=0.6942690099672835,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5412194869103928, y=-1.3398619315007636, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7188029893017137,
                                    w=0.6952138250717692,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5403855940757012, y=-1.3648481989629317, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7173899972981596,
                                    w=0.6966717963119697,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5396531721897873, y=-1.3898375181829579, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7165919672878188,
                                    w=0.6974926181821379,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5389779707637103, y=-1.414828363281913, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7162724358641022,
                                    w=0.6978207489185927,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5383256575215682, y=-1.4398199713203326, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7164749268638367,
                                    w=0.6976128433274862,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5376588484296008, y=-1.4648108164192877, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7171983974153363,
                                    w=0.6968690398811481,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5369401594540477, y=-1.4898008985787783, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7184420644657052,
                                    w=0.6955868026395091,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5361322065611489, y=-1.514787928980411, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7203195505566513,
                                    w=0.6936423754975354,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5351892133830347, y=-1.539769618805792, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7248276975136831,
                                    w=0.6889301916137894,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5339204450535817, y=-1.5647375757208124, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7352470344699905,
                                    w=0.6777992315598216,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5318910260780285, y=-1.5896551786311761, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7341363336177794,
                                    w=0.6790020940043149,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5299432416251761, y=-1.6145788850572558, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7161872762019531,
                                    w=0.69790814969194,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.52929703189875, y=-1.6395704930956754, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7608168646486667,
                                    w=0.648966638947005,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5253549236857111, y=-1.664257688287762, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.8350053938591244,
                                    w=0.5502417579811337,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5154931681676658, y=-1.6872305585031313, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.8860729474778258,
                                    w=0.4635458248630421,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5012368813341368, y=-1.7077673630083723, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9163538181408871,
                                    w=0.40036942937569303,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4842515600361139, y=-1.7261114794926584, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9274232012363036,
                                    w=0.3740136438802824,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.466246188674063, y=-1.7434546193995288, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9385099573036991,
                                    w=0.3452521687720567,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4472062713981586, y=-1.7596556389280522, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9660356244666798,
                                    w=0.2584089245000488,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.42554489412225394, y=-1.7721373285671689, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9773825917885406,
                                    w=0.21147876789104605,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4027810693201559, y=-1.7824721065532003, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9607177072725148,
                                    w=0.2775274525754929,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.3816316244249265, y=-1.795803710755763, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9555044192415272,
                                    w=0.29497678690010826,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.36098266781840493, y=-1.8098959656044258, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.968502330084155,
                                    w=0.24900449116745366,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.33908249049011374, y=-1.8219542238407485, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9815230193273616,
                                    w=0.19134409457963328,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.31591354483236955, y=-1.8313444827697367, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9778623548406838,
                                    w=0.20924916961706827,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.2931024177834729, y=-1.841575500988597, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9659876621751868,
                                    w=0.2585881600640624,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.2714463810838197, y=-1.8540648200223586, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9675566210876708,
                                    w=0.2526542795786555,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.24963775649126774, y=-1.866287873183012, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.986781925475646,
                                    w=0.16205379216351787,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.22595077493713234, y=-1.8742834787709057, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.996934371859385,
                                    w=0.07824230444799918,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.20125671328986527, y=-1.8781836253133974, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9977135554743459,
                                    w=0.06758447471675158,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.17648559475668435, y=-1.881555054806995, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.997748993358672,
                                    w=0.06705927416664155,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.15170989858671646, y=-1.8849005443587998, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9935419229082546,
                                    w=0.11346562221116997,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.1273538191222201, y=-1.8905371411224792, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9806620785743618,
                                    w=0.19570868055917234,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.10426879680557022, y=-1.9001333937068807, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9657876956825654,
                                    w=0.25933400638589715,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.08263183359252935, y=-1.91265628207708, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9350545326119587,
                                    w=0.35450390835903556,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.0639154026495703, y=-1.9292303790037408, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.8822151805527175,
                                    w=0.4708464454600205,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.05000014975667, y=-1.9499998800456524, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9238795325112867,
                                    w=0.38268343236508984,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(
                                    sec=65, nanosec=153000000
                                ),
                                frame_id="map",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.02500014938414097, y=-1.9749998804181814, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9238795325112867,
                                    w=0.38268343236508984,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.4901161193847656e-07,
                                    y=-1.9999998807907104,
                                    z=0.0,
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9436283191604178,
                                    w=0.3310069414355004,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(x=0.0, y=-2.0, z=0.0),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0, y=0.0, z=0.0, w=1.0
                                ),
                            ),
                        ),
                    ],
                ),
                required_resources=[],
            ),
        ],
    ),
    mmrs_msgs.msg.DividedPath(
        robot_namespace="/robot2",
        segments=[
            mmrs_msgs.msg.PathSegment(
                uuid=unique_identifier_msgs.msg.UUID(
                    uuid=array(
                        [
                            107,
                            8,
                            235,
                            228,
                            209,
                            244,
                            70,
                            222,
                            167,
                            23,
                            32,
                            158,
                            185,
                            188,
                            194,
                            232,
                        ],
                        dtype=uint8,
                    )
                ),
                path=nav_msgs.msg.Path(
                    header=std_msgs.msg.Header(
                        stamp=builtin_interfaces.msg.Time(sec=72, nanosec=971000000),
                        frame_id="map",
                    ),
                    poses=[
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.800000175833702, y=-0.04999985173344612, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7855301436475844,
                                    w=0.6188233943711287,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7913911669163554, y=-0.014249271366736593, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.716826275707205,
                                    w=0.6972518128020451,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7906991808220596, y=0.010740810792754019, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7166557981235812,
                                    w=0.6974270334707803,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7900194017591957, y=0.035731655891709124, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7113022349557925,
                                    w=0.7028862856443385,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7897218553680432, y=0.06073013038530917, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7101644864577449,
                                    w=0.7040357960886701,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.789505180560127, y=0.08572860487890921, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7106583558305931,
                                    w=0.7035372778241094,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.789253410536844, y=0.11072784231197375, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7076029279230422,
                                    w=0.70661028608048,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7892183153214773, y=0.13572784268450278, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7717289149481751,
                                    w=0.6359516348221084,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7844400254553534, y=0.16026702762047762, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7612383380694828,
                                    w=0.6484721988283012,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7804658737848058, y=0.18494888223631278, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7142458768434075,
                                    w=0.6998948688283062,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7799585190409175, y=0.20994354203259036, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7664112299648147,
                                    w=0.6423502366963213,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7755891647277622, y=0.23455902091501457, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7034504167965944,
                                    w=0.7107443359666665,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.775847038266761, y=0.2595574954086146, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6609763889852442,
                                    w=0.7504066985335534,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.779002555891907, y=0.2843576056414463, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7734369923471464,
                                    w=0.6338731883184525,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7740922774984256, y=0.30887085063562836, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.8364203738737621,
                                    w=0.5480884583430635,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7641122663633837, y=0.3317926039068766, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.8795509540897429,
                                    w=0.4758047069542535,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7504319988255475, y=0.35271698166008036, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9014796832740097,
                                    w=0.4328214188833439,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7347986062586074, y=0.37222610670664835, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9200520664462691,
                                    w=0.3917961141052193,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7174737768988848, y=0.39024978861584714, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9226650862074927,
                                    w=0.38560230639056114,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.6999086216078467, y=0.40803848516998187, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9343438015865881,
                                    w=0.35637292326539405,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.6812585663982986, y=0.42468735016416304, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.944242725048159,
                                    w=0.3292501726554244,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.6616784879815327, y=0.44023224175322184, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9674528007327081,
                                    w=0.2530515330015588,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.6398805445414837, y=0.4524728425215585, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9953294713194154,
                                    w=0.09653622906977984,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.6153467001817603, y=0.45727707232947523, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9998634186292238,
                                    w=0.01652707134012392,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.5903596697801277, y=0.45645080888942857, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9995426196640359,
                                    w=0.03024155212875945,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.5654054457754683, y=0.4549394258102666, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9989183606447993,
                                    w=0.04649848133763678,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.5405137828068973, y=0.4526170380803478, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9975235209581262,
                                    w=0.07033366999739699,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.5157609748208642, y=0.44910904242260585, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9977570984678699,
                                    w=0.06693857226575305,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.4909852786508964, y=0.4524484284586947, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9978165024127961,
                                    w=0.0660471612765795,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.4662034789652125, y=0.4557435640058429, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9979120659519009,
                                    w=0.06458721721524355,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.4414117610664903, y=0.45896622030386425, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.998046848517927,
                                    w=0.062469898058457596,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.4166070731968716, y=0.46208359095578544, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9982067186488167,
                                    w=0.05986106284023066,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.3917863635984986, y=0.4650712618987427, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9984120134081159,
                                    w=0.05633339579993434,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.3669450546345843, y=0.4678834567648664, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9986885165345055,
                                    w=0.051198114634318274,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.3420755169104837, y=0.47044006691038476, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9991014968307741,
                                    w=0.04238158834336814,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.3171655433947649, y=0.47255722392435473, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.999471113830549,
                                    w=0.0325191115856795,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.2922189487847504, y=0.4741822849837263, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9997924165988585,
                                    w=0.02037458493845909,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.2672395477777627, y=0.47520080916882534, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999585053382691,
                                    w=0.009109753106139186,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.2422433621025561, y=0.4756562840291281, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999988358542355,
                                    w=0.0015258735772116238,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.2172433617300271, y=0.47573257797557744, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999855750595334,
                                    w=0.005371189147128498,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.192244887236427, y=0.47546402328407567, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999586416767623,
                                    w=0.009094775201430743,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.167249464500685, y=0.47500931136323743, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999298860096407,
                                    w=0.011841582020443069,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.1422563305833364, y=0.47441727033879033, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.999935204463511,
                                    w=0.01138362308389929,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.1172624337265233, y=0.47384811749827804, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999406423292015,
                                    w=0.010895495319800715,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.0922685368697103, y=0.47330337872062955, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999455274396475,
                                    w=0.0104375357937248,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.0672738770734327, y=0.4727815281269159, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999506553794557,
                                    w=0.00993412332301941,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.0422792172771551, y=0.4722848545355305, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999561090901425,
                                    w=0.0093690924482065,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.0172830316019486, y=0.47181640970433136, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999627019218796,
                                    w=0.008636826100722363,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.992286845926742, y=0.47138458596742794, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.999971931958167,
                                    w=0.007492349154369925,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.9672898973120709, y=0.47100998269036154, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999762146404279,
                                    w=0.006897111960883432,
                                ),
                            ),
                        ),
                    ],
                ),
                required_resources=[],
            ),
            mmrs_msgs.msg.PathSegment(
                uuid=unique_identifier_msgs.msg.UUID(
                    uuid=array(
                        [
                            159,
                            118,
                            15,
                            45,
                            32,
                            8,
                            75,
                            153,
                            165,
                            6,
                            14,
                            125,
                            22,
                            163,
                            118,
                            184,
                        ],
                        dtype=uint8,
                    )
                ),
                path=nav_msgs.msg.Path(
                    header=std_msgs.msg.Header(
                        stamp=builtin_interfaces.msg.Time(sec=72, nanosec=971000000),
                        frame_id="map",
                    ),
                    poses=[
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.9672898973120709, y=0.47100998269036154, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999762146404279,
                                    w=0.006897111960883432,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.9422921857579354, y=0.4706651340524104, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999693607790257,
                                    w=0.007827994838196175,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.9172952371432643, y=0.47027374610712513, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.999960563855128,
                                    w=0.008880919689678789,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.8922990514680578, y=0.4698297153387898, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999508068184171,
                                    w=0.00991886804009131,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.8673043916717802, y=0.4693338046868689, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.999938292553998,
                                    w=0.011109054153932733,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.8423104948149671, y=0.4687783847567175, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999220895458192,
                                    w=0.012482581396601876,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.817318123837083, y=0.4681543002747617, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9998933525672848,
                                    w=0.014604228557350303,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.7923288046170569, y=0.4674241672072412, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9998345180590826,
                                    w=0.018191660110117722,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.7673455889127467, y=0.46651474336556475, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9997431235221692,
                                    w=0.02266466346842194,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.742370765542546, y=0.46538177826079163, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9995946488773418,
                                    w=0.028469947941358303,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.7174119639010996, y=0.4639588961595109, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9993832431386017,
                                    w=0.035115998259631354,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.6924729986857301, y=0.46220413539117544, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9990989175132013,
                                    w=0.042442349415994625,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.6675630251700113, y=0.4600839266193475, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9990117117177924,
                                    w=0.04444772042171155,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.6426622069278665, y=0.4578637727776709, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9995215718522215,
                                    w=0.03092939381986883,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.6177102717416005, y=0.45631805742260667, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9997506831539161,
                                    w=0.022328715441736913,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5927346854319353, y=0.45743423785916093, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9848027914756524,
                                    w=0.17367631358870664,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5692430163807103, y=0.46598602631667063, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9551404318662727,
                                    w=0.2961532633862236,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.548628392050091, y=0.48012939810945454, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9827596517094032,
                                    w=0.18488771449723884,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5253373760780278, y=0.48921448125264533, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9975407934091388,
                                    w=0.07008826923719645,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5005830422130657, y=0.4927102698789554, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9986536139465229,
                                    w=0.051874457602459234,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.47571808212575206, y=0.49530044936091144, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9996220503548222,
                                    w=0.027491024797584137,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4507554657869832, y=0.49667450333646457, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9996596901688086,
                                    w=0.026086468745339257,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4257897976903564, y=0.4979783668812843, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9997073307765661,
                                    w=0.02419199850350165,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.40081878901747814, y=0.49918762593250676, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9997084358500551,
                                    w=0.02414628936785619,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.37584778034459987, y=0.5003945961653358, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9997197502208005,
                                    w=0.02367321309962706,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.3508760087322571, y=0.5015779152747655, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9997226463253959,
                                    w=0.023550592861908217,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.32590347418044985, y=0.5027551308684792, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9997311945558586,
                                    w=0.023184879381096722,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.3009309396286426, y=0.5039140359150451, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9997326226239357,
                                    w=0.02312321909828724,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.27595764213737084, y=0.5050698892037531, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9997354510002678,
                                    w=0.02300060897652499,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.2509835817066346, y=0.506219638976745, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9997448324277238,
                                    w=0.022589157444720585,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.22600952127589835, y=0.5073487893841957, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.999756435879588,
                                    w=0.022069637906937303,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.20103393496623312, y=0.5084519998498536, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9997716945838362,
                                    w=0.021367234471603838,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.1760568227776389, y=0.5095201151001447, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9997874038790614,
                                    w=0.020619094179101228,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.1510781847101157, y=0.5105508463166757, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9998255225001603,
                                    w=0.018679522405066245,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.12609496900580552, y=0.511484684221216, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9998686522563945,
                                    w=0.01620735126358164,
                                ),
                            ),
                        ),
                    ],
                ),
                required_resources=[
                    unique_identifier_msgs.msg.UUID(
                        uuid=array(
                            [
                                155,
                                33,
                                58,
                                123,
                                41,
                                196,
                                67,
                                177,
                                172,
                                74,
                                144,
                                222,
                                51,
                                214,
                                43,
                                244,
                            ],
                            dtype=uint8,
                        )
                    )
                ],
            ),
            mmrs_msgs.msg.PathSegment(
                uuid=unique_identifier_msgs.msg.UUID(
                    uuid=array(
                        [
                            70,
                            153,
                            215,
                            3,
                            187,
                            61,
                            68,
                            80,
                            149,
                            147,
                            9,
                            148,
                            255,
                            175,
                            65,
                            39,
                        ],
                        dtype=uint8,
                    )
                ),
                path=nav_msgs.msg.Path(
                    header=std_msgs.msg.Header(
                        stamp=builtin_interfaces.msg.Time(sec=72, nanosec=971000000),
                        frame_id="map",
                    ),
                    poses=[
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.12609496900580552, y=0.511484684221216, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9998686522563945,
                                    w=0.01620735126358164,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.10110870154363738, y=0.5122949259325082, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999003676806676,
                                    w=0.014115761122436806,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.07611861938414677, y=0.5130006449371649, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999350306946144,
                                    w=0.011398876688542899,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.05112472252733369, y=0.5135705607171417, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999571013025406,
                                    w=0.009262588980447085,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.02612929979159162, y=0.5140336649720894, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999790698960402,
                                    w=0.0064699126617125055,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.0011315882374560715, y=0.5143571513050347, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999930124388593,
                                    w=0.0037383249531647345,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.02386841213507296, y=0.5145440714738356, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999999905703594,
                                    w=0.00013732909767766885,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.04886841250760199, y=0.5145509379290161, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999954358897227,
                                    w=0.0030212910689773856,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.07386764994066652, y=0.5143998759150463, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.999974063479684,
                                    w=0.007202247422092251,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.09886536149480207, y=0.5140397684878053, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999576646949057,
                                    w=0.009201566057510123,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.12386078423054414, y=0.5135797159907156, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999307862867778,
                                    w=0.011765314951426334,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.14885391814789273, y=0.512991489663591, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999092110912282,
                                    w=0.013474775505277871,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.17384476324684783, y=0.5123178141164431, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9998766978872395,
                                    w=0.015703153253719866,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.19883255658794496, y=0.5115327494074791, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.999866667385617,
                                    w=0.01632934325623315,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.2238188240501131, y=0.5107164041804708, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9998541771158203,
                                    w=0.017077016836843425,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.24880432857281676, y=0.5098626749197024, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9998585793923884,
                                    w=0.016817289182112206,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.2737905960349849, y=0.5090219156298303, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9998646676628423,
                                    w=0.016451333060695475,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.29877686349715304, y=0.5081994668871062, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9998840176713907,
                                    w=0.01522994436359395,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.32376541977771467, y=0.5074380533015415, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999085934416958,
                                    w=0.013520531108265423,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.3487562648766698, y=0.506762088936, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999409742916419,
                                    w=0.010864986547713206,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.37375016173348286, y=0.5062188760372806, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.999973843244671,
                                    w=0.00723276064046407,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.3987478732876184, y=0.5058572427311105, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999629649932363,
                                    w=0.008606314073725741,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.42374405896282497, y=0.5054269448731361, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999364104564799,
                                    w=0.011277191290844911,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.44873719288017355, y=0.5048631326088753, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.999998927122725,
                                    w=0.001464839035205618,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.4737371932527026, y=0.5047898904202839, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9998115611280407,
                                    w=0.019412424750921947,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.49871888307808376, y=0.5057603494191198, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9971092613877418,
                                    w=0.07598105589416294,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.5234297293935697, y=0.5019723549779087, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9734102354166212,
                                    w=0.22906879662266993,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.5458059809477049, y=0.4908235205832625, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.8602362305755208,
                                    w=0.5098957026738109,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.5578062557847261, y=0.5127549784295979, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9442717665021976,
                                    w=0.32916687407274037,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.5773886230198855, y=0.5282960553213343, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9878083579495226,
                                    w=0.15567481480659554,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.6011770755227985, y=0.5359849592445016, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.992678401457626,
                                    w=0.12078738046473413,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.6254477057672716, y=0.541980137556493, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9972506414413109,
                                    w=0.07410234911859388,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.6501730479325829, y=0.5456750533830359, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9977270975871189,
                                    w=0.06738426181523266,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.6749456923446928, y=0.5490365646635951, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9983358461462771,
                                    w=0.057667480432188174,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.6997793719139622, y=0.5519151352631297, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9986385317656952,
                                    w=0.052163999778168116,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.7246435690618114, y=0.5545198105949112, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9989884885220435,
                                    w=0.04496665209288897,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.7495428614250272, y=0.5567659043783806, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9992373908001304,
                                    w=0.03904659814820693,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.7744665678511069, y=0.5587167405890909, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9995048839501921,
                                    w=0.03146405822065905,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.7994169771584438, y=0.5602891588254124, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9997017560145117,
                                    w=0.024421282142870725,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.8243872228918576, y=0.5615098619686023, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9998863288761958,
                                    w=0.015077444295507645,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.8493757791724192, y=0.5622636461595221, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999733983590622,
                                    w=0.00729400947547058,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.8743727277870903, y=0.56262833122355, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999935163634491,
                                    w=0.0036010041744293734,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.8993727281596193, y=0.5624482775099295, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9998585707623366,
                                    w=0.01681780226716399,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.924358232682323, y=0.5616075182200575, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9993170384310396,
                                    w=0.03695208656376717,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.9492895685030476, y=0.5597612047159828, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9994254438790926,
                                    w=0.03389368860243371,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.9742323484157396, y=0.5580674791048068, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9996216314005848,
                                    w=0.02750625448935719,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.9991949647545084, y=0.5566926621897892, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9997124699883753,
                                    w=0.0239786853213838,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.0241659734273867, y=0.5554940842910696, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9998226518848891,
                                    w=0.018832545708639133,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.0491484261922324, y=0.5545526169918844, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9998676616775665,
                                    w=0.01626834753241317,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.0741346936544005, y=0.5537393235227341, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999186245497642,
                                    w=0.01275712657723901,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.0991270646322846, y=0.5531015061304174, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999393051800084,
                                    w=0.011017529492673908,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.1241209614890977, y=0.552550663837053, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999623055702334,
                                    w=0.008682594005431227,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.1491171471643042, y=0.5521165512817561, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.999972160117743,
                                    w=0.007461835528533077,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.1741140957789753, y=0.5517434738836187, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999828333875778,
                                    w=0.005859430872693092,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.1991125702725753, y=0.5514505051292531, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999870125146753,
                                    w=0.005096548044975893,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.2241110447661754, y=0.5511956833481122, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999918246026416,
                                    w=0.004043603328674919,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.24911028219924, y=0.5509935043900214, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999937338952167,
                                    w=0.0035400805502785555,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.2741095196323045, y=0.5508165024342588, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999958855099769,
                                    w=0.002868616934577177,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.2991095200048335, y=0.550673069814934, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999966749198558,
                                    w=0.0025787883264029735,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.324108757437898, y=0.5505441330454346, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999975860439365,
                                    w=0.0021972497126791987,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.349108757810427, y=0.5504342697625475, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999978150270825,
                                    w=0.002090440398795645,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.374108758182956, y=0.5503297470559119, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999980020966744,
                                    w=0.0019989503894690857,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.3991079956160206, y=0.5502298019860632, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999978150270825,
                                    w=0.002090440398795645,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.4241079959885496, y=0.5501252792794276, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999974500745528,
                                    w=0.002258283505739458,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.4491079963610787, y=0.5500123642386825, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999950596321061,
                                    w=0.003143359887197608,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.4741072337941432, y=0.5498551987089968, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999898689027822,
                                    w=0.004501343332448534,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.4991064712272077, y=0.5496301315669712, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999683978860334,
                                    w=0.007950045863990009,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.5241034198418788, y=0.54923264010597, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9998554763246583,
                                    w=0.01700077832308508,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.5490889243645825, y=0.548382725542524, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9994269907979902,
                                    w=0.0338480437318587,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.5740317042772745, y=0.5466912887497415, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9968072722805574,
                                    w=0.07984523735699407,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.5987127959536451, y=0.5427117965029424, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9888792875928816,
                                    w=0.14872039056496295,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.6226065341026583, y=0.5353585859441523, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9749750529026708,
                                    w=0.22231429602577119,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.6451353735496923, y=0.5245210308510195, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9499361426737397,
                                    w=0.3124441147503605,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.6652540872283907, y=0.5096810953271529, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9252894218547913,
                                    w=0.37926176422574676,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.6830626202086023, y=0.49213425058326266, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9246538946185363,
                                    w=0.3808085807420477,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.7008116439105834, y=0.4745286595006064, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9170310514565577,
                                    w=0.39881581044948583,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.7178587633052302, y=0.4562425264156218, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9012681354386636,
                                    w=0.43326175465059774,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.733473082385558, y=0.43671814257976393, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9033402357422016,
                                    w=0.4289247235695602,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.7492743216346867, y=0.4173448207578758, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.8914044266475363,
                                    w=0.45320872470990364,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.7640043938756662, y=0.39714523549594105, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.8503529029323379,
                                    w=0.5262128281166718,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.7751593317860284, y=0.3747720356996638, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.8291633019709461,
                                    w=0.559006456728934,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.7845350948651912, y=0.35159622358673914, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.8671716918821859,
                                    w=0.49800929388736037,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.79713427718184, y=0.3300035108626389, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.872880373099362,
                                    w=0.4879342724772659,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.810230133089874, y=0.3087083445296912, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.8401338232055487,
                                    w=0.5423791654424308,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.8205214235264293, y=0.2859246833015163, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7912725091625118,
                                    w=0.6114636671492941,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.8268271182004696, y=0.2617326358218861, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7723128247405221,
                                    w=0.6352423952644499,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.8316504214949987, y=0.23720260615948519, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7890223564431904,
                                    w=0.6143644854911741,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.837778351273812, y=0.21296478231198535, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.8353998628480197,
                                    w=0.5496426740651692,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.8476729131888305, y=0.1900064079464414, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.8454761723271829,
                                    w=0.5340131478034749,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.858414337909437, y=0.1674317921315378, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.8483936098650688,
                                    w=0.5293659251785265,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.8694029550165396, y=0.1449761948730952, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.8709081264776156,
                                    w=0.4914458619576011,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.8823271495450626, y=0.12357574289404738, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9076478683811321,
                                    w=0.41973247077535836,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.8985182508605476, y=0.10452743328403358, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.914759944881936,
                                    w=0.4039978257857303,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.9153578507208522, y=0.08604903945399656, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9176412538639429,
                                    w=0.39740977492634816,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.932460664696407, y=0.06781554919206201, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9223777914628265,
                                    w=0.3862890236806615,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.9499998800456524, y=0.05000014975667, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9238795325112867,
                                    w=0.38268343236508984,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(
                                    sec=72, nanosec=971000000
                                ),
                                frame_id="map",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.9749998804181814, y=0.02500014938414097, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9238795325112867,
                                    w=0.38268343236508984,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.9999998807907104,
                                    y=1.4901161193847656e-07,
                                    z=0.0,
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9013032363068559,
                                    w=0.4331887304891343,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(x=-2.0, y=0.0, z=0.0),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0, y=0.0, z=0.0, w=1.0
                                ),
                            ),
                        ),
                    ],
                ),
                required_resources=[],
            ),
        ],
    ),
]
