import builtin_interfaces.msg
import geometry_msgs.msg
import mmrs_msgs.msg
import nav_msgs.msg
import std_msgs.msg
import unique_identifier_msgs.msg
from numpy import array, uint8

no_collision = [
    mmrs_msgs.msg.DividedPath(
        robot_namespace="/robot1",
        segments=[
            mmrs_msgs.msg.PathSegment(
                uuid=unique_identifier_msgs.msg.UUID(
                    uuid=array(
                        [
                            140,
                            188,
                            58,
                            31,
                            84,
                            44,
                            67,
                            87,
                            160,
                            142,
                            107,
                            181,
                            6,
                            122,
                            106,
                            214,
                        ],
                        dtype=uint8,
                    )
                ),
                path=nav_msgs.msg.Path(
                    header=std_msgs.msg.Header(
                        stamp=builtin_interfaces.msg.Time(sec=14, nanosec=615000000),
                        frame_id="map",
                    ),
                    poses=[
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.4901161193847656e-07,
                                    y=0.5000001564621925,
                                    z=0.0,
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.07238038520016592,
                                    w=0.9973771001172403,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.03695235903489902, y=0.5053918496577694, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.0024108676526738153,
                                    w=0.9999970938543578,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.06195235940742805, y=0.5055123940931594, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0, y=0.0, z=0.0, w=1.0
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.08695235977995708, y=0.5055123940931594, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.007400808089510212,
                                    w=0.9999726136448049,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.11194930839462813, y=0.50514236845288, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.012269040390347043,
                                    w=0.999924732491351,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.13694167937251223, y=0.504528965123427, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.0226646634684219,
                                    w=0.9997431235221692,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.16191650274271296, y=0.5033960000186539, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.031143451278174732,
                                    w=0.9995149250723993,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.1868676749895144, y=0.5018396035110868, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.04440081646866721,
                                    w=0.9990137974507237,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.21176925617112374, y=0.49962173848780367, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.059018376096950834,
                                    w=0.9982568964364228,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.23659454340628372, y=0.49667602921539356, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.17844764975366387,
                                    w=0.9839494073870839,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.26000228911641443, y=0.5054551736333224, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.19711735434646577,
                                    w=0.9803799001486362,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.28305984561234254, y=0.5151178019511349, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.21387386318848112,
                                    w=0.9768612852625673,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.3057725534703195, y=0.5255639690989824, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.23812610111563534,
                                    w=0.9712342456727245,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.32793747079279, y=0.5371278425623132, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.23137976610407548,
                                    w=0.972863507300805,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.3502603165844107, y=0.548382725542524, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.2881230582190052,
                                    w=0.9575933914363276,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.3711099262700941, y=0.5621781969394988, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.31973458315922926,
                                    w=0.9475071484331894,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.39099823223051544, y=0.5773255970675564, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.258838592505219,
                                    w=0.9659205883663093,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4126481654144527, y=0.5898263601932854, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.3473544511154469,
                                    w=0.9377339096408355,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4316156034412302, y=0.606112828941832, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.4013389408045345,
                                    w=0.9159296122486129,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4485620148265639, y=0.6244928035809494, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.4773450491625492,
                                    w=0.8787159404722344,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.46216904017580873, y=0.6454652465204163, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.49764827516019167,
                                    w=0.8673789219424727,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4747865330396053, y=0.6670480410314781, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.523145541047795,
                                    w=0.8522433589543599,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4861024511769756, y=0.689340369244519, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.5424311739674974,
                                    w=0.8401002449161901,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4963906898556729, y=0.7121247934121584, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6062755207262377,
                                    w=0.7952546717675598,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5030122414680136, y=0.7362321546112298, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6121603423895555,
                                    w=0.7907336562999593,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5092752115320422, y=0.760434883243363, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6206404696442804,
                                    w=0.784095279567303,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5150155680628927, y=0.78476731158446, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6277380566904761,
                                    w=0.7784246477228638,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5203126567648724, y=0.8091989220559412, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6353532750704335,
                                    w=0.7722216105868017,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.525129093604221, y=0.8337312405367356, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6428114574174874,
                                    w=0.7660244318641579,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5294686932782611, y=0.8583512970559468, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.649096392934195,
                                    w=0.760706167110414,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5334024091571905, y=0.8830400181269624, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6537218721248657,
                                    w=0.7567349033218705,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5370347639476449, y=0.9077745155658477, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6590924957553127,
                                    w=0.7520618871070606,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5403146407055033, y=0.932558604069925, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6633731903352236,
                                    w=0.7482887212463296,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5433114669220345, y=0.9573785507288335, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6674774973129647,
                                    w=0.7446299688978555,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5460351608102769, y=0.9822297779057862, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.669622856060749,
                                    w=0.7427013064759247,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5486154220791946, y=1.0070962638720289, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6728618529270105,
                                    w=0.7397681575166845,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5509782456007315, y=1.0319841121432773, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6696566722918743,
                                    w=0.7426708162133296,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5535562180512557, y=1.05685059810952, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6731908732261054,
                                    w=0.7394687608040476,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5558969163283223, y=1.081740735199162, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6754198965189261,
                                    w=0.7374333619971117,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5580873155308836, y=1.1066446051991647, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6781373433200089,
                                    w=0.7349351968676425,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.560093846322502, y=1.131564496927922, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7164110772454476,
                                    w=0.6976784133109016,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5594316148673215, y=1.156555342026877, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7166983483265822,
                                    w=0.6973833074471664,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5587487840465997, y=1.1815461871258321, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7145234293193106,
                                    w=0.6996115128796639,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5582215928766345, y=1.2065408469221097, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.714726410424599,
                                    w=0.6994041451418255,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.557679905856844, y=1.2315347437789228, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7151746212590251,
                                    w=0.698945821295907,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5571061753795448, y=1.2565278776962714, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7152809880046064,
                                    w=0.6988369682545094,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5565248155076006, y=1.2815217745530845, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7157929968266629,
                                    w=0.6983125272354098,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5559068345413607, y=1.3065141455309686, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7157184273493706,
                                    w=0.6983889552051519,
                                ),
                            ),
                        ),
                    ],
                ),
                required_resources=[],
            ),
            mmrs_msgs.msg.PathSegment(
                uuid=unique_identifier_msgs.msg.UUID(
                    uuid=array(
                        [
                            115,
                            19,
                            18,
                            216,
                            236,
                            114,
                            69,
                            59,
                            142,
                            254,
                            120,
                            159,
                            200,
                            244,
                            122,
                            20,
                        ],
                        dtype=uint8,
                    )
                ),
                path=nav_msgs.msg.Path(
                    header=std_msgs.msg.Header(
                        stamp=builtin_interfaces.msg.Time(sec=14, nanosec=615000000),
                        frame_id="map",
                    ),
                    poses=[
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5559068345413607, y=1.3065141455309686, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7157184273493706,
                                    w=0.6983889552051519,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5552941941513723, y=1.3315065165088527, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7156331941414039,
                                    w=0.6984762926849928,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5546876572770998, y=1.3564988874867367, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7151639587900446,
                                    w=0.6989567311699283,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.554114689739265, y=1.3814920214040853, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7089601367946575,
                                    w=0.7052485550755141,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5539834641513721, y=1.4064920217766144, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7088309933592704,
                                    w=0.7053783543980564,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5538613938370531, y=1.4314920221491434, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7185060553948591,
                                    w=0.6955207030426338,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5530488633073674, y=1.4564782896113115, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7163150112893082,
                                    w=0.6977770450520698,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5523934983073673, y=1.4814698976497311, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7273494385638362,
                                    w=0.6862672906534832,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5509416245064358, y=1.506427936351713, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7315021589752756,
                                    w=0.6818391242914348,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5491868637381003, y=1.531366138627618, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7322629756375628,
                                    w=0.6810219779936784,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5473764083888568, y=1.5563005262062006, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7673676883444753,
                                    w=0.6412073228565439,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5429338118871101, y=1.580902272178264, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7929870592633715,
                                    w=0.6092384786278934,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5364923139883899, y=1.605058461503063, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.8469272527406524,
                                    w=0.5317087817265869,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5256280560139999, y=1.6275743309792006, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9055322355722465,
                                    w=0.42427746857396226,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5096284525041028, y=1.646784383755687, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9228147953329933,
                                    w=0.38524388835454076,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.49204956430270386, y=1.6645593473994609, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9346243987351155,
                                    w=0.35563637790448793,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4733728062118985, y=1.6811792206939913, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9446109193065939,
                                    w=0.32819233861678054,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.45375839551923036, y=1.6966798617941095, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9796606271964776,
                                    w=0.20066154469903813,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.43077179239350016, y=1.7065088109151816, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.999926961619832,
                                    w=0.012086001229980643,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.40577942141561607, y=1.7071130589710606, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9943174018375884,
                                    w=0.1064561149157142,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.38134628506520585, y=1.701820547905868, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.970407719194953,
                                    w=0.24147227279099642,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.3592614763865072, y=1.6901040865496384, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9502256163831325,
                                    w=0.3115626389175952,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.33911529688708697, y=1.7049066380397448, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9630997872875003,
                                    w=0.26914457030891714,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.31773697015250946, y=1.7178674536625635, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9713981656896583,
                                    w=0.23745653011607648,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.2955560311012846, y=1.7294008095473146, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9763843274409284,
                                    w=0.21604084134192303,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.27288986255064174, y=1.7399476847044753, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9786712184823829,
                                    w=0.20543282628199416,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.25000015273690224, y=1.750000175088644, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9238801597772125,
                                    w=0.38268191800767426,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(
                                    sec=14, nanosec=615000000
                                ),
                                frame_id="map",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.22500007636845112, y=1.775000087544322, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9238801597772116,
                                    w=0.3826819180076763,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(x=0.2, y=1.8, z=0.0),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0, y=0.0, z=0.0, w=1.0
                                ),
                            ),
                        ),
                    ],
                ),
                required_resources=[],
            ),
        ],
    ),
    mmrs_msgs.msg.DividedPath(
        robot_namespace="/robot2",
        segments=[
            mmrs_msgs.msg.PathSegment(
                uuid=unique_identifier_msgs.msg.UUID(
                    uuid=array(
                        [
                            162,
                            89,
                            60,
                            45,
                            115,
                            205,
                            77,
                            248,
                            162,
                            118,
                            204,
                            44,
                            64,
                            59,
                            206,
                            168,
                        ],
                        dtype=uint8,
                    )
                ),
                path=nav_msgs.msg.Path(
                    header=std_msgs.msg.Header(
                        stamp=builtin_interfaces.msg.Time(sec=27, nanosec=721000000),
                        frame_id="map",
                    ),
                    poses=[
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.4901161193847656e-07,
                                    y=-0.49999985843896866,
                                    z=0.0,
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.19188971027101348,
                                    w=0.9814164962400553,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.03042464897669106, y=-0.5123701589162692, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.03941379312692294,
                                    w=0.9992229745714157,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.055346829523841734, y=-0.5143393056741274, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.04466028346122129,
                                    w=0.9990022317698611,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.08024764776598659, y=-0.516570140668307, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.045181935411853745,
                                    w=0.9989787749058731,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.10514541425027346, y=-0.5188269156042793, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.04945429887771678,
                                    w=0.9987763875475398,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.130022581369019, y=-0.5212965506508453, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.05389548048994815,
                                    w=0.9985465823799897,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.15487762324329424, y=-0.5239874381421146, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.05656389196217578,
                                    w=0.9983989814328195,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.1797174063282796, y=-0.5268110771002057, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.051765208062547724,
                                    w=0.9986592828558903,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.20458389229452223, y=-0.5293959160059103, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.05273107606947283,
                                    w=0.9986087490186312,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.2294450376845134, y=-0.532028820097878, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.04830480033502999,
                                    w=0.9988326417696775,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.2543283083189749, y=-0.534441234684607, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.047447904991018114,
                                    w=0.9988737138957874,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.27921539365075887, y=-0.5368109246613244, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.044262812660874194,
                                    w=0.9990199214306732,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.3041177377718327, y=-0.5390219232294271, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.04068375306841798,
                                    w=0.9991720733868956,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.3290345777427319, y=-0.5410543939628383, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.030378565049394003,
                                    w=0.9995384648854388,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.35398880174739134, y=-0.5425726434971807, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.02399392230430282,
                                    w=0.9997121044042905,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.3789598104202696, y=-0.5437719843353648, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.21287328145931422,
                                    w=0.9770798155937639,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4016938805832524, y=-0.5541716121758782, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.14836760688835277,
                                    w=0.9889322793933987,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.42559372224798153, y=-0.5615080380664494, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.11398036772362109,
                                    w=0.9934830022570029,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.44994369819676194, y=-0.5671698118324571, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.019107534843567742,
                                    w=0.9998174343910001,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4749253880221431, y=-0.5681250120420032, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.037456157033283526,
                                    w=0.9992982719390132,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.49985519796393874, y=-0.5699965025484062, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.00872809540632999,
                                    w=0.999961909449844,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5248521465786098, y=-0.5695601011747158, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.006607227914886162,
                                    w=0.9999781720314103,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5498498581327453, y=-0.56922974838659, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.009064263356303419,
                                    w=0.999958918721068,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5748452808684874, y=-0.5696829344284993, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.012971051508592318,
                                    w=0.9999158723726519,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.599836888906907, y=-0.5703314329733189, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.01987094133874533,
                                    w=0.9998025533525667,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.6248170528533592, y=-0.5713247801560897, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.02333655162463969,
                                    w=0.9997276655961215,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.649790350344631, y=-0.5724913145973005, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.028500404398627614,
                                    w=0.9995937809676062,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.6747491519860773, y=-0.5739157225775102, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.031172944750851087,
                                    w=0.9995140056625321,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.6997010871723433, y=-0.5754736449640063, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.03497909278507884,
                                    w=0.9993880442890704,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.7246400523877128, y=-0.5772215392771614, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.03728889376703624,
                                    w=0.9993045273597186,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.7495698623295084, y=-0.5790846374494549, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.04282468282651003,
                                    w=0.9990826024612823,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.7744783099662982, y=-0.5812239197078952, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.04404888710025694,
                                    w=0.9990293767178364,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.7993814170268365, y=-0.583424237123495, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.04772245938174514,
                                    w=0.998860634358246,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.824267739419156, y=-0.5858076600105733, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.04793777474451989,
                                    w=0.998850323998818,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.8491525359325465, y=-0.5882017640501545, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.050203416295928775,
                                    w=0.9987390134525724,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.874026651293434, y=-0.5907087831304807, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.049191970562878065,
                                    w=0.9987893421698797,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.8989061072305731, y=-0.5931654482061504, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.05002145702360291,
                                    w=0.998748143346077,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.9237802225914606, y=-0.5956633120129027, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.047064244789666344,
                                    w=0.9988918644489895,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.9486695967416381, y=-0.5980139285030077, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.043574133906104734,
                                    w=0.9990501963636926,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.9735749926205699, y=-0.6001905947952082, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.03927579614839415,
                                    w=0.999228408241534,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.9984979361071851, y=-0.602152875097886, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.03525504682476154,
                                    w=0.9993783476108454,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.0234353754436256, y=-0.6039145023214019, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.03071440022410315,
                                    w=0.9995282015125304,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.0483888365088205, y=-0.6054495365239632, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.02555172109843599,
                                    w=0.9996735014738101,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.0733560304843763, y=-0.6067266971875256, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.01935203840785663,
                                    w=0.9998127317700355,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.098336957370293, y=-0.6076941044285036, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.01883254570863903,
                                    w=0.9998226518848891,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.1233194101351387, y=-0.6067526371293184, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.01661906543901592,
                                    w=0.999861893795305,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.1483056775973068, y=-0.6059217960524848, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.006591970719481655,
                                    w=0.9999782727249795,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.1733033891514424, y=-0.6055922062038235, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.004501343332448551,
                                    w=0.9999898689027822,
                                ),
                            ),
                        ),
                    ],
                ),
                required_resources=[],
            ),
            mmrs_msgs.msg.PathSegment(
                uuid=unique_identifier_msgs.msg.UUID(
                    uuid=array(
                        [
                            201,
                            157,
                            177,
                            3,
                            128,
                            82,
                            68,
                            94,
                            182,
                            115,
                            16,
                            52,
                            196,
                            36,
                            85,
                            104,
                        ],
                        dtype=uint8,
                    )
                ),
                path=nav_msgs.msg.Path(
                    header=std_msgs.msg.Header(
                        stamp=builtin_interfaces.msg.Time(sec=27, nanosec=721000000),
                        frame_id="map",
                    ),
                    poses=[
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.1733033891514424, y=-0.6055922062038235, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.004501343332448551,
                                    w=0.9999898689027822,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.198302626584507, y=-0.6053671390617978, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.002426200080553414,
                                    w=0.9999970567722533,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.2233018640175715, y=-0.6054884464366523, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.004745467918494921,
                                    w=0.9999887402037257,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.248301101450636, y=-0.6057257206101099, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.011551763893356552,
                                    w=0.9999332761494399,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.2732942353679846, y=-0.6063032657847316, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.014268271253657129,
                                    w=0.9998982030364051,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.2982843175274752, y=-0.6070166141840332, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.018710583265339878,
                                    w=0.9998249417142336,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.3232667702923209, y=-0.6079519779675024, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.02387202503829939,
                                    w=0.9997150226042274,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.3482377789651991, y=-0.6091452152899706, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.03158582485940848,
                                    w=0.9995010433551087,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.373188188272536, y=-0.610723737042008, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.04131048991200646,
                                    w=0.9991463573587355,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.3981027394250418, y=-0.6127874882934634, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.05587240723980127,
                                    w=0.9984379170029701,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.4229471001468141, y=-0.6155767949756523, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.07388715182934148,
                                    w=0.9972666086832286,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.4476739681910544, y=-0.6192610296496923, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.0769492248316697,
                                    w=0.997035012824427,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.47237794805136, y=-0.6230970892771666, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.119979071594599,
                                    w=0.992776421143904,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.496657733569407, y=-0.6290525947370043, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.16472440647115547,
                                    w=0.9863396321312075,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.5203012275740662, y=-0.637176374154933, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.2777792328088949,
                                    w=0.9606449384762831,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.5414430430746506, y=-0.6505186595099985, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.37094900057191804,
                                    w=0.9286532393604704,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.5595628553563756, y=-0.6332945381595891, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.295837981908249,
                                    w=0.955238131808218,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.5801866349605689, y=-0.6191648992771661, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.009232077557660544,
                                    w=0.9999573834639001,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.605182057696311, y=-0.6187033209011474, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.0003814696432958045,
                                    w=0.999999927240453,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.63018205806884, y=-0.6186842474145351, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6537104476770104,
                                    w=0.7567447724285399,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.633815175798759, y=-0.5939497499756499, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.20626216933836491,
                                    w=0.9784967641744308,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.6566881009442795, y=-0.5838583496787919, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.2897233916726899,
                                    w=0.9571104201280399,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.6774911713226288, y=-0.5699934507905482, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.40390954680786006,
                                    w=0.9147989276324437,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.6943338229407914, y=-0.5515188716578336, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7135510742375675,
                                    w=0.7006032147045954,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.6938760592620952, y=-0.5265226859826271, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.47123468755918246,
                                    w=0.8820078623464758,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7077730016078476, y=-0.5057409779092836, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.25276042434966134,
                                    w=0.9675288976989675,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7295785744425416, y=-0.49351334711184336, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.3378815909743024,
                                    w=0.9411886264084762,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7488702617417289, y=-0.4776129257323305, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.43002227346315613,
                                    w=0.9028182786837995,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.764624198744059, y=-0.4582014569372177, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.711752503578313,
                                    w=0.7024303336630641,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7642946088953977, y=-0.43320298244361766, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7250282139046388,
                                    w=0.6887191655836575,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7630113447161193, y=-0.40823655140752635, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6084486186749661,
                                    w=0.7935932701532478,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7695009078011026, y=-0.3840933320536237, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.5070498108928688,
                                    w=0.8619167530994545,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7816461411363775, y=-0.36224121991159564, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6391932362615957,
                                    w=0.7690461668309828,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7862176744076237, y=-0.3376631250629316, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6325831196514858,
                                    w=0.7744924768724316,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7912095873238059, y=-0.3131666647369684, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6844083118741805,
                                    w=0.729098938853661,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7927888720153078, y=-0.2882162554296315, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6863448451742067,
                                    w=0.7272762566609708,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7942354052399878, y=-0.2632582167276496, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6981268866806271,
                                    w=0.7159740568579388,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.794866356177124, y=-0.2382658457497655, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7010276512226002,
                                    w=0.7131340913329866,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.795294365216705, y=-0.21326966007455894, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7088848602636303,
                                    w=0.7053242196954628,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7951684802050636, y=-0.1882704226414944, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7127701757352717,
                                    w=0.7013976593789787,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7947664111072754, y=-0.16327347402682335, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7203191513211584,
                                    w=0.6936427900872076,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7938234179291612, y=-0.13829102126197768, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7257116692723191,
                                    w=0.6879989629948465,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7924905626846908, y=-0.11332687904427985, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.8202354604415992,
                                    w=0.5720260391224841,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7838510361887643, y=-0.08986649051109907, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7967048085507736,
                                    w=0.6043686358772064,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.777114280717285, y=-0.06579193570900088, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.8201720885466781,
                                    w=0.572116898167656,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.76848009479761, y=-0.04233002129689112, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.852735642861156,
                                    w=0.5223427259895279,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7571222149896926, y=-0.020059055388855995, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6950380458555558,
                                    w=0.7189729583324328,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.757968314855816, y=0.004926449133847655, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6934116897382142,
                                    w=0.7205416216530414,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7589273297626846, y=0.029908138959228836, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.8318842318199671,
                                    w=0.5549492092518947,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7493257366020316, y=0.05299087245748524, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.8257904179351815,
                                    w=0.5639771144704706,
                                ),
                            ),
                        ),
                    ],
                ),
                required_resources=[],
            ),
            mmrs_msgs.msg.PathSegment(
                uuid=unique_identifier_msgs.msg.UUID(
                    uuid=array(
                        [
                            175,
                            84,
                            202,
                            105,
                            43,
                            13,
                            64,
                            42,
                            150,
                            201,
                            101,
                            246,
                            24,
                            227,
                            141,
                            41,
                        ],
                        dtype=uint8,
                    )
                ),
                path=nav_msgs.msg.Path(
                    header=std_msgs.msg.Header(
                        stamp=builtin_interfaces.msg.Time(sec=27, nanosec=721000000),
                        frame_id="map",
                    ),
                    poses=[
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7493257366020316, y=0.05299087245748524, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.8257904179351815,
                                    w=0.5639771144704706,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7402292093668734, y=0.07627731079276145, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7226986729772945,
                                    w=0.6911632427124995,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7391145548092481, y=0.10125213416296219, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6428960890102102,
                                    w=0.7659534050680732,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7434488139070368, y=0.12587371656110236, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6074317408218775,
                                    w=0.7943718778016399,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.750000175088644, y=0.15000015124678612, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.38268354250047004,
                                    w=0.9238794868917108,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(
                                    sec=27, nanosec=721000000
                                ),
                                frame_id="map",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.775000087544322, y=0.17500007562339306, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.382683542500468,
                                    w=0.9238794868917117,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(x=1.8, y=0.2, z=0.0),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0, y=0.0, z=0.0, w=1.0
                                ),
                            ),
                        ),
                    ],
                ),
                required_resources=[],
            ),
        ],
    ),
]
