import builtin_interfaces.msg
import geometry_msgs.msg
import mmrs_msgs.msg
import nav_msgs.msg
import std_msgs.msg
import unique_identifier_msgs.msg
from numpy import array, uint8

collision = [
    mmrs_msgs.msg.DividedPath(
        robot_namespace="/robot1",
        segments=[
            mmrs_msgs.msg.PathSegment(
                uuid=unique_identifier_msgs.msg.UUID(
                    uuid=array(
                        [
                            65,
                            57,
                            60,
                            52,
                            33,
                            47,
                            72,
                            105,
                            148,
                            161,
                            129,
                            141,
                            44,
                            215,
                            197,
                            186,
                        ],
                        dtype=uint8,
                    )
                ),
                path=nav_msgs.msg.Path(
                    header=std_msgs.msg.Header(
                        stamp=builtin_interfaces.msg.Time(sec=53, nanosec=964000000),
                        frame_id="map",
                    ),
                    poses=[
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4000001549720764, y=1.700000174343586, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.07098137699128,
                                    w=0.9974776409125278,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.432299960140881, y=1.6953798129466122, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.15794323077520575,
                                    w=0.9874481940093314,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.45605255448896287, y=1.6875818086800223, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.24282993205028294,
                                    w=0.9700688759569884,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4781045567706883, y=1.6758035492271688, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.34551621313532194,
                                    w=0.938412780422681,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.49713531877301875, y=1.6595918485461425, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.37934148208931556,
                                    w=0.9252567427294337,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5149400370559079, y=1.6420427149838588, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.408558756846285,
                                    w=0.9127320210249108,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5315942426263405, y=1.6233972374110976, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.43491477441490123,
                                    w=0.9004716203166181,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5471368453970058, y=1.6038156331154028, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.5377878137012708,
                                    w=0.8430802259775799,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5576760911595215, y=1.5811456498674374, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6619685519088125,
                                    w=0.7495316112638277,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.560765995990721, y=1.5563371473004963, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6908321816972995,
                                    w=0.7230151428091596,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.561903538732281, y=1.53136308686976, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6934776060617561,
                                    w=0.7204781814120784,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5628579760023626, y=1.506381397044379, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6948183510266447,
                                    w=0.7191852745131914,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.563719334657776, y=1.4813966554611397, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6956967144164969,
                                    w=0.7183356329391513,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5645196581560299, y=1.4564088621200426, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6963322015177918,
                                    w=0.7177196284966613,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5652757311653431, y=1.431420305839481, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6970768526840442,
                                    w=0.7169964166243143,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5659799242910708, y=1.4064302236799904, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6978863003690376,
                                    w=0.7162085672185284,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5666276598964259, y=1.3814386156415708, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6988476258007978,
                                    w=0.7152705753157947,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5672082568289056, y=1.3564454817242222, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6997967954846961,
                                    w=0.7143419664483884,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5677224780279744, y=1.3314508219279446, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7008861979701783,
                                    w=0.7132731156400808,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5681604052805937, y=1.306454636252738, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7017346961929193,
                                    w=0.7124383595505167,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5685388232549826, y=1.281457687638067, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7026256380096675,
                                    w=0.7115597043196781,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.568854680193283, y=1.2564599760839315, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7034613728563108,
                                    w=0.71073349217489,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5691117907928174, y=1.231460738650867, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7044042144558198,
                                    w=0.709799057943077,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5693025256589408, y=1.2064615012178024, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.705464824236193,
                                    w=0.708744934207926,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5694184924575438, y=1.1814622637847378, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7067830194055114,
                                    w=0.7074303947951548,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5694413806414786, y=1.1564622634122088, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7080771750563619,
                                    w=0.7061350537710207,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5693727160896742, y=1.1314622630396798, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7096163163782158,
                                    w=0.7045883078293395,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5691949511944472, y=1.1064630256066152, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7115166868288187,
                                    w=0.7026691998117897,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5688821460140048, y=1.0814645511130152, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7140000392018598,
                                    w=0.7001456591451115,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5683923388777998, y=1.0564691283772731, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7167411875077824,
                                    w=0.6973392790671796,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.56770645629922, y=1.0314790462177825, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7202133765924567,
                                    w=0.6937526159787019,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5667710925157508, y=1.0064965934529368, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7225190683903382,
                                    w=0.6913509932099309,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5656694079290219, y=0.9815210071432716, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.724838196034875,
                                    w=0.6889191458864444,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5643998766601044, y=0.9565530502282513, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7260475983180891,
                                    w=0.6876444466266959,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5630426073527701, y=0.931589670950018, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7270874279463388,
                                    w=0.6865448799039853,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.561609807038451, y=0.9066308693085716, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7267514645243514,
                                    w=0.686900508670441,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5602014207869956, y=0.8816705417881963, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.725722152398055,
                                    w=0.6879879050671852,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5588678026030607, y=0.8567063995704984, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7240376970282275,
                                    w=0.6897604028081494,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5576562547334447, y=0.8317353908976202, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7217373227106194,
                                    w=0.692167058596772,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5566110276670884, y=0.8067575157695615, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7198532140088659,
                                    w=0.694126321559056,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5557016038254119, y=0.7817743000652513, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7177833471155437,
                                    w=0.6962665198066088,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5549409531793117, y=0.7567857437846897, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7162617915487308,
                                    w=0.697831674522877,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5542894028766341, y=0.7317941357462701, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7146728302704665,
                                    w=0.6994588949132043,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.553751530554166, y=0.7067994759499925, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7134443738213222,
                                    w=0.7007118705022068,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5533013962701148, y=0.6818040532142504, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7120955107872284,
                                    w=0.702082604482319,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5529473923585897, y=0.6568063416601149, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7126310476153896,
                                    w=0.7015390152903773,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5525552414738399, y=0.6318093930454438, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7144700632825921,
                                    w=0.6996660122322428,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5520318650011973, y=0.6068147332491662, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7148117618869371,
                                    w=0.6993169131860696,
                                ),
                            ),
                        ),
                    ],
                ),
                required_resources=[
                    unique_identifier_msgs.msg.UUID(
                        uuid=array(
                            [
                                171,
                                163,
                                198,
                                47,
                                197,
                                68,
                                75,
                                67,
                                176,
                                8,
                                176,
                                115,
                                174,
                                133,
                                111,
                                222,
                            ],
                            dtype=uint8,
                        )
                    )
                ],
            ),
            mmrs_msgs.msg.PathSegment(
                uuid=unique_identifier_msgs.msg.UUID(
                    uuid=array(
                        [
                            78,
                            183,
                            237,
                            146,
                            44,
                            213,
                            77,
                            23,
                            140,
                            107,
                            156,
                            238,
                            83,
                            27,
                            80,
                            221,
                        ],
                        dtype=uint8,
                    )
                ),
                path=nav_msgs.msg.Path(
                    header=std_msgs.msg.Header(
                        stamp=builtin_interfaces.msg.Time(sec=53, nanosec=964000000),
                        frame_id="map",
                    ),
                    poses=[
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5520318650011973, y=0.6068147332491662, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7148117618869371,
                                    w=0.6993169131860696,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5514840744656908, y=0.5818208363923532, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7163043677084914,
                                    w=0.6977879712360613,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5508294724051552, y=0.5568292283539336, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7148971017896544,
                                    w=0.6992296717479547,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5502755783539328, y=0.5318353314971205, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7099387682980127,
                                    w=0.7042634061681046,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.550074925274771, y=0.5068360940640559, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.708335756906469,
                                    w=0.7058756657427282,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5499879501758187, y=0.4818368566309914, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7081202724610115,
                                    w=0.7060918351954956,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5499162338661563, y=0.4568368562584624, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7081741404646499,
                                    w=0.7060378083198905,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5498407028591714, y=0.43183685588593335, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7083895707340567,
                                    w=0.7058216602479829,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5497499130628967, y=0.4068368555134043, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7087018743203306,
                                    w=0.7055080816935058,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5496369980221516, y=0.3818376180803398, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7091430484488711,
                                    w=0.7050646330916351,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5494928024633623, y=0.35683761770781075, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7096915688569583,
                                    w=0.7045125102461625,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5493096969918838, y=0.3318383802747462, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7104006815561359,
                                    w=0.7037974649319062,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5490762375157487, y=0.3068391428416817, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7113345357889471,
                                    w=0.7028535965575784,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5487764023062027, y=0.28184143128754613, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7125347107081615,
                                    w=0.7016368619421562,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5483911178766334, y=0.2568444826728751, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7141175218995908,
                                    w=0.7000258316062254,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.547892918406319, y=0.231849059937133, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7162192124516101,
                                    w=0.697875375489919,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5472444198614994, y=0.2068574518987134, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7182616974000303,
                                    w=0.6957730478022466,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.546449436939497, y=0.18187042149708077, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7206896219563036,
                                    w=0.6932578660242379,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5454797408801255, y=0.1568887316716996, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.722128538525684,
                                    w=0.691758898639374,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5444062850535829, y=0.13191238242256986, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7234472141943135,
                                    w=0.6903796986256817,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5432374617939786, y=0.10693908493129811, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7239961547179643,
                                    w=0.6898040069132692,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5420289656822206, y=0.08196883919788434, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7243330031095911,
                                    w=0.6894502887128564,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5407960555075988, y=0.05699935640393505, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7236476057209817,
                                    w=0.6901696477926934,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5396127363981691, y=0.032026821852127796, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7223605013853597,
                                    w=0.6915166708317969,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5385224959034076, y=0.00705047260299807, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7213463168912015,
                                    w=0.6925745383043607,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5375054975972375, y=-0.017928165464525136, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7202979979093062,
                                    w=0.6936647563541376,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5365640302980523, y=-0.04291061822937081, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7204147342538852,
                                    w=0.6935435175026178,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5356141706647577, y=-0.06789230805475199, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7212398598753555,
                                    w=0.6926854008328582,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5346048017532326, y=-0.09287247200120419, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7219593306114758,
                                    w=0.6919354918943166,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5335435529581218, y=-0.1178495841897984, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7228461571240865,
                                    w=0.691008996418238,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5324182172479937, y=-0.14282440755999914, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7232998787787203,
                                    w=0.6905340580729444,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5312600751408922, y=-0.1677977050512709, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7236165450027604,
                                    w=0.6902022137013674,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.530079044849856, y=-0.19276947666361366, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7243540175429447,
                                    w=0.6894282103811791,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5288446087963052, y=-0.21773895945756294, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7254379337047633,
                                    w=0.6882875883975851,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5275315899779116, y=-0.24270462755418976, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7261424559065496,
                                    w=0.6875442776505414,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5261674542153969, y=-0.2676672438929586, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7267619290342822,
                                    w=0.6868894368865844,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5247583050244771, y=-0.29262757141333395, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7271397187240014,
                                    w=0.6864894969728089,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5233216900128355, y=-0.3175863730547803, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7273807991494535,
                                    w=0.6862340512017037,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5218675273935105, y=-0.3425444117567622, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7271298626255999,
                                    w=0.6864999365463017,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5204316753213334, y=-0.3675024504587441, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7264258015738613,
                                    w=0.687244901623703,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5190469401932774, y=-0.3924643038580484, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7248171987916463,
                                    w=0.6889412372153602,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5177789348032888, y=-0.4174322607730687, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7223394177678563,
                                    w=0.6915386941732143,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5166902201874564, y=-0.44240861002219845, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7252170988891111,
                                    w=0.6885202680232886,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5153932230978171, y=-0.46737504105828975, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7329918146136407,
                                    w=0.6802374583256954,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.513529361986059, y=-0.49230561393954986, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7244490948546005,
                                    w=0.6893283027443092,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5122880594773278, y=-0.5172743337940346, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6974926181821378,
                                    w=0.7165919672878188,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5129632609034047, y=-0.5422651788929898, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6837057078664895,
                                    w=0.7297578399926803,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5145906107811697, y=-0.5672125364424687, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6436297240803398,
                                    w=0.7653370357432506,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5188775676321598, y=-0.5918417482352538, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6000146481581005,
                                    w=0.7999890136718822,
                                ),
                            ),
                        ),
                    ],
                ),
                required_resources=[
                    unique_identifier_msgs.msg.UUID(
                        uuid=array(
                            [
                                225,
                                157,
                                52,
                                251,
                                115,
                                92,
                                79,
                                237,
                                130,
                                96,
                                68,
                                196,
                                235,
                                115,
                                127,
                                131,
                            ],
                            dtype=uint8,
                        )
                    )
                ],
            ),
            mmrs_msgs.msg.PathSegment(
                uuid=unique_identifier_msgs.msg.UUID(
                    uuid=array(
                        [
                            252,
                            136,
                            77,
                            246,
                            221,
                            12,
                            70,
                            205,
                            191,
                            15,
                            236,
                            253,
                            8,
                            68,
                            21,
                            155,
                        ],
                        dtype=uint8,
                    )
                ),
                path=nav_msgs.msg.Path(
                    header=std_msgs.msg.Header(
                        stamp=builtin_interfaces.msg.Time(sec=53, nanosec=964000000),
                        frame_id="map",
                    ),
                    poses=[
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5188775676321598, y=-0.5918417482352538, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6000146481581005,
                                    w=0.7999890136718822,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5258767742794248, y=-0.6158422979092961, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6333430185664618,
                                    w=0.7738711913704518,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5308206220093439, y=-0.6403486764482977, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.659079613516633,
                                    w=0.7520731766568767,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5341012617066667, y=-0.6651320020129106, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6782155854260674,
                                    w=0.7348629938193763,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5361024519220337, y=-0.6900518937416678, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6893178111151474,
                                    w=0.7244590777120692,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5373445173702294, y=-0.715021376535617, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6932574394841376,
                                    w=0.7206900322606782,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5383142134296008, y=-0.7400023034215337, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.696178936261265,
                                    w=0.7178682948188571,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.539080967591417, y=-0.7649908597020953, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6983780373621128,
                                    w=0.715729080679445,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5396943709208699, y=-0.7899832306799794, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6993823380425146,
                                    w=0.7147477493732918,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5402375838195894, y=-0.8149771275367925, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6994150484689683,
                                    w=0.7147157406795731,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5407785078999154, y=-0.8399710243936056, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6988258029602946,
                                    w=0.7152918964429134,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5413606307113241, y=-0.8649641583109542, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6976240632758197,
                                    w=0.7164640021232993,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5420266768638271, y=-0.8899557663493738, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6970331069159393,
                                    w=0.717038944453586,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5427339217474128, y=-0.9149458485088644, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6969124809898081,
                                    w=0.7171561851093738,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5434495589651078, y=-0.9399351677288905, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6971643373369596,
                                    w=0.7169113520830299,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5441476485751195, y=-0.9649252498883811, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6977114849067244,
                                    w=0.7163788689159206,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5448075912119066, y=-0.9899168579268007, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.6985966276431016,
                                    w=0.7155157243874419,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5454057357520696, y=-1.0149099918441493, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.699840385130272,
                                    w=0.7142992617528822,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5459169051932804, y=-1.039904651640427, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7015063969869293,
                                    w=0.7126631567482752,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5463113448964236, y=-1.064901600255098, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7036672774951712,
                                    w=0.7105296352598769,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5465539596461326, y=-1.089900074748698, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7059405497150374,
                                    w=0.7082710923566137,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5466363571082979, y=-1.114900075121227, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7085403321117492,
                                    w=0.7056703180458792,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5465348861595203, y=-1.139900075493756, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7109482378312975,
                                    w=0.7032443409829708,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.546262516770696, y=-1.164898549987356, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7134657544770449,
                                    w=0.7006901006782537,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5458108566077158, y=-1.1898939727230982, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7154304660551404,
                                    w=0.6986839401618765,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5452188155832687, y=-1.2148871066404467, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7171239912225554,
                                    w=0.6969456085040153,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5445054671839671, y=-1.2398771887999374, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7184314561414924,
                                    w=0.6955977593598291,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5436982772305328, y=-1.26486421920157, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7195352138856903,
                                    w=0.6944559568312982,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5428117415727911, y=-1.2898481978453447, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7198316597412284,
                                    w=0.6941486740131312,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5419038436100436, y=-1.3148321764891193, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7193869349971949,
                                    w=0.6946095577771312,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5410279891048049, y=-1.339816155132894, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7185269242887511,
                                    w=0.6954991438327925,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5402139326961901, y=-1.3648031855345266, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7171452510618072,
                                    w=0.6969237324696996,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5394990584179595, y=-1.3897932676940172, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7164004349986182,
                                    w=0.6976893411352869,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5388375899022435, y=-1.4147841127929723, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7160911851743907,
                                    w=0.6980067438897253,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5381982466309978, y=-1.4397764837708564, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7163152904062626,
                                    w=0.6977767585196512,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5375428816309977, y=-1.4647673288698115, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7170495759480298,
                                    w=0.6970221701156648,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5368348738079476, y=-1.4897574110293021, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.718293530826129,
                                    w=0.695740183957584,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5360376020675517, y=-1.5147444414309348, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7201706665450007,
                                    w=0.6937969523197183,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5351052900419404, y=-1.539727657135245, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.724649191940652,
                                    w=0.6891179497152574,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5338494916833838, y=-1.5646956140502652, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7349669691137188,
                                    w=0.6781029083493109,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5318406720733719, y=-1.589614742839558, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7339280998378643,
                                    w=0.6792271669098504,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5299081464098094, y=-1.6145399751445666, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7161979218019688,
                                    w=0.6978972251030526,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5292611737439188, y=-1.6395315831829862, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7608559768255323,
                                    w=0.6489207829379986,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.525316013773022, y=-1.6642187783750728, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.8349712190431134,
                                    w=0.5502936155995788,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5154573100128346, y=-1.6871924115299066, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.885945422432235,
                                    w=0.46378950879830044,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.501212467271273, y=-1.7077368454297925, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9162370649576851,
                                    w=0.40063654451101527,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.48423782712575303, y=-1.726090880127117, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9273665540902356,
                                    w=0.37415407836184567,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.46623703340048905, y=-1.7434401235497035, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9384951038227892,
                                    w=0.34529254278169996,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.44719864200351367, y=-1.7596426689571558, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.96601801960165,
                                    w=0.25847472952864503,
                                ),
                            ),
                        ),
                    ],
                ),
                required_resources=[],
            ),
            mmrs_msgs.msg.PathSegment(
                uuid=unique_identifier_msgs.msg.UUID(
                    uuid=array(
                        [
                            213,
                            97,
                            132,
                            189,
                            18,
                            168,
                            74,
                            35,
                            132,
                            221,
                            241,
                            237,
                            31,
                            148,
                            178,
                            181,
                        ],
                        dtype=uint8,
                    )
                ),
                path=nav_msgs.msg.Path(
                    header=std_msgs.msg.Header(
                        stamp=builtin_interfaces.msg.Time(sec=53, nanosec=964000000),
                        frame_id="map",
                    ),
                    poses=[
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.44719864200351367, y=-1.7596426689571558, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.96601801960165,
                                    w=0.25847472952864503,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.425538790606538, y=-1.7721274103541305, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9773711073629578,
                                    w=0.21153183801051187,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.40277649168336893, y=-1.7824644771585554, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9607154490619694,
                                    w=0.27753526971118203,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.381627809727604, y=-1.795796081361118, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9554907278904642,
                                    w=0.29502113299787686,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.36097961606054696, y=-1.8098906250281743, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9684971689613554,
                                    w=0.24902456447876764,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.33908020167172026, y=-1.8219496462039615, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9815203133251139,
                                    w=0.19135797482773026,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.31591125601397607, y=-1.8313406680724142, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9778652680190928,
                                    w=0.20923555530059362,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.2931001289650794, y=-1.84157092335181, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9659808257541965,
                                    w=0.2586136969985168,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.2714440922654262, y=-1.8540617682645006, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.967551373023416,
                                    w=0.25267437654123637,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.24963623061233875, y=-1.8662855843646184, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9867787915272396,
                                    w=0.16207287432522652,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.22595001199766784, y=-1.8742819528919767, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.996934371859385,
                                    w=0.07824230444799918,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.20125595035040078, y=-1.8781820994344685, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9977136945460701,
                                    w=0.06758242164373268,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.17648406887775536, y=-1.881553528928066, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9977478422394652,
                                    w=0.06707639902746111,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.15170913564725197, y=-1.8848997814193353, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9935419229082546,
                                    w=0.11346562221116997,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.1273530561827556, y=-1.8905363781830147, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9806620785743618,
                                    w=0.19570868055917234,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.10426803386610572, y=-1.9001326307674162, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9657876956825654,
                                    w=0.25933400638589715,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.08263107065306485, y=-1.9126555191376156, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9350504829564396,
                                    w=0.35451458971801036,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.0639146397101058, y=-1.9292303790037408, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.882209211663908,
                                    w=0.47085762907204337,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.05000014975667, y=-1.9499998800456524, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9238795325112867,
                                    w=0.38268343236508984,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(
                                    sec=53, nanosec=964000000
                                ),
                                frame_id="map",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.02500014938414097, y=-1.9749998804181814, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9238795325112867,
                                    w=0.38268343236508984,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.4901161193847656e-07,
                                    y=-1.9999998807907104,
                                    z=0.0,
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9436283191604178,
                                    w=0.3310069414355004,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(x=0.0, y=-2.0, z=0.0),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0, y=0.0, z=0.0, w=1.0
                                ),
                            ),
                        ),
                    ],
                ),
                required_resources=[],
            ),
        ],
    ),
    mmrs_msgs.msg.DividedPath(
        robot_namespace="/robot2",
        segments=[
            mmrs_msgs.msg.PathSegment(
                uuid=unique_identifier_msgs.msg.UUID(
                    uuid=array(
                        [
                            254,
                            149,
                            179,
                            246,
                            130,
                            248,
                            65,
                            246,
                            156,
                            146,
                            20,
                            246,
                            25,
                            182,
                            109,
                            109,
                        ],
                        dtype=uint8,
                    )
                ),
                path=nav_msgs.msg.Path(
                    header=std_msgs.msg.Header(
                        stamp=builtin_interfaces.msg.Time(sec=61, nanosec=685000000),
                        frame_id="map",
                    ),
                    poses=[
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.750000175088644, y=-0.04999985173344612, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.691940686016189,
                                    w=0.7219543524596591,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7519174419629167, y=-0.004860538316677321, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6974270334707803,
                                    w=0.7166557981235812,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7525972210257805, y=0.020130306782277785, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6973502117028465,
                                    w=0.716730550652053,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7532823406648959, y=0.0451203889417684, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6973286466772627,
                                    w=0.7167515319294806,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7539689861829402, y=0.0701112340407235, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6982142521909036,
                                    w=0.7158888587186543,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7545938336043605, y=0.0951036050186076, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6955316609427568,
                                    w=0.7184954478813419,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7554056011945818, y=0.12008987248077574, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6996442129377578,
                                    w=0.7144914102371738,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7559305035461534, y=0.1450845322770533, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6171142817581223,
                                    w=0.7868735370122424,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.761889060763849, y=0.1693643177951003, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6375691575145107,
                                    w=0.7703931265180376,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7665643538022664, y=0.19392333915715199, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6939063493359715,
                                    w=0.7200652597863785,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7674890364332327, y=0.21890579192199766, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.5318821342387595,
                                    w=0.8468183956891951,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7783441391340489, y=0.24142623903492222, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7635191584320133,
                                    w=0.6457851769027143,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7741960372655967, y=0.2660798648905711, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.8614038097121537,
                                    w=0.5079207385147685,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7620950544192624, y=0.287956391095463, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7880664884250456,
                                    w=0.615590131354798,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.756042655647434, y=0.3122125254901107, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.823191075576864,
                                    w=0.5677644345066057,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7471605144017985, y=0.3355813612875522, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.8327560474180756,
                                    w=0.5536401046606213,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7374864419920186, y=0.3586335772072289, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.8749919542788082,
                                    w=0.4841374597646334,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.724205954733577, y=0.37981430262050253, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9167208434561519,
                                    w=0.3995283408884049,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7071870640991165, y=0.3981271385867444, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9251379758091439,
                                    w=0.3796310389256388,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.6893930269687303, y=0.41568771624099554, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9369045642664208,
                                    w=0.3495852363254892,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.6705034087673312, y=0.43206421184635246, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9468160154828626,
                                    w=0.32177543850510965,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.6506807156008563, y=0.44729706119443335, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9809075498367432,
                                    w=0.19447462218314557,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.6275712792213426, y=0.4568353303795334, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9992102375695833,
                                    w=0.03973538896420767,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.602650624553121, y=0.45882049886614595, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9997648029758502,
                                    w=0.02168729422171784,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.5776735123645267, y=0.4599046358451915, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9998809711351281,
                                    w=0.01542866040436347,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.5526857190234296, y=0.4606759676437946, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999232278139776,
                                    w=0.01239106444484675,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.5276933480455455, y=0.46129547448896346, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999596097264767,
                                    w=0.008987709144855024,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.502697162370339, y=0.46174484583355024, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999823823982733,
                                    w=0.0059358986744704806,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.4776994508162034, y=0.4620416292852383, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999975860439365,
                                    w=0.0021972497126791987,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.4526994504436743, y=0.4621514925681254, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999975185248677,
                                    w=0.0022277666185454574,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.4276994500711453, y=0.4620401034063093, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999706604446512,
                                    w=0.007660172967243016,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.4027025014564742, y=0.46165710779513347, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9998990716810058,
                                    w=0.014207267557936687,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.3777124192969836, y=0.46094681115368985, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9997506679421314,
                                    w=0.022329396527047178,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.3527375959267829, y=0.4598306307171356, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9994976401258714,
                                    w=0.03169333341278731,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.3277879495589104, y=0.4582467683888467, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9990588451311029,
                                    w=0.04337538432460319,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.3028817907405141, y=0.45608002030968464, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.998302939076329,
                                    w=0.05823436984773944,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.2780511629291027, y=0.4531732209499637, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9970207123164901,
                                    w=0.07713429335852223,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.2533487089477262, y=0.44932800604891554, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9997073129206727,
                                    w=0.024192736367103736,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.2283784632143124, y=0.45053726510013803, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9996660411680092,
                                    w=0.025841945272758435,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.2034120321782211, y=0.45182892161352584, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9996394821256581,
                                    w=0.026849688555849436,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.1784478899605233, y=0.4531709321315702, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9996072018407104,
                                    w=0.028025738673322042,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.1534867995006834, y=0.45457168898838063, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9995797434150586,
                                    w=0.028988559023959214,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.1285287607987016, y=0.45602051103145413, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9995449198184111,
                                    w=0.03016543162638551,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.1035745367940422, y=0.45752807941329365, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9994621636713532,
                                    w=0.03279303873350582,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.0786279421840277, y=0.4591668733830261, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9993466788426248,
                                    w=0.036141603260171455,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.0536935546054451, y=0.46097275109548264, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.999219373210923,
                                    w=0.039504989557904505,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.0287713740582944, y=0.4629464754901278, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9990535052964165,
                                    w=0.04349820174378577,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.0038659781793626, y=0.4651193270850058, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9989752765661098,
                                    w=0.04525921795241689,
                                ),
                            ),
                        ),
                    ],
                ),
                required_resources=[],
            ),
            mmrs_msgs.msg.PathSegment(
                uuid=unique_identifier_msgs.msg.UUID(
                    uuid=array(
                        [
                            196,
                            23,
                            20,
                            171,
                            103,
                            232,
                            69,
                            205,
                            150,
                            228,
                            119,
                            97,
                            171,
                            26,
                            235,
                            59,
                        ],
                        dtype=uint8,
                    )
                ),
                path=nav_msgs.msg.Path(
                    header=std_msgs.msg.Header(
                        stamp=builtin_interfaces.msg.Time(sec=61, nanosec=685000000),
                        frame_id="map",
                    ),
                    poses=[
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.0038659781793626, y=0.4651193270850058, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9989752765661098,
                                    w=0.04525921795241689,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.9789689746345402, y=0.4673799167183006, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9988890028044416,
                                    w=0.04712494112832701,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.9540796004843628, y=0.4697335849662636, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9988020809056308,
                                    w=0.04893263919493465,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.9291993816077593, y=0.4721772800710369, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.998680573474033,
                                    w=0.05135282042474778,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.9043313697625877, y=0.47474151961120015, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9986782341009574,
                                    w=0.051398295039751817,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.879463357917416, y=0.4773080479697569, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9986711228797525,
                                    w=0.051536281647926695,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.8545961090117089, y=0.4798814427834941, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.998745940318575,
                                    w=0.05006542416843627,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.8297212307113568, y=0.4823815954086399, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9988192558562637,
                                    w=0.048580800021608396,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.8048394859558243, y=0.4848077429057298, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9989586408718248,
                                    w=0.04562492550696898,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.7799432453504664, y=0.48708664308617244, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9991034289288936,
                                    w=0.042336016611477734,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.7550332718347477, y=0.48920151128174894, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9993204496258895,
                                    w=0.03685972001399515,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.7301011730745586, y=0.4910432471490367, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9995435404163353,
                                    w=0.03021110411716147,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.7051469490698992, y=0.4925531043492697, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9996072018407104,
                                    w=0.028025738673322042,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.6801858586100593, y=0.4939538612060801, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9996127328500293,
                                    w=0.027827761751458315,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.6552247681502195, y=0.4953446998498521, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9996488518119531,
                                    w=0.026498548470507578,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.6302598629930571, y=0.4966691627602131, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9995328676930239,
                                    w=0.030562172719884404,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.6053064019278622, y=0.49819656756812947, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9996983785112428,
                                    w=0.024559153120416635,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5803369191339129, y=0.4994241371664998, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999583662431089,
                                    w=0.009125008515757742,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5553407334587064, y=0.499880374966267, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.999984489998513,
                                    w=0.005569538797237575,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5303422589651063, y=0.5001588478708072, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999873984242627,
                                    w=0.005020258227926889,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5053437844715063, y=0.5004098549546256, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999829226722122,
                                    w=0.005844173503611646,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.48034530997790625, y=0.5007020607695267, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.999974501157615,
                                    w=0.007141220804532213,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4553475984237707, y=0.5010591164389098, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999585028057116,
                                    w=0.009110031095429865,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.43035217568802864, y=0.5015145912992125, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999334522534722,
                                    w=0.01153651006383939,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.40535904177068005, y=0.5020913735343697, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999098321528049,
                                    w=0.0134286099112885,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.38036743373226045, y=0.5027627602631242, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9998826126417468,
                                    w=0.015321910348076463,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.3553796403911633, y=0.5035287514854758, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9998634102945142,
                                    w=0.016527575570058923,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.3303933729289952, y=0.5043550149255225, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9998446421461458,
                                    w=0.017626445235661824,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.305408631345756, y=0.5052362100070127, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9998294872686296,
                                    w=0.0184660875160152,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.28042541564144585, y=0.50615936675905, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9998124365825465,
                                    w=0.019367283105059314,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.25544448875552916, y=0.5071275369394925, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.999806783991154,
                                    w=0.019656924613632852,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.23046356186961248, y=0.5081102029697604, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9998025533525667,
                                    w=0.019870941338745274,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.20548339792316028, y=0.5091035501525312, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9998052706828243,
                                    w=0.01973374558577982,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.1805032339767081, y=0.5100900308801215, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9998085780571527,
                                    w=0.01956546046824222,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.1555223070907914, y=0.5110681192736024, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9998317324496027,
                                    w=0.018344121315185747,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.13053909138648123, y=0.5119851725099238, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9998631581247284,
                                    w=0.016542823968246998,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.10555282392431309, y=0.512812198889435, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9998881679485462,
                                    w=0.014954985673674565,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.08056350470428697, y=0.5135598795646388, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999144764707723,
                                    w=0.013078216399094709,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.055572659605331864, y=0.5142137186857099, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999318589492403,
                                    w=0.011673793655731464,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.030579525687983278, y=0.5147973673760475, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999489773167854,
                                    w=0.010101621806172564,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.00558410295224121, y=0.5153024333015424, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.999960563855128,
                                    w=0.008880919689678789,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.01941208272296535, y=0.5157464640698777, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.999971931958167,
                                    w=0.007492349154369925,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.04440903133763641, y=0.5161210673469441, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999816503894986,
                                    w=0.006057960407145107,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.06940674289177196, y=0.5164239543143481, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999908076334639,
                                    w=0.004287732334532608,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.09440598032483649, y=0.5166383403038708, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999953899537318,
                                    w=0.003036457028170087,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.11940598069736552, y=0.516790165257305, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999986671707508,
                                    w=0.0016326839013918648,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.14440598106989455, y=0.5168717997800059, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999999436515625,
                                    w=0.0003357035474869146,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.1694052185029591, y=0.5168885844482247, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999992172267461,
                                    w=0.0012512177648580218,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.19440521887548812, y=0.5168260234121362, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.999994963547032,
                                    w=0.0031737801704111593,
                                ),
                            ),
                        ),
                    ],
                ),
                required_resources=[
                    unique_identifier_msgs.msg.UUID(
                        uuid=array(
                            [
                                171,
                                163,
                                198,
                                47,
                                197,
                                68,
                                75,
                                67,
                                176,
                                8,
                                176,
                                115,
                                174,
                                133,
                                111,
                                222,
                            ],
                            dtype=uint8,
                        )
                    ),
                    unique_identifier_msgs.msg.UUID(
                        uuid=array(
                            [
                                225,
                                157,
                                52,
                                251,
                                115,
                                92,
                                79,
                                237,
                                130,
                                96,
                                68,
                                196,
                                235,
                                115,
                                127,
                                131,
                            ],
                            dtype=uint8,
                        )
                    ),
                ],
            ),
            mmrs_msgs.msg.PathSegment(
                uuid=unique_identifier_msgs.msg.UUID(
                    uuid=array(
                        [
                            224,
                            3,
                            20,
                            226,
                            223,
                            215,
                            67,
                            66,
                            141,
                            134,
                            142,
                            128,
                            136,
                            170,
                            6,
                            237,
                        ],
                        dtype=uint8,
                    )
                ),
                path=nav_msgs.msg.Path(
                    header=std_msgs.msg.Header(
                        stamp=builtin_interfaces.msg.Time(sec=61, nanosec=685000000),
                        frame_id="map",
                    ),
                    poses=[
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.19440521887548812, y=0.5168260234121362, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.999994963547032,
                                    w=0.0031737801704111593,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.21940521924801715, y=0.5166673320035216, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999835411482355,
                                    w=0.005737371578974484,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.2444036937416172, y=0.5163804667648719, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999633578556401,
                                    w=0.008560545898083157,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.26939987941682375, y=0.515952457725291, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999262222954711,
                                    w=0.012147014691197935,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.29439225039470784, y=0.515345157911554, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9998664263837688,
                                    w=0.016344093445388154,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.3193792807963405, y=0.5145280497450813, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9997637957166082,
                                    w=0.02173367834306921,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.3443556300454702, y=0.5134416239476423, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.999583708357197,
                                    w=0.028851516197143268,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.3693136687474521, y=0.5119996683597492, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.999270518304307,
                                    w=0.03818941277162547,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.3942411898708542, y=0.5100915567590505, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9981295451905654,
                                    w=0.061134368547283977,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.4190542700745823, y=0.5070405618405402, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9946011564090017,
                                    w=0.10377157447912426,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.4435156351851788, y=0.501880039302705, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9647084569038135,
                                    w=0.26332032427874424,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.4650488386310485, y=0.4891786230978141, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9677667180949806,
                                    w=0.25184832607676866,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.4868772996496773, y=0.5013650551641717, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9530359197514295,
                                    w=0.3028572859674118,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.5072912709011348, y=0.5157968180745343, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9656619552677707,
                                    w=0.2598018247596158,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.5289167900222083, y=0.5283410687497394, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9738073633117795,
                                    w=0.22737462294583333,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.5513319514890327, y=0.5394120833190073, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.986566803207464,
                                    w=0.16335832641467923,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.5749975707381623, y=0.5474702499429895, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9968953940848865,
                                    w=0.07873736884312836,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.5996870547486424, y=0.551394810548345, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9979387485644271,
                                    w=0.06417362475086674,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.6244810614657581, y=0.554596867480825, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9990205940593488,
                                    w=0.0442476286969788,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.6493834055868319, y=0.5568071031094632, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9992827129252045,
                                    w=0.03786898003436652,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.6743116896496986, y=0.5586991929814076, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9995793017139463,
                                    w=0.02900378570220677,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.6992697283516804, y=0.5601487779639456, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9997667681931433,
                                    w=0.02159650936234344,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.7242460776008102, y=0.5612283373062041, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9999384619061832,
                                    w=0.011093800101711036,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.7492399744576232, y=0.561782994296891, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999976194542528,
                                    w=0.0021819912528238793,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.7742399748301523, y=0.5616738939534685, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9997651191319791,
                                    w=0.02167271480501909,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.799216324079282, y=0.5605905199138874, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9997587999275497,
                                    w=0.021962285113932906,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.8241926733284117, y=0.559492650024481, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9997899113174371,
                                    w=0.02049715170142271,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.8491713113959349, y=0.558468022323666, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9998028562049185,
                                    w=0.019855697532123705,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.8741514753423871, y=0.5574754380803597, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9998275105639564,
                                    w=0.018572805912994577,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.8991346910466973, y=0.5565469407520709, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9998440945928563,
                                    w=0.01765747739036865,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.924118669690472, y=0.5556642197916517, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9998664263837688,
                                    w=0.016344093445388154,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.9491057000921046, y=0.554847111625179, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9998860988004545,
                                    w=0.01509269444491668,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.9740942563726662, y=0.5540925644947947, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999092110912282,
                                    w=0.013474775505277871,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-0.9990851014716213, y=0.5534188889476468, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999307862867778,
                                    w=0.011765314951426334,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.02407823538897, y=0.5528306626205222, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999549554381879,
                                    w=0.009491422159598842,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.049073658124712, y=0.5523561142736071, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999664274327482,
                                    w=0.008194144701331889,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.074070606739383, y=0.551946415781174, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999785734094461,
                                    w=0.006546199050526368,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.0990683182935186, y=0.5516191147509062, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999839759348587,
                                    w=0.0056610841286735235,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.1240667927871186, y=0.551336064209579, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999897994822563,
                                    w=0.0045167390268790394,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.1490652672807187, y=0.5511102341280889, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999924299468393,
                                    w=0.003891021590238868,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.1740645047137832, y=0.550915684564643, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999952027675796,
                                    w=0.003097489600841943,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.1990645050863122, y=0.5507608078533508, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.999996352674934,
                                    w=0.0027008585355393143,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.2240637425193768, y=0.5506257675681354, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999975860439365,
                                    w=0.0021972497126791987,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.2490637428919058, y=0.5505159042852483, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.999998122356665,
                                    w=0.0019378552950658902,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.2740637432644348, y=0.5504190109732576, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999986670893983,
                                    w=0.001632733728072625,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.2990629806974994, y=0.5503373764505568, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999988590205909,
                                    w=0.0015106149464261672,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.3240629810700284, y=0.5502618454435719, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999990570407836,
                                    w=0.0013732871307514936,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.3490629814425574, y=0.5501931808917675, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999990146660559,
                                    w=0.001403804444087945,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.3740629818150865, y=0.5501229904610341, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999989493577918,
                                    w=0.0014495803919504,
                                ),
                            ),
                        ),
                    ],
                ),
                required_resources=[],
            ),
            mmrs_msgs.msg.PathSegment(
                uuid=unique_identifier_msgs.msg.UUID(
                    uuid=array(
                        [
                            233,
                            123,
                            76,
                            33,
                            78,
                            212,
                            66,
                            26,
                            143,
                            209,
                            236,
                            154,
                            135,
                            124,
                            62,
                            129,
                        ],
                        dtype=uint8,
                    )
                ),
                path=nav_msgs.msg.Path(
                    header=std_msgs.msg.Header(
                        stamp=builtin_interfaces.msg.Time(sec=61, nanosec=685000000),
                        frame_id="map",
                    ),
                    poses=[
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.3740629818150865, y=0.5501229904610341, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999989493577918,
                                    w=0.0014495803919504,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.3990629821876155, y=0.5500505112119072, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999986421418271,
                                    w=0.0016479425057030423,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.4240629825601445, y=0.5499681137497419, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999981518091378,
                                    w=0.0019225967618319588,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.4490629829326735, y=0.5498719833772157, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.999996269785529,
                                    w=0.002731376031866523,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.474062220365738, y=0.5497354172130713, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999921906025747,
                                    w=0.0039520543852412824,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.4990614577988026, y=0.5495378158917674, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9999726136448049,
                                    w=0.007400808089510121,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.5240584064134737, y=0.549167790251488, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9998666755217246,
                                    w=0.016328845064311082,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.5490454368151063, y=0.5483514450244797, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9994429226112762,
                                    w=0.0333743081161315,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.5739897426067273, y=0.5466836593550966, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9968184754685523,
                                    w=0.0797052505456896,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.598672360162027, y=0.5427110335634779, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9889051337658962,
                                    w=0.1485484311982292,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.6225683871294336, y=0.5353662153387972, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9750023257691076,
                                    w=0.22219465507709885,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.6451002783343256, y=0.5245340008219159, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.949983805693829,
                                    w=0.3122991657361085,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.665223569649811, y=0.5097001688137652, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9253180930442206,
                                    w=0.3791918072493222,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.683034391448416, y=0.492156375827733, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9246785453855527,
                                    w=0.3807487198975965,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.7007857039687906, y=0.4745530735634702, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9170441029021701,
                                    w=0.3987857988097798,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.7178343492423664, y=0.4562677034179501, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9012960100378052,
                                    w=0.4332037653228934,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.7334509571410877, y=0.4367456084004857, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.903316745442088,
                                    w=0.42897419200216935,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.7492499075718229, y=0.4173707606996686, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.8914694895498816,
                                    w=0.453080731439413,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.7639860833285184, y=0.3971749901350563, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.850406761424694,
                                    w=0.5261257835567116,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.7751455988756675, y=0.3748040791571725, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.829174407869342,
                                    w=0.5589899832148391,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.7845221248942948, y=0.35162902998371237, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.8671235527083404,
                                    w=0.49809310810175444,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.797117492513621, y=0.33003326550175416, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.8728906152865471,
                                    w=0.48791594946739875,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.8102141113611196, y=0.30873886210827095, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.8402153213323349,
                                    w=0.5422529057537647,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.8205122682528554, y=0.28595825263795405, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7913608181705718,
                                    w=0.6113493726703277,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.8268248293820761, y=0.2617684939767173, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7723603762569885,
                                    w=0.6351845788337144,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.8316519473739277, y=0.2372384643143164, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.7890928652790155,
                                    w=0.6142739209552637,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.8377852177289924, y=0.21300292928521003, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.8354978483653613,
                                    w=0.5494937173224563,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.8476881719781204, y=0.19004760667752407, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.8455017460431447,
                                    w=0.5339726560770633,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.8584318855171205, y=0.16747375380208496, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.8484477887809854,
                                    w=0.5292790849000709,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.86942508026101, y=0.14502044536203584, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.8712718702295648,
                                    w=0.49080070104542073,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.8823805553075772, y=0.12363982980906485, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9084596405114759,
                                    w=0.41797258470115,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.898645661751118, y=0.10465408123513953, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9153304526857102,
                                    w=0.40270356639365984,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.9155371414950082, y=0.08622375259136561, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9173753399834393,
                                    w=0.3980232224258643,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.9326163043471638, y=0.06796661120603176, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9206934331593208,
                                    w=0.390286564126032,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.9499998800456524, y=0.05000014975667, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9238795325112867,
                                    w=0.38268343236508984,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(
                                    sec=61, nanosec=685000000
                                ),
                                frame_id="map",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.9749998804181814, y=0.02500014938414097, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9238795325112867,
                                    w=0.38268343236508984,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=-1.9999998807907104,
                                    y=1.4901161193847656e-07,
                                    z=0.0,
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9013032363068559,
                                    w=0.4331887304891343,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(x=-2.0, y=0.0, z=0.0),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0, y=0.0, z=0.0, w=1.0
                                ),
                            ),
                        ),
                    ],
                ),
                required_resources=[],
            ),
        ],
    ),
]
