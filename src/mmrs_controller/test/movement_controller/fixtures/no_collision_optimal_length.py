import builtin_interfaces.msg
import geometry_msgs.msg
import mmrs_msgs.msg
import nav_msgs.msg
import std_msgs.msg
import unique_identifier_msgs.msg
from numpy import array, uint8

no_collision = [
    mmrs_msgs.msg.DividedPath(
        robot_namespace="/robot1",
        segments=[
            mmrs_msgs.msg.PathSegment(
                uuid=unique_identifier_msgs.msg.UUID(
                    uuid=array(
                        [
                            192,
                            125,
                            231,
                            68,
                            93,
                            239,
                            65,
                            214,
                            150,
                            112,
                            169,
                            68,
                            202,
                            194,
                            105,
                            20,
                        ],
                        dtype=uint8,
                    )
                ),
                path=nav_msgs.msg.Path(
                    header=std_msgs.msg.Header(
                        stamp=builtin_interfaces.msg.Time(sec=17, nanosec=756000000),
                        frame_id="map",
                    ),
                    poses=[
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.4901161193847656e-07,
                                    y=0.5000001564621925,
                                    z=0.0,
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.07134087026784246,
                                    w=0.9974519939472911,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.037249905426051555, y=0.5053559915029382, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.0019073382245694617,
                                    w=0.9999981810287942,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.062249905798580585, y=0.5054513589359999, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.0007781975352820726,
                                    w=0.9999996972042522,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.08724990617110961, y=0.5054124490233107, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.008499521412958257,
                                    w=0.999963878415491,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.11224609184631618, y=0.5049874917415877, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.013856487132215415,
                                    w=0.9999039942736276,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.1372361740058068, y=0.5042947427078275, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.024848627910167634,
                                    w=0.9996912251745446,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.16220565679975607, y=0.5030526772596318, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.03416962743862668,
                                    w=0.9994160477802553,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.1871469108335191, y=0.5013452187380949, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.04853381493543806,
                                    w=0.998821540019944,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.2120294185285161, y=0.49892136005939847, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.06455701752261687,
                                    w=0.9979140200881961,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.23682113642723834, y=0.49570022964030613, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.17633325402665917,
                                    w=0.9843305255473741,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.26026626617112925, y=0.5043786660489218, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.19475550020484572,
                                    w=0.9808518211941906,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.283369599034927, y=0.5139299052049182, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.21121216872224982,
                                    w=0.9774402384717154,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.30613952735274097, y=0.5242524761595178, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.23522186898272246,
                                    w=0.971941702136643,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.3283731092270159, y=0.5356835981560266, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.2607795781334066,
                                    w=0.9653983693939836,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.3499726884062966, y=0.5482713363807079, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.28782719845094223,
                                    w=0.9576823606143542,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.3708306904260894, y=0.5620538378067863, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.31942515359422224,
                                    w=0.9476115086106265,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.39072891459954917, y=0.5771882679639475, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.2578475816803519,
                                    w=0.9661856056791543,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4124047877252792, y=0.5896447806007359, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.34665284874223307,
                                    w=0.9379934980898825,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.431395876875456, y=0.6059022576496318, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.4007353236241402,
                                    w=0.9161938661658108,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4483667023236535, y=0.6242601070442788, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.4769953843776453,
                                    w=0.8789057988672179,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4619905123411172, y=0.6452218688312428, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.49732145874594724,
                                    w=0.8675663471290268,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4746240269336681, y=0.6667947451292662, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.522872223063521,
                                    w=0.8524110735722593,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4859544409208638, y=0.6890802068871267, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.5422072948793887,
                                    w=0.840244755639436,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.49625488663099304, y=0.7118592904785146, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6062755207262377,
                                    w=0.7952546717675598,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5028764382433337, y=0.735966651677586, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.612148661498792,
                                    w=0.790742699128634,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5091401712468269, y=0.7601693803097191, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6206637587224534,
                                    w=0.7840768448363439,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5148790018987484, y=0.7845018086508162, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6277870059620517,
                                    w=0.7783851714576806,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.52017303884287, y=0.8089341820617619, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6354364772259599,
                                    w=0.7721531476401959,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5249841351059672, y=0.8334672634820208, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6429306202893009,
                                    w=0.7659244202233108,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5293161053853623, y=0.858088845880161, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6492719724721258,
                                    w=0.760556313340541,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5332383771723244, y=0.8827790928301056, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6539456102778752,
                                    w=0.7565415644882291,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5368562361129534, y=0.9075166420268488, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6594268678779124,
                                    w=0.751768718370701,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5401139876263414, y=0.9323030193493196, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6638673203123129,
                                    w=0.7478503734179378,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5430780074458994, y=0.9571267807055506, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6681171720847576,
                                    w=0.7440560760893404,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5457589767241302, y=0.9819825855192903, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6713407294632574,
                                    w=0.741148854794866,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5482240341339093, y=1.0068605155775003, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6746738344936205,
                                    w=0.7381159915959515,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5504647873411272, y=1.0317598079407162, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.676458346568185,
                                    w=0.7364808927312624,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5525849961129552, y=1.056669781456435, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.679441007217413,
                                    w=0.7337301395686205,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5545030259266923, y=1.0815965396403726, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6810776971184039,
                                    w=0.7322111515730224,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5563096665786134, y=1.1065309272189552, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6838168165181687,
                                    w=0.7296537270835785,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5579293870617335, y=1.1314782847684342, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7249961882181928,
                                    w=0.6887528780840707,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5566484117008486, y=1.15644547874399, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7251436513732287,
                                    w=0.6885976218903905,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5553567551874607, y=1.1814119097800813, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7190043959162485,
                                    w=0.6950055241889164,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5545083665029438, y=1.2063981772422494, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7188775557429075,
                                    w=0.6951367202566003,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5536691330920007, y=1.231383681764953, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7150357544485114,
                                    w=0.6990878842178915,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5531053208277399, y=1.2563775786217661, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7148971017896544,
                                    w=0.6992296717479547,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5525514267765175, y=1.2813714754785792, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7122991605304354,
                                    w=0.7018759904054541,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.552182927015167, y=1.3063684240932503, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7122026196630519,
                                    w=0.7019739514719092,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.551821293708997, y=1.3313661356473858, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7106369942134609,
                                    w=0.7035588550045102,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5515710495646431, y=1.3563646101409859, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7105402674036347,
                                    w=0.7036565415015846,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5513276718754696, y=1.3813638475740504, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.708152625656435,
                                    w=0.7060593875701229,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5512536667474137, y=1.406363085007115, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7082710923566138,
                                    w=0.7059405497150374,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5511712692852484, y=1.431363085379644, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7113665778075589,
                                    w=0.7028211664273938,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5508691452573089, y=1.456361559873244, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.710980427169117,
                                    w=0.7032117975278998,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5505944870500912, y=1.481360034366844, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7162511470497746,
                                    w=0.6978425999821752,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5499436996868781, y=1.5063516424052636, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7196728680383007,
                                    w=0.694313303206504,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.549047245816098, y=1.5313356210490383, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7317000466583653,
                                    w=0.6816267612998672,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5472779891979371, y=1.5562730603854789, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7666820139662491,
                                    w=0.6420270161454704,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5428880355192405, y=1.580883961631116, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7926803599791333,
                                    w=0.6096374717021187,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5364709516833841, y=1.605046254471631, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.8467276553605823,
                                    w=0.5320265760726348,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5256234783772129, y=1.627570516281878, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9054816664438704,
                                    w=0.4243853811502364,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5096284525041028, y=1.646784383755687, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9228147953329933,
                                    w=0.38524388835454076,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.49204956430270386, y=1.6645593473994609, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9346243987351155,
                                    w=0.35563637790448793,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4733728062118985, y=1.6811792206939913, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9446109193065939,
                                    w=0.32819233861678054,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.45375839551923036, y=1.6966798617941095, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9796606271964776,
                                    w=0.20066154469903813,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.43077179239350016, y=1.7065088109151816, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.999926961619832,
                                    w=0.012086001229980643,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.40577942141561607, y=1.7071130589710606, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.9943174018375884,
                                    w=0.1064561149157142,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.38134628506520585, y=1.701820547905868, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.970407719194953,
                                    w=0.24147227279099642,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.3592614763865072, y=1.6901040865496384, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9502256163831325,
                                    w=0.3115626389175952,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.33911529688708697, y=1.7049066380397448, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9630997872875003,
                                    w=0.26914457030891714,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.31773697015250946, y=1.7178674536625635, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9713981656896583,
                                    w=0.23745653011607648,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.2955560311012846, y=1.7294008095473146, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9763843274409284,
                                    w=0.21604084134192303,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.27288986255064174, y=1.7399476847044753, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9786712184823829,
                                    w=0.20543282628199416,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.25000015273690224, y=1.750000175088644, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9238801597772125,
                                    w=0.38268191800767426,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(
                                    sec=17, nanosec=756000000
                                ),
                                frame_id="map",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.22500007636845112, y=1.775000087544322, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9238801597772116,
                                    w=0.3826819180076763,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(x=0.2, y=1.8, z=0.0),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0, y=0.0, z=0.0, w=1.0
                                ),
                            ),
                        ),
                    ],
                ),
                required_resources=[],
            )
        ],
    ),
    mmrs_msgs.msg.DividedPath(
        robot_namespace="/robot2",
        segments=[
            mmrs_msgs.msg.PathSegment(
                uuid=unique_identifier_msgs.msg.UUID(
                    uuid=array(
                        [
                            230,
                            161,
                            90,
                            30,
                            184,
                            159,
                            75,
                            182,
                            164,
                            188,
                            44,
                            193,
                            231,
                            26,
                            42,
                            80,
                        ],
                        dtype=uint8,
                    )
                ),
                path=nav_msgs.msg.Path(
                    header=std_msgs.msg.Header(
                        stamp=builtin_interfaces.msg.Time(sec=25, nanosec=170000000),
                        frame_id="map",
                    ),
                    poses=[
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.4901161193847656e-07,
                                    y=-0.49999985843896866,
                                    z=0.0,
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.17691604046070852,
                                    w=0.9842259469388647,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.03608871156109217, y=-0.5134069936485162, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.04735542209989908,
                                    w=0.998878102672063,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.06097655983234063, y=-0.5157721059884466, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.052776533725937394,
                                    w=0.9986063476104461,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.0858377052223318, y=-0.5184072988988078, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.04905546986560027,
                                    w=0.9987960556971904,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.11071716115947083, y=-0.520857097519297, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.05331328856312445,
                                    w=0.998577835355154,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.13557525479160404, y=-0.5235189933109154, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.05325268629978697,
                                    w=0.998581069018363,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.16043334842373724, y=-0.5261778373446759, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.05627284335510226,
                                    w=0.9984154281163388,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.1852746573876516, y=-0.5289869804529417, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.053988008761894454,
                                    w=0.9985415839662991,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.21012893632246232, y=-0.5316824455809979, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.05599522343226616,
                                    w=0.9984310366533938,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.23497253410477015, y=-0.5344778557789027, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.05297672967158372,
                                    w=0.9985957470935393,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.25983215361583234, y=-0.5371229669023023, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.054141127578941003,
                                    w=0.9985332935383181,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.2846856696111786, y=-0.5398260614250034, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.05411083327989586,
                                    w=0.9985349356541089,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.3095391856065248, y=-0.5425276300687756, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.05351349035938597,
                                    w=0.9985671266117045,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.33439575335972904, y=-0.5451994440734325, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.04727811462952416,
                                    w=0.9988817647134608,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.359284364570442, y=-0.5475607417160404, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.04544276776526387,
                                    w=0.9989669438263872,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.3841806051757999, y=-0.5498304866229091, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.19161380179158843,
                                    w=0.9814704024895371,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.4073449731967571, y=-0.5592337155227938, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.14661999813788598,
                                    w=0.9891928912735101,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.43126999186381454, y=-0.5664854551328062, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.014710982453639472,
                                    w=0.9998917876426672,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.45625931108384066, y=-0.5672209287765781, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.06526337182312675,
                                    w=0.997868073594038,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.48104645134577595, y=-0.5704771544110372, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.010910749671625692,
                                    w=0.9999404759992482,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.506040348202589, y=-0.5699316526939242, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.01861910978704578,
                                    w=0.9998266493501451,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5310228009674347, y=-0.5690008665472419, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.002700776113681641,
                                    w=0.9999963528975412,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5560228013399637, y=-0.5691359068324573, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.004776129065826561,
                                    w=0.9999885942305275,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.5810212758335638, y=-0.5693747068848438, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.01292489993802821,
                                    w=0.9999164699921649,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.6060136468114479, y=-0.57002091661127, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.01495544196630707,
                                    w=0.9998881611238291,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.6310022030920095, y=-0.5707685972864738, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.020725791582247705,
                                    w=0.9997851977116331,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.6559808411595327, y=-0.5718046690792562, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.021764159839367293,
                                    w=0.9997631326201655,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.6809571904086624, y=-0.5728926207556242, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.02573530994430273,
                                    w=0.9996687920616861,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.7059236214447537, y=-0.5741789366927605, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.025490007322641787,
                                    w=0.99967507697586,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.730891578359774, y=-0.575453045598465, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.026926664888683952,
                                    w=0.999637411623821,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.7558549576380074, y=-0.5767988708138319, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.02544430586762972,
                                    w=0.9996762412395898,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.7808229145530277, y=-0.5780706909011428, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.017916673751748678,
                                    w=0.9998394835180662,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.8058068931968023, y=-0.5789663818324584, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.01675680911904922,
                                    w=0.9998595948172662,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.830792397719506, y=-0.5798040893644725, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.01110905415393278,
                                    w=0.999938292553998,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.855786294576319, y=-0.5803595092946239, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.010452790351177306,
                                    w=0.9999453680946147,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.8807809543725966, y=-0.580882122827802, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.004013087047747384,
                                    w=0.9999919475337525,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.9057801918056612, y=-0.5810827759069639, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.0047912409989492935,
                                    w=0.999988521938972,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.9307794292387257, y=-0.5813223388988149, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.003723180737499885,
                                    w=0.9999930689385782,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.9557786666717902, y=-0.5815084961281514, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.005707030793065203,
                                    w=0.9999837147671591,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=0.9807763782259258, y=-0.581793835487872, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.010010093967608687,
                                    w=0.9999498977542624,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.0057718009616679, y=-0.5822943237765799, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.013032060727042959,
                                    w=0.9999150790908229,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.0307634090000874, y=-0.5829458740792575, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.025643124061842396,
                                    w=0.9996711610266393,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.0557306029756433, y=-0.5842276123796069, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.029003785702206658,
                                    w=0.9995793017139463,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.0806886416776251, y=-0.5856771973621449, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.04466164367804041,
                                    w=0.999002170960592,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.1055886969803055, y=-0.5879080323563244, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.04786045931466862,
                                    w=0.9988540315953022,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.1304742564331605, y=-0.5902983216985831, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.0673369573430766,
                                    w=0.9977302912990949,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.1552476637847349, y=-0.5936575441607488, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.06872125707667395,
                                    w=0.9976358999283264,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.1800111529232709, y=-0.5970854311747189, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.07654901336204696,
                                    w=0.9970658195692484,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.2047181845414343, y=-0.6009016543761163, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.07594867249249997,
                                    w=0.9971117285172344,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.2294297937963847, y=-0.6046881229383985, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.07705679200865043,
                                    w=0.9970267051615697,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.2541330107172257, y=-0.6085295231421242, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.0719803056803657,
                                    w=0.9974060535179046,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.278873611671827, y=-0.612119153322567, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.05654703469618704,
                                    w=0.9983999363316628,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.3037141576962767, y=-0.6149420293411936, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.04738576855418304,
                                    w=0.9988766635268488,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.3286020059675252, y=-0.617308667560053, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.033726320969592986,
                                    w=0.9994311058166321,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.3535447858802172, y=-0.6156233342829864, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.025764991675575485,
                                    w=0.9996680274991081,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.378511979855773, y=-0.6143354924669211, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.007660172967243097,
                                    w=0.9999706604446512,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.403508928470444, y=-0.6147184880780969, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.011139562188535124,
                                    w=0.9999379531522182,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.4285028253272571, y=-0.6152754338871773, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.041126905082438464,
                                    w=0.9991539309227283,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.4534181394192274, y=-0.6173300298650588, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.039979777827706245,
                                    w=0.9992004890735628,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.4783380311479846, y=-0.6193274053831033, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.25081691847370785,
                                    w=0.9680345414329766,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.5001924321084061, y=-0.6314672981421268, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.2108184087211731,
                                    w=0.9775252418962246,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.5229707527603296, y=-0.6417715585495785, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.1367393631423337,
                                    w=0.990607059619216,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.5470353893493893, y=-0.6485441721758889, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.3471309575307704,
                                    w=0.9378166656248813,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.5660104567708117, y=-0.6322668587009161, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.2707269667359012,
                                    w=0.9626561740735776,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.5873460588953776, y=-0.619235852647364, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.0005645749253793133,
                                    w=0.9999998406275641,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.6123460592679066, y=-0.6192076238871778, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=-0.011277191290845007,
                                    w=0.9999364104564799,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.6373391931852552, y=-0.6197714361514386, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.5678879499949866,
                                    w=0.8231058718357509,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.6462144679757102, y=-0.5963995485961391, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.19473595105034358,
                                    w=0.9808557026232341,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.6693185637789725, y=-0.5868490723796072, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.26437031834818725,
                                    w=0.9644212434286576,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.690823538464656, y=-0.5741011168673822, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6850548085981633,
                                    w=0.7284915299552451,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.6923585726672172, y=-0.5491476558021873, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7383126201989867,
                                    w=0.6744586531840976,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.690103323610174, y=-0.5242498893179004, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.47996119356723715,
                                    w=0.877289719915555,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7035852268872418, y=-0.5031965747951972, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.21608764935117533,
                                    w=0.9763739692340653,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7262506324984201, y=-0.49264741081964303, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.33305923007750166,
                                    w=0.9429059068964314,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.74570406296408, y=-0.4769453537008985, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.44182270563169385,
                                    w=0.8971023892445553,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7609437787673414, y=-0.4571272381712106, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6892068465394787,
                                    w=0.7245646435502545,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.762193473610182, y=-0.4321585183167258, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7047507049200364,
                                    w=0.7094550330462895,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7623597944134417, y=-0.40715928088366127, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6497888740069262,
                                    w=0.7601147408232595,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.766248496863966, y=-0.3824636933574652, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.639503874312413,
                                    w=0.7687878736943069,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7708001937091353, y=-0.3578817838114787, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.5790749879215491,
                                    w=0.8152742841299839,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7790338364099512, y=-0.33427643678004415, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.5248078495650208,
                                    w=0.851220724039857,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7902627794483692, y=-0.31193985807806257, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.678046111868636,
                                    w=0.7350193672141063,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7922754137557035, y=-0.28702149222823437, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.675488132903559,
                                    w=0.7373708580534383,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.794461235321478, y=-0.26211685928876705, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7089924763323149,
                                    w=0.7052160438505153,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7943277209151915, y=-0.2371176218557025, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.710013985015401,
                                    w=0.7041875752117115,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7941217272597783, y=-0.21211838442263797, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7158462552606077,
                                    w=0.6982579314475166,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.793499931596216, y=-0.18712601344475388, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7167302590419181,
                                    w=0.6973505114171101,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7928148119571006, y=-0.16213516834579877, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7164004349986182,
                                    w=0.6976893411352869,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7921533434413845, y=-0.13714432324684367, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7177196284966612,
                                    w=0.6963322015177918,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7913972704320713, y=-0.11215576696628204, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7535618835565784,
                                    w=0.6573769752970222,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.788004478633468, y=-0.0873869372514946, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7489211469940876,
                                    w=0.6626591247278348,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.784960350170138, y=-0.06257309410830203, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7216735644755711,
                                    w=0.6922335345366647,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7839197007405687, y=-0.03759445604077882, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7213247684583564,
                                    w=0.6925969812297037,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7829042283133276, y=-0.012615055033791123, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7633895481306127,
                                    w=0.6459383854555627,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7787660446579139, y=0.012040096700786762, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7501426868771467,
                                    w=0.6612760008685745,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7756303634588448, y=0.03684249575201193, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.8851254166661424,
                                    w=0.46535255105305673,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7614579999664102, y=0.05743728365655443, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.9192127919477135,
                                    w=0.39376114983539123,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.7442102274926015, y=0.0755349706938091, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.7214202278168438,
                                    w=0.6924975486575337,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.74318788861018, y=0.10051360876133231, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6732930274402362,
                                    w=0.7393757496701939,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.745521720432066, y=0.12540450879043874, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.6406489392480689,
                                    w=0.7678339251689289,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.750000175088644, y=0.15000015124678612, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.38268354250047004,
                                    w=0.9238794868917108,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(
                                    sec=25, nanosec=170000000
                                ),
                                frame_id="map",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(
                                    x=1.775000087544322, y=0.17500007562339306, z=0.0
                                ),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0,
                                    y=0.0,
                                    z=0.382683542500468,
                                    w=0.9238794868917117,
                                ),
                            ),
                        ),
                        geometry_msgs.msg.PoseStamped(
                            header=std_msgs.msg.Header(
                                stamp=builtin_interfaces.msg.Time(sec=0, nanosec=0),
                                frame_id="",
                            ),
                            pose=geometry_msgs.msg.Pose(
                                position=geometry_msgs.msg.Point(x=1.8, y=0.2, z=0.0),
                                orientation=geometry_msgs.msg.Quaternion(
                                    x=0.0, y=0.0, z=0.0, w=1.0
                                ),
                            ),
                        ),
                    ],
                ),
                required_resources=[],
            )
        ],
    ),
]
