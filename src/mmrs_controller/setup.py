"""Setup script for mmrs_controller package."""


import glob
import os

from setuptools import find_packages, setup

package_name = "mmrs_controller"

setup(
    name=package_name,
    version="0.0.0",
    packages=find_packages(exclude=["test"]),
    data_files=[
        (
            "share/ament_index/resource_index/packages",
            ["resource/" + package_name],
        ),
        (
            "share/" + package_name,
            ["package.xml"],
        ),
        (
            os.path.join("share", package_name, "launch"),
            glob.glob("launch/*launch.py"),
        ),
        (
            os.path.join("share", package_name, "params"),
            glob.glob("params/*.yaml"),
        ),
    ],
    install_requires=["setuptools"],
    zip_safe=True,
    maintainer="Jakub Kozłowicz",
    maintainer_email="ja.kozlowicz@gmail.com",
    description="Controller for the multi-mobile robot system.",
    license="MIT",
    tests_require=["pytest"],
    entry_points={
        "console_scripts": [
            "controller = mmrs_controller.controller:main",
            "path_divider = mmrs_controller.path_divider:main",
            "movement_controller = mmrs_controller.movement_controller:main"
        ],
    },
)
