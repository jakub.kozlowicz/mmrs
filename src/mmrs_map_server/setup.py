"""Setup script for mmrs_map_server package."""


import glob
import os

from setuptools import find_packages, setup

package_name = "mmrs_map_server"


setup(
    name=package_name,
    version="0.0.0",
    packages=find_packages(exclude=["test"]),
    data_files=[
        (
            "share/ament_index/resource_index/packages",
            ["resource/" + package_name],
        ),
        (
            "share/" + package_name,
            ["package.xml"],
        ),
        (
            os.path.join("share", package_name, "launch"),
            glob.glob("launch/*launch.py"),
        ),
        (
            os.path.join("share", package_name, "maps"),
            glob.glob("maps/*.[yp][ag]m*"),
        ),
        (
            os.path.join("share", package_name, "rviz"),
            glob.glob("rviz/*.rviz"),
        ),
    ],
    install_requires=["setuptools"],
    zip_safe=True,
    maintainer="Jakub Kozłowicz",
    maintainer_email="ja.kozlowicz@gmail.com",
    description="Map server for the multi-mobile robot system.",
    license="MIT",
    tests_require=["pytest"],
    entry_points={
        "console_scripts": ["map_server = mmrs_map_server.map_server:main"],
    },
)
