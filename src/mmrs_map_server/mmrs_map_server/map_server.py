"""Map server node for the multi-mobile robot system.

Implements same functionality what nav2 map_server package does,
but for global system. Not only for one robot. Useful for visualization
purposes.
"""

import sys

import rclpy
from nav_msgs.msg import OccupancyGrid
from rclpy.executors import ExternalShutdownException
from rclpy.node import Node
from rclpy.parameter import Parameter

from mmrs_map_server.map_data import MapData


class MapServer(Node):
    """Map server node for the multi mobile robot system."""

    def __init__(self) -> None:
        """Create map server node."""
        super().__init__(node_name="map_server")  # type: ignore[type]
        self.get_logger().debug("Starting map server node.")

        self.declare_parameters(
            namespace="",
            parameters=[
                ("map_publish_interval", Parameter.Type.DOUBLE),
                ("map_file", Parameter.Type.STRING),
            ],
        )

        self.map_publisher = self.create_publisher(
            OccupancyGrid,
            "mmrs/map",
            10,
        )
        self.map_publisher_timer = self.create_timer(
            self.get_parameter("map_publish_interval")
            .get_parameter_value()
            .double_value,
            self.publish_map,
        )

        self.map_data = MapData.from_yaml(
            self.get_parameter("map_file").get_parameter_value().string_value,
        )

        self.get_logger().info("Map server node has been initialized.")

    def publish_map(self) -> None:
        """Publish map to topic."""
        msg = OccupancyGrid()
        msg.header.frame_id = "map"
        msg.header.stamp = self.get_clock().now().to_msg()
        msg.info.resolution = self.map_data.resolution
        msg.info.width = self.map_data.width
        msg.info.height = self.map_data.height
        msg.info.origin.position.x = self.map_data.position[0]
        msg.info.origin.position.y = self.map_data.position[1]
        msg.info.origin.position.z = self.map_data.position[2]
        msg.info.origin.orientation.x = self.map_data.orientation[0]
        msg.info.origin.orientation.y = self.map_data.orientation[1]
        msg.info.origin.orientation.z = self.map_data.orientation[2]
        msg.info.origin.orientation.w = self.map_data.orientation[3]
        msg.data = self.map_data.data

        self.map_publisher.publish(msg)


def main(args: list[str] | None = None) -> None:
    """Run controller node."""
    rclpy.init(args=args)
    node = MapServer()
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        pass
    except ExternalShutdownException:
        sys.exit(1)
    finally:
        rclpy.try_shutdown()
        node.destroy_node()
