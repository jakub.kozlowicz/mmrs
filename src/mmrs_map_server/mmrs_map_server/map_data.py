"""Map data representation for the map server of the multi mobile robot system."""

import dataclasses
import enum
import math
import pathlib

import imageio
import numpy as np
import yaml


def rpy_to_quaternion(
    roll: float,
    pitch: float,
    yaw: float,
) -> tuple[float, float, float, float]:
    """Convert roll, pitch, yaw to quaternion.

    :param roll: The roll.
    :type roll: float
    :param pitch: The pitch.
    :type pitch: float
    :param yaw: The yaw.
    :type yaw: float
    :return: The quaternion.
    :rtype: tuple[float, float, float, float]
    """
    cy = math.cos(yaw * 0.5)
    sy = math.sin(yaw * 0.5)
    cp = math.cos(pitch * 0.5)
    sp = math.sin(pitch * 0.5)
    cr = math.cos(roll * 0.5)
    sr = math.sin(roll * 0.5)

    w = cy * cp * cr + sy * sp * sr
    x = cy * cp * sr - sy * sp * cr
    y = sy * cp * sr + cy * sp * cr
    z = sy * cp * cr - cy * sp * sr

    return x, y, z, w


class MapImageMode(enum.Enum):
    """Map image mode."""

    TRINARY = "trinary"
    SCALE = "scale"
    RAW = "raw"


@dataclasses.dataclass
class MapMetadata:
    """Map metadata for the map server of the multi mobile robot system."""

    image: pathlib.Path
    resolution: float
    origin: tuple[float, float, float]
    negate: int
    occupied_thresh: float
    free_thresh: float
    mode: MapImageMode | None = None

    def __post_init__(self) -> None:
        """Post init."""
        if not isinstance(self.image, pathlib.Path):
            self.image = pathlib.Path(self.image)

        if self.mode is None:
            self.mode = MapImageMode.TRINARY
        elif not isinstance(self.mode, MapImageMode):
            self.mode = MapImageMode(self.mode)

        if self.negate not in (0, 1):
            msg = "Negate must be 0 or 1."
            raise ValueError(msg)

        if self.occupied_thresh < self.free_thresh:
            msg = "Occupied threshold must be greater than or equal to free threshold."
            raise ValueError(msg)


@dataclasses.dataclass
class MapData:
    """Map data for the map server of the multi mobile robot system."""

    image: pathlib.Path
    resolution: float
    width: int
    height: int
    data: list[int]
    position: tuple[float, float, float]
    orientation: tuple[float, float, float, float]

    @classmethod
    def from_yaml(cls, yaml_file: str) -> "MapData":
        """Load map data from YAML file."""
        meta_file = pathlib.Path(yaml_file)

        with meta_file.open("r") as file:
            yaml_data = yaml.safe_load(file)

        map_metadata = MapMetadata(**yaml_data)
        if not map_metadata.image.is_absolute():
            map_metadata.image = meta_file.parent / map_metadata.image

        try:
            image = imageio.imread(map_metadata.image.resolve())
        except Exception as exc:  # noqa: BLE001 (The implementation of the exception is not relevant.)
            msg = f"Failed to open image file '{map_metadata.image}': {exc!s}"
            raise RuntimeError(msg) from None

        height, width = image.shape

        position = (map_metadata.origin[0], map_metadata.origin[1], 0.0)
        orientation = rpy_to_quaternion(0.0, 0.0, map_metadata.origin[2])

        data = load_data(image, map_metadata)

        return cls(
            image=map_metadata.image,
            resolution=map_metadata.resolution,
            width=width,
            height=height,
            data=data.tolist(),
            position=position,
            orientation=orientation,
        )


def load_data(
    image: np.ndarray,
    map_metadata: MapMetadata,
) -> np.ndarray:
    """Load map data from image."""
    height, width = image.shape

    data = np.zeros(width * height, dtype=np.int8)

    for j in range(height):
        for i in range(width):
            color_avg = image[j, i]

            if map_metadata.negate:
                color_avg = 255 - color_avg

            if map_metadata.mode == MapImageMode.RAW:
                value = color_avg
                data[i + width * (height - j - 1)] = value
                continue

            occ = (255 - color_avg) / 255.0

            if occ > map_metadata.occupied_thresh:
                value = 100
            elif occ < map_metadata.free_thresh:
                value = 0
            elif map_metadata.mode == MapImageMode.TRINARY:
                value = -1
            else:
                ratio = (occ - map_metadata.occupied_thresh) / (
                    map_metadata.occupied_thresh - map_metadata.free_thresh
                )
                value = 1 + int(98 * ratio)

            data[i + width * (height - j - 1)] = value

    return data
