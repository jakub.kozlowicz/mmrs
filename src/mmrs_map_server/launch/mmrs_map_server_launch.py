"""Launch global map server for multi-mobile robot system."""

from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument
from launch.substitutions import (
    LaunchConfiguration,
    PathJoinSubstitution,
    TextSubstitution,
)
from launch_ros.actions import Node
from launch_ros.substitutions import FindPackageShare


def generate_launch_description() -> LaunchDescription:
    """Generate launch description."""

    mmrs_map_server_dir = FindPackageShare("mmrs_map_server")

    map_file = LaunchConfiguration("map_file")
    map_publish_interval = LaunchConfiguration("map_publish_interval")
    rviz_config_file = LaunchConfiguration("rviz_config_file")

    return LaunchDescription(
        [
            DeclareLaunchArgument(
                "map_file",
                description="Full path to the yaml map file.",
                default_value=PathJoinSubstitution(
                    [mmrs_map_server_dir, "maps", "turtlebot3_world.yaml"],
                ),
            ),
            DeclareLaunchArgument(
                "map_publish_interval",
                description="Interval in seconds how frequent map should be published.",
                default_value=TextSubstitution(text=str(2.0)),
            ),
            DeclareLaunchArgument(
                "rviz_config_file",
                default_value=PathJoinSubstitution(
                    [mmrs_map_server_dir, "rviz", "mmrs_default_view.rviz"]
                ),
                description="Full path to the rviz config file to use for simulation",
            ),
            Node(
                package="mmrs_map_server",
                executable="map_server",
                name="map_server",
                parameters=[
                    {
                        "map_file": map_file,
                        "map_publish_interval": map_publish_interval,
                    },
                ],
            ),
            Node(
                package="rviz2",
                executable="rviz2",
                name="rviz2",
                arguments=["-d", rviz_config_file],
            ),
        ],
    )
