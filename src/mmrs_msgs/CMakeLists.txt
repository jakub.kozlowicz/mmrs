cmake_minimum_required(VERSION 3.8)
project(mmrs_msgs)

find_package(ament_cmake REQUIRED)
find_package(geometry_msgs REQUIRED)
find_package(nav_msgs REQUIRED)
find_package(unique_identifier_msgs REQUIRED)
find_package(rosidl_default_generators REQUIRED)

rosidl_generate_interfaces(${PROJECT_NAME}
  "action/Navigate.action"

  "msg/DividedPath.msg"
  "msg/Mission.msg"
  "msg/PathSegment.msg"
  "msg/PathToDivide.msg"
  "msg/RobotMetadata.msg"

  "srv/AddRobotClearance.srv"
  "srv/CalculatePath.srv"
  "srv/CalculatePathThroughPoses.srv"
  "srv/CanNextSegment.srv"
  "srv/DividePaths.srv"
  "srv/Register.srv"
  "srv/RegisterDividedPaths.srv"
  "srv/RemoveRobotClearance.srv"
  "srv/ReleaseSegmentResources.srv"
  DEPENDENCIES geometry_msgs nav_msgs unique_identifier_msgs
)

ament_export_dependencies(rosidl_default_runtime)

ament_package()
