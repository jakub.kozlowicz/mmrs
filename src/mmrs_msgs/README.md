# mmrs_msgs

The `mmrs_msgs` package contains the custom messages, actions and services used across the `mmrs` project.

## Actions

- `Navigate.action` - Action for navigating robot segment by segment.

## Messages

- `PathSegment.msg` - Message for representing one segment of a path.

## Services

- `CalculatePath.srv` - Service for calculating a path to a single pose.
- `CalculatePathThroughPoses.srv` - Service for calculating a path through multiple poses.
- `CanNextSegment.srv` - Service for checking if the robot can move to the next segment.
