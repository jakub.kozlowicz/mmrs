"""Launch simulation of the multi-mobile robot system."""

from launch import LaunchDescription
from launch.actions import (
    DeclareLaunchArgument,
    ExecuteProcess,
    GroupAction,
    IncludeLaunchDescription,
    LogInfo,
)
from launch.conditions import IfCondition
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import (
    LaunchConfiguration,
    PathJoinSubstitution,
    TextSubstitution,
)
from launch_ros.substitutions import FindPackageShare
from nav2_common.launch import ReplaceString


def generate_launch_description() -> LaunchDescription:
    """Generate launch description."""

    robots = {
        "robot1": {
            "name": "robot1",
            "x_pose": 0.0,
            "y_pose": 0.5,
            "z_pose": 0.01,
            "x_orientation": 0.0,
            "y_orientation": 0.0,
            "z_orientation": 0.0,
            "w_orientation": 1.0,
            "roll": 0.0,
            "pitch": 0.0,
            "yaw": 0.0,
        },
        "robot2": {
            "name": "robot2",
            "x_pose": 0.0,
            "y_pose": -0.5,
            "z_pose": 0.01,
            "x_orientation": 0.0,
            "y_orientation": 0.0,
            "z_orientation": 0.0,
            "w_orientation": 1.0,
            "roll": 0.0,
            "pitch": 0.0,
            "yaw": 0.0,
        },
        "robot3": {
            "name": "robot3",
            "x_pose": -0.5,
            "y_pose": 0.0,
            "z_pose": 0.01,
            "x_orientation": 0.0,
            "y_orientation": 0.0,
            "z_orientation": 0.0,
            "w_orientation": 1.0,
            "roll": 0.0,
            "pitch": 0.0,
            "yaw": 0.0,
        },
        "robot4": {
            "name": "robot4",
            "x_pose": 0.5,
            "y_pose": 0.0,
            "z_pose": 0.01,
            "x_orientation": 0.0,
            "y_orientation": 0.0,
            "z_orientation": 0.0,
            "w_orientation": 1.0,
            "roll": 0.0,
            "pitch": 0.0,
            "yaw": 0.0,
        },
    }

    mmrs_simulation_dir = FindPackageShare("mmrs_simulation")
    mmrs_controller_dir = FindPackageShare("mmrs_controller")
    mmrs_map_server_dir = FindPackageShare("mmrs_map_server")
    mmrs_navigator_dir = FindPackageShare("mmrs_navigator")
    nav2_bringup_dir = FindPackageShare("nav2_bringup")

    world = LaunchConfiguration("world")
    simulator = LaunchConfiguration("simulator")
    map_yaml_file = LaunchConfiguration("map")
    autostart = LaunchConfiguration("autostart")
    rviz_config_file = LaunchConfiguration("rviz_config")
    robots_params_file = LaunchConfiguration("robots_params_file")
    use_robot_state_pub = LaunchConfiguration("use_robot_state_pub")
    use_rviz = LaunchConfiguration("use_rviz")
    log_settings = LaunchConfiguration("log_settings", default="true")
    map_server_config = LaunchConfiguration("map_server_config")

    declare_world_cmd = DeclareLaunchArgument(
        "world",
        default_value=PathJoinSubstitution(
            [mmrs_simulation_dir, "worlds", "world_only.model"]
        ),
        description="Full path to world file to load",
    )

    declare_simulator_cmd = DeclareLaunchArgument(
        "simulator",
        default_value="gazebo",
        description="The simulator to use (gazebo or gzserver)",
    )

    declare_map_yaml_cmd = DeclareLaunchArgument(
        "map",
        default_value=PathJoinSubstitution(
            [mmrs_simulation_dir, "maps", "turtlebot3_world.yaml"]
        ),
        description="Full path to map file to load",
    )

    declare_robots_params_file_cmd = DeclareLaunchArgument(
        "robots_params_file",
        default_value=PathJoinSubstitution(
            [mmrs_simulation_dir, "params", "multirobot_params.yaml"]
        ),
        description="Full path to the ROS2 parameters file to use for robots nodes",
    )

    declare_autostart_cmd = DeclareLaunchArgument(
        "autostart",
        default_value="True",
        description="Automatically startup the stacks",
    )

    declare_rviz_config_file_cmd = DeclareLaunchArgument(
        "rviz_config",
        default_value=PathJoinSubstitution(
            [nav2_bringup_dir, "rviz", "nav2_namespaced_view.rviz"]
        ),
        description="Full path to the RVIZ config file to use.",
    )

    declare_map_server_config_cmd = DeclareLaunchArgument(
        "map_server_config",
        default_value=PathJoinSubstitution(
            [mmrs_simulation_dir, "rviz", "mmrs_default_view.rviz"]
        ),
        description="Full path to the RVIZ config file to use.",
    )

    declare_use_robot_state_pub_cmd = DeclareLaunchArgument(
        "use_robot_state_pub",
        default_value="True",
        description="Whether to start the robot state publisher",
    )

    declare_use_rviz_cmd = DeclareLaunchArgument(
        "use_rviz", default_value="True", description="Whether to start RVIZ"
    )

    # Start Gazebo with plugin providing the robot spawning service
    start_gazebo_cmd = ExecuteProcess(
        cmd=[
            simulator,
            "--verbose",
            "-s",
            "libgazebo_ros_init.so",
            "-s",
            "libgazebo_ros_factory.so",
            world,
        ],
        output="screen",
    )

    start_controller_cmd = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            PathJoinSubstitution(
                [mmrs_controller_dir, "launch", "mmrs_controller_launch.py"]
            ),
        ),
    )

    start_map_server_cmd = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            PathJoinSubstitution(
                [mmrs_map_server_dir, "launch", "mmrs_map_server_launch.py"]
            ),
        ),
        launch_arguments={
            "map_file": map_yaml_file,
            "rviz_config_file": map_server_config,
        }.items(),
    )

    nav_instances_cmds = []
    for robot, position in robots.items():
        params_file = ReplaceString(
            source_file=robots_params_file,
            replacements={"<robot_namespace>": ("/", robot)},
        )

        group = GroupAction(
            [
                IncludeLaunchDescription(
                    PythonLaunchDescriptionSource(
                        PathJoinSubstitution(
                            [nav2_bringup_dir, "launch", "rviz_launch.py"]
                        )
                    ),
                    condition=IfCondition(use_rviz),
                    launch_arguments={
                        "namespace": TextSubstitution(text=robot),
                        "use_namespace": "True",
                        "rviz_config": rviz_config_file,
                    }.items(),
                ),
                IncludeLaunchDescription(
                    PythonLaunchDescriptionSource(
                        PathJoinSubstitution(
                            [nav2_bringup_dir, "launch", "tb3_simulation_launch.py"]
                        )
                    ),
                    launch_arguments={
                        "namespace": robot,
                        "use_namespace": "True",
                        "map": map_yaml_file,
                        "use_sim_time": "True",
                        "params_file": params_file,
                        "autostart": autostart,
                        "use_rviz": "False",
                        "use_simulator": "False",
                        "headless": "False",
                        "use_robot_state_pub": use_robot_state_pub,
                        "x_pose": TextSubstitution(text=str(position["x_pose"])),
                        "y_pose": TextSubstitution(text=str(position["y_pose"])),
                        "z_pose": TextSubstitution(text=str(position["z_pose"])),
                        "roll": TextSubstitution(text=str(position["roll"])),
                        "pitch": TextSubstitution(text=str(position["pitch"])),
                        "yaw": TextSubstitution(text=str(position["yaw"])),
                        "robot_name": TextSubstitution(text=robot),
                    }.items(),
                ),
                IncludeLaunchDescription(
                    PythonLaunchDescriptionSource(
                        PathJoinSubstitution(
                            [mmrs_navigator_dir, "launch", "mmrs_navigator_launch.py"]
                        )
                    ),
                    launch_arguments={
                        "namespace": robot,
                        "x_pose": TextSubstitution(text=str(position["x_pose"])),
                        "y_pose": TextSubstitution(text=str(position["y_pose"])),
                        "z_pose": TextSubstitution(text=str(position["z_pose"])),
                        "x_orientation": TextSubstitution(
                            text=str(position["x_orientation"])
                        ),
                        "y_orientation": TextSubstitution(
                            text=str(position["y_orientation"])
                        ),
                        "z_orientation": TextSubstitution(
                            text=str(position["z_orientation"])
                        ),
                        "w_orientation": TextSubstitution(
                            text=str(position["w_orientation"])
                        ),
                    }.items(),
                ),
                LogInfo(
                    condition=IfCondition(log_settings),
                    msg=["Launching ", robot],
                ),
                LogInfo(
                    condition=IfCondition(log_settings),
                    msg=[robot, " map yaml: ", map_yaml_file],
                ),
                LogInfo(
                    condition=IfCondition(log_settings),
                    msg=[robot, " params yaml: ", params_file],
                ),
                LogInfo(
                    condition=IfCondition(log_settings),
                    msg=[robot, " rviz config file: ", rviz_config_file],
                ),
                LogInfo(
                    condition=IfCondition(log_settings),
                    msg=[
                        robot,
                        " using robot state pub: ",
                        use_robot_state_pub,
                    ],
                ),
                LogInfo(
                    condition=IfCondition(log_settings),
                    msg=[robot, " autostart: ", autostart],
                ),
            ]
        )

        nav_instances_cmds.append(group)

    ld = LaunchDescription()

    ld.add_action(declare_simulator_cmd)
    ld.add_action(declare_world_cmd)
    ld.add_action(declare_map_yaml_cmd)
    ld.add_action(declare_robots_params_file_cmd)
    ld.add_action(declare_use_rviz_cmd)
    ld.add_action(declare_autostart_cmd)
    ld.add_action(declare_rviz_config_file_cmd)
    ld.add_action(declare_map_server_config_cmd)
    ld.add_action(declare_use_robot_state_pub_cmd)

    ld.add_action(start_gazebo_cmd)
    ld.add_action(start_controller_cmd)
    ld.add_action(start_map_server_cmd)

    for simulation_instance_cmd in nav_instances_cmds:
        ld.add_action(simulation_instance_cmd)

    return ld
