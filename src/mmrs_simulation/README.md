# mmrs_simulation

The `mmrs_simulation` is a package that combines multiple packages and creates a
simulation using `Gazebo` to run virtual robots and test controller and
navigator.